import UIKit

class Res {
    
    static let i = Res()
    
    let categoriesSafe: [String] = ["Camisa".l, "Calça".l, "Saia".l, "Vestido".l]
    
    struct Therm {
        var uid: Int
        var name: String
        var color: UIColor
        var invertedColor: UIColor
        var desc: String
    }
    
    let therm: [Therm] = [
        Therm(uid: 1, name: "muito frio".l, color: Cl.mc.lightBlue500Color(), invertedColor: Cl.mc.lightBlue500Color(), desc: String.fontAwesomeIcon(name: .thermometerEmpty)),
        
        Therm(uid: 2, name: "frio".l, color: Cl.mc.lightBlue700Color(), invertedColor: Cl.mc.lightBlue200Color(), desc: String.fontAwesomeIcon(name: .thermometerQuarter)),
        
        Therm(uid: 3, name: "ameno".l, color: Cl.mc.blackColor(), invertedColor: Cl.mc.whiteColor(), desc: String.fontAwesomeIcon(name: .thermometerHalf)),
        
        Therm(uid: 4, name: "quente".l, color: Cl.mc.orange500Color(), invertedColor: Cl.mc.orange500Color(), desc: String.fontAwesomeIcon(name: .thermometerThreeQuarters)),
        
        Therm(uid: 5, name: "muito quente".l, color: Cl.mc.red500Color(), invertedColor: Cl.mc.red500Color(), desc: String.fontAwesomeIcon(name: .thermometerFull)),
    ]
    
    func therm(uid: Int16) -> Therm? {
        return therm.first(where: { $0.uid == uid })
    }
    
    func therm(uid: Int16) -> String {
        return therm(uid: uid)?.desc ?? String.fontAwesomeIcon(name: .thermometerHalf)
    }
    
    func therm(uid: Int16, inverted: Bool) -> UIColor
    {
        if let model: Res.Therm = therm(uid: uid) {
            return inverted ? model.invertedColor : model.color
        }

        return inverted ? UIColor.black : UIColor.white
    }
    
    private lazy var colorPaletteCategories: [String] = [
        "PRETO".l, "BRANCO".l, "CINZA".l, "BEGE".l, "VERMELHO".l, "LARANJA".l, "AMARELO".l,
        "VERDE".l, "AZUL".l, "ROXO".l, "ROSA".l, "MARROM".l
    ]
    
    private lazy var colorPaletteStr: [String: [String]] = [
        "PRETO".l: ["#000000", "#001A34", "#362D29"],
        "BRANCO".l: ["#F5FAFB", "#F0F1EC", "#F4F3EA", "#FAFBF6"],
        "CINZA".l: ["#40414A", "#515A60", "#6E767F", "#666666", "#878789", "#A49EA2", "#B7B7B7", "#D4D4D4"],
        "BEGE".l: ["#947F78", "#A07E6E", "#C78D6C", "#D6B68A", "#B79B84", "#E5CBB7", "#F8DEC7", "#F3F1E4"],
        "VERMELHO".l: ["#7F182E", "#D1232A", "#F01836", "#EC332E", "#DF4516", "#9D5053", "#E15A52", "#FE756D"],
        "LARANJA".l: ["#FF5A00", "#C65821", "#DF6F30", "#EE854D", "#FFA306", "#D29611", "#FBB875", "#F6D18B"],
        "AMARELO".l: ["#D4C13F", "#FCC938", "#F4D056", "#FEE05E", "#EEF637", "#FFED2E", "#F9E380", "#FDF4B8"],
        "VERDE".l: [
            "#035535", "#4E6D38", "#6B6436", "#7E803E", "#1B9683", "#1F9D4B", "#519E4B", "#B8CC94", "#BDCC37",
            "#B7F92F","#B5DFCD","#E0E9E2"
        ],
        "AZUL".l: [
            "#182046", "#0C316A", "#384A65", "#3145C0", "#0E6195",
            "#028FA9", "#00B2CA", "#4E7A8E", "#8BA3D4", "#9DD3E6", "#CDECEB", "#D5E9F8"
        ],
        "ROXO".l: [
            "#400A41", "#731F5B", "#5E3B4B", "#432E66", "#462F90", "#964688", "#966DAC",
            "#B793B4", "#A091CB", "#ADB0D6", "#DCD0DA", "#E2D4E8"
        ],
        "ROSA".l: [
            "#B70558", "#CE0056", "#FD12A2", "#EA4EA0", "#E57999", "#E4ABAC", "#D6B5B6",
            "#F0C6DC", "#FCD0BF", "#FCCAD6", "#FADCDA", "#F7E5DB"
        ],
        "MARROM".l: [
            "#423635", "#522F24", "#5F4627", "#795349", "#7B4A22", "#9A5837", "#B78240",
            "#B87139", "#9C8369", "#B18259", "#CFA77C"
        ]
    ]
    
//
//    lazy var colorPaletteStr: [String: [String]] = [
//        "#e6194b", "#3cb44b", "#ffe119", "#4363d8", "#f58231", "#911eb4", "#46f0f0",
//        "#f032e6", "#bcf60c", "#fabebe", "#008080", "#e6beff", "#9a6324", "#fffac8",
//        "#800000", "#aaffc3", "#808000", "#ffd8b1", "#000075", "#808080", "#ffffff",
//        "#000000"
//    ]
    
    lazy var colorPaletteFull: [(String, [UIColor])] = {
        var r: [(String, [UIColor])] = []
        for category in self.colorPaletteCategories {
            r.append((category, colorPaletteStr[category]!.map { Cl.from(hex: $0)! }))
        }
        return r
    }()
    
    func colorNameFrom(cl: UIColor?) -> String? {
        if let c = cl {
            for s in colorPaletteFull {
                if s.1.contains(c) { return s.0 }
            }
        }
        return nil
    }
    
    lazy var colorPaletteOnlyColors: [UIColor] = {
        var r: [UIColor] = []
        for t in self.colorPaletteFull {
            r.append(contentsOf: t.1)
        }
        return r
    }()
    
    // baseado na tabela acima, mude isso aki se mudar acima!
    lazy var colorPaletteLite: [(String, UIColor)] = {
        let strs = [
            ("PRETO".l, "#362D29"),
            ("BRANCO".l, "#F5FAFB"),
            ("CINZA".l, "#6E767F"),
            ("BEGE".l, "#C78D6C"),
            ("VERMELHO".l, "#F01836"),
            ("LARANJA".l, "#FF5A00"),
            ("AMARELO".l, "#EEF637"),
            ("VERDE".l, "#1F9D4B"),
            ("AZUL".l, "#3145C0"),
            ("ROXO".l, "#462F90"),
            ("ROSA".l, "#FD12A2"),
            ("MARROM".l, "#5F4627")
        ]
        return strs.map({ ($0.0, Cl.from(hex: $0.1)!) })
    }()
    
    lazy var colorPaletteLiteColors: [UIColor] = {
        return colorPaletteLite.map { $0.1 }
    }()

    // chart colors
    
    func colorChartFrom(index: Int, reverseIt: Bool=false) -> UIColor {
        var colors = [
            "#3366CC",
            "#DC3912",
            "#FF9900",
            "#109618",
            "#990099",
            "#3B3EAC",
            "#0099C6",
            "#DD4477",
            "#66AA00",
            "#B82E2E",
            "#316395",
            "#994499",
            "#22AA99",
            "#AAAA11",
            "#6633CC",
            "#E67300",
            "#8B0707",
            "#329262",
            "#5574A6",
            "#3B3EAC"
        ]
        if reverseIt { colors.reverse() }
        
        return Cl.from(hex: colors[index % colors.count])!
    }
    
}

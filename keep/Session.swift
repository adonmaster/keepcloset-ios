//
//  Session.swift
//  keep
//
//  Created by Adonio Silva on 27/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit

class Session {
    
    static let shared = Session()
    
    private var cache = [String:Any]()
    
    func getAndSetTrueAfter(_ key: String) -> Bool
    {
        if cache[key] as? Bool == true { return true }
        
        cache[key] = true
        
        return false
    }
    
}

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class FbSignIn: SignInable {

    private init() {}
    static let i = FbSignIn()
    
    
    func signIn(caller: UIViewController, onSuccess: @escaping (SignInData)->Void, onError: @escaping (String)->Void, onDone: @escaping ()->Void)
    {
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: caller) { result, error in
            // error
            if let e = error {
                onError(e.localizedDescription)
                onDone()
            }
                // ok
            else if let r = result {
                
                if !r.isCancelled && r.grantedPermissions.contains("email")
                {
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email, name, picture.type(large)"])?
                        .start { (conn, result, error) in
                            if let err = error {
                                print(err)
                            } else if let dict = result as? [String:Any] {
                                
                                let n = dict["name"] as! String
                                let e = dict["email"] as! String
                                var img: URL? = nil
                                if let picture = dict["picture"] as? [String:Any],
                                    let data = picture["data"] as? [String:Any],
                                    let url = data["url"] as? String {
                                    img = URL(string: url)
                                }
                                
                                let data = SignInData(name: n, email: e, imgUrl: img)
                                onSuccess(data)
                                onDone()
                                
                            } else {
                                onError("Erro: FBSignIn@37")
                                onDone()
                            }
                    }
                } else {
                    onDone()
                }
                
            } else {
                onError("Erro: FBSignIn@43")
                onDone()
            }
        }
    }
    
}

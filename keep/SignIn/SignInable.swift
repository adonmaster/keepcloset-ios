import UIKit

struct SignInData {
    
    var name: String
    var email: String
    var imgUrl: URL?
    
}

protocol SignInable {
    
    func signIn(caller: UIViewController, onSuccess: @escaping (SignInData)->Void, onError: @escaping (String)->Void, onDone: @escaping ()->Void)
    
}

import UIKit
import GoogleSignIn

protocol GIDSignInDelegateAll: GIDSignInDelegate, GIDSignInUIDelegate {}

class GoogleSignIn: SignInable {

    private init() {}
    static let i = GoogleSignIn()
    
    private var mCbSuccess: ((SignInData)->Void)?
    private var mCbDone: (()->Void)?
    private var mCbError: ((String)->Void)?
    
    func initialize(caller: GIDSignInDelegateAll)
    {
        if let i = GIDSignIn.sharedInstance() {
            i.clientID = Env.google_id
            i.delegate = caller
            i.uiDelegate = caller
        }
    }
    
    func deinitialize() {
        mCbSuccess = nil
        mCbDone = nil
        mCbError = nil
    }
    
    func signIn(caller: UIViewController, onSuccess: @escaping (SignInData)->Void, onError: @escaping (String)->Void, onDone: @escaping ()->Void)
    {
        mCbSuccess = onSuccess
        mCbDone = onDone
        mCbError = onError
        
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    
    func delegate(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if let error = error {
            mCbError?(error.localizedDescription)
        } else {
            let n = user.profile.name
            let e = user.profile.email
            let img = user.profile.imageURL(withDimension: 600)
            
            let data = SignInData(name: n!, email: e!, imgUrl: img)
            mCbSuccess?(data)
        }
        
        mCbDone?()
        deinitialize()
    }
}

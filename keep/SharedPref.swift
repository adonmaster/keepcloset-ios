//
//  SharedPref.swift
//  keep
//
//  Created by Adonio Silva on 30/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit

class SharedPref {
    
    //
    static let shared = SharedPref()
    static let key_shared_pending_closet = "shared.pending.closet"
    static let key_shared_pending_inpirations = "shared.pending.inspirations"
    
    //
    var def: UserDefaults {
        return UserDefaults(suiteName: Env.group_container_id)!
    }
    
    
    func put(key: String, list: [URL]) {
        let strings = list.map({ $0.absoluteString })
        def.set(strings, forKey: key)
    }
    
    func get(key: String) -> [URL]? {
        let strings = def.stringArray(forKey: key) ?? []
        return strings.map { URL(string: $0)! }
    }
    
    
}

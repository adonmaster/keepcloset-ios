//
//  collectionview-ext.swift
//  keep
//
//  Created by Adonio Silva on 31/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func withValidIndexes(_ _list: [IndexPath]?, cb: (UICollectionView, [IndexPath])->()) {
        let list = (_list ?? [])
            .filter {
                $0.section < numberOfSections && $0.row < numberOfItems(inSection: $0.section)
            }
        cb(self, Arr(list).distinct({ $0 }))
    }
    
}

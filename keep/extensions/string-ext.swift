import Foundation

extension String {
    
    /**
     l18n - Localization
     */
    var l: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func trim() -> String {
        return String(self.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    var nillify: String? {
        return trim().isEmpty ? nil : self
    }
    
}

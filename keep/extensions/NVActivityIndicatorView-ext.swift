//
//  NVActivityIndicatorView+ext.swift
//  keep
//
//  Created by Adonio Silva on 27/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import NVActivityIndicatorView

extension NVActivityIndicatorView {
    
    func setup(_ type: NVActivityIndicatorType = .ballPulseSync) {
        self.type = type
        startAnimating()
    }
    
}

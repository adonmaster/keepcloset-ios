//
//  notification-ext.swift
//  keep
//
//  Created by Adonio Silva on 01/06/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let exApplicationDidBecomeActive = Notification.Name("applicationDidBecomeActive")
}

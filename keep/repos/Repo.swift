import CoreData

class Repo {
    
    static let pref = PrefRepo()
    static let user = UserRepo()
    static let item = ItemRepo()
    static let look = LookRepo()
    static let scheduleImg = ScheduleImgRepo()
    static let luggage = LuggageRepo()
    static let inspiration = InspirationRepo()
    static let stats = StatsRepo()

    // # utils
    class func seed() {
    }
    
}

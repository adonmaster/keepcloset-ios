import UIKit

class PrefRepo {
    
    private let pref = UserDefaults.standard;
    
    func set(_ key: String, _ val: Bool) {
        pref.set(val, forKey: key);
    }
    
    func get(_ key: String) -> Bool {
        return pref.bool(forKey: key);
    }
    
    func get(_ key: String) -> Int {
        return pref.integer(forKey: key);
    }
    
    func set(_ key: String, _ val: Int) {
        pref.set(val, forKey: key);
    }
    
    func get(_ key: String, _ def: String) -> String {
        return pref.string(forKey: key) ?? def
    }
    
    func get(key: String) -> String? {
        return pref.string(forKey: key)
    }
    
    func set(_ key: String, _ val: String) {
        pref.set(val, forKey: key);
    }
    
    func remove(_ key: String) {
        pref.removeObject(forKey: key)
    }
    
    func first(_ key: String, cb: ()->Void) {
        let k = "ProfRepo@first_\(key)"
        if !get(k) {
            set(k, true)
            cb()
        }
    }
    
    func firstView(_ v: UIView, key: String, cb: ()->Void) {
        v.isHidden = true
        first(key) {
            v.isHidden = false
            cb()
        }
    }
    
    // ****** methods ******
    
//    last storyboard
    
    func setLastStoryboard(_ s: String) {
        set("PrefRepo@lastStoryboard", s)
    }
    
    func getLastStoryboard() -> String? {
        return get(key: "PrefRepo@lastStoryboard")
    }
    
    func setLastClosetIndex(_ i: Int?) {
        let key = "PrefRepo@setLastClosetIndex"
        if let index = i {
            set(key, index)
        } else {
            remove(key)
        }
    }
    
    func getLastClosetIndex() -> Int {
        let key = "PrefRepo@setLastClosetIndex"
        return get(key)
    }
    
//    token
    
    func getDeviceToken() -> String? {
        return pref.string(forKey: "PrefRepo@deviceToken")
    }
    
    func setDeviceToken(v: String)
    {
        let oldToken: String! = getDeviceToken()
        
        if oldToken == nil || oldToken != v
        {
            set("PrefRepo@deviceToken", v)
            
            setDeviceTokenSent(false)
        }
    }
    
    func wasDeviceTokenSent() -> Bool {
        return get("PrefRepo@isDeviceTokenSent")
    }
    
    func setDeviceTokenSent(_ option: Bool) {
        set("PrefRepo@isDeviceTokenSent", option)
    }
    
}

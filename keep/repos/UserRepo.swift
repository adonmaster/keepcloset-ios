import CoreData
import UIKit

class UserRepo: BaseRepo<User> {
    
    let EV_USER_CHANGED = "userChanged"
    
    enum OAuthServer: String {
        case fb = "fb"
        case google = "google"
        case own = "own"
    }
    
    func save(email: String, name: String, server: OAuthServer, avatar: URL?)
    {
        let model = _first(condition: "email = %@", args: [email]) ?? User(context: context)
        model.email = email
        model.oauth_server = server.rawValue
        model.is_beta = email == Env.beta_user_email
        model.name = name
        model.avatar_thumb_url = nil
        model.avatar_url = nil
        model.active = true
        
        try? context.save()
        
        inactivateOtherUsers(current: model)
        
        event.fire(key: EV_USER_CHANGED, payload: model)
        
        if let a = avatar {
            asyncSaveAvatar(avatar: a, model: model)
        }
    }
    
    // inactivate other users
    private func inactivateOtherUsers(current: User) {
        for user in _get(limit: nil) {
            if current.email! != user.email {
                user.active = false
                try? context.save()
            }
        }
    }
    
    func active() -> User? {
        return _first(condition: "active = 1")
    }

    private func asyncSaveAvatar(avatar: URL, model: User)
    {
        Http.shared.downloadImage(url: avatar) { image in
            
            guard let img = image else { return }
            
            self.save(user: model, avatar: img)
        }
    }
    
    func save(user: User, avatar img: UIImage, onDone: (()->Void)?=nil)
    {
        let full = img
        var filename = "avatar.jpg"
        switch F.app.write(image: full, filename: filename, ext: .jpeg) {
        case .error(let reason): self.fireError(reason: reason); break
        case .success(_): break
        }
        user.avatar_url = F.app.dynamicPath(filename: filename)
        
        let thumb = Img.init(img).limit(.width(80)).get()
        filename = "avatar_thumb.jpg"
        switch F.app.write(image: thumb, filename: filename) {
        case .error(let reason): self.fireError(reason: reason); break
        case .success(_): break
        }
        user.avatar_thumb_url = F.app.dynamicPath(filename: filename)
        
        self.context.perform {
            try? self.context.save()
            
            onDone?()
            self.event.fire(key: self.EV_USER_CHANGED, payload: user)
        }
    }
    
    func removeAvatar(user: User) {
        user.avatar_url = nil
        user.avatar_thumb_url = nil
        
        try? self.context.save()
        
        self.event.fire(key: self.EV_USER_CHANGED, payload: user)
    }
    
    func save(user: User, name: String, onDone: (()->Void)?=nil)
    {
        user.name = name
        context.perform {
            try? self.context.save()
            
            onDone?()
            self.event.fire(key: self.EV_USER_CHANGED, payload: user)
        }
    }
}

// MARK: listeners
extension UserRepo {
    
    func listenUserChanged(caller: EventableDelegate) {
        event.listen(key: EV_USER_CHANGED, caller: caller)
    }
    
    func unlistenUserChanged(caller: EventableDelegate) {
        event.unlisten(caller: caller, key: EV_USER_CHANGED)
    }
    
}

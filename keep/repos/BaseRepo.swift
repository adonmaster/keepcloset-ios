import CoreData
import UIKit

class BaseRepo<T: NSManagedObject> {
    
    var context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let event = Eventable()
    
    let EV_ON_ERROR = "BaseRepo@onError"
    
    func _first(condition: String?=nil, args: [Any]?=nil, sort: [NSSortDescriptor]?=nil) -> T?
    {
        return _get(limit: 1, condition: condition, args: args, sort: sort).first
    }
    
    func _get(limit: Int?=nil, condition: String?=nil, args: [Any]?=nil, sort: [NSSortDescriptor]?=nil) -> [T]
    {
        let request = buildRequest(condition, args, sort)
        if let l = limit {
            request.fetchLimit = l
        }
        
        return (try? context.fetch(request)) ?? []
    }

    func _count(condition: String?=nil, args: [Any]?=nil) -> Int
    {
        let request = buildRequest(condition, args, nil)
        return (try? context.count(for: request)) ?? 0
    }
    
    func _exists(condition: String?=nil, args: [Any]?=nil) -> Bool
    {
        return _count(condition: condition, args: args) > 0
    }
    
    func _delete(_ item: T)
    {
        context.delete(item)
        
        try? context.save()
    }
    
    func _delete(where condition: String?=nil, args: [Any]?=nil)
    {
        let classString = String(describing: T.self)
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: classString);
        if let c = condition {
            fetch.predicate = NSPredicate(format: c, argumentArray: args)
        }
        
        let r = NSBatchDeleteRequest(fetchRequest: fetch);
        try! context.execute(r);
    }
    
    func _deleteAll() {
        _delete()
    }
    
    func _distinct(field: String, _ condition: String?=nil, _ args: [Any]?=nil) -> [String]
    {
        let request: NSFetchRequest<NSFetchRequestResult> = T.fetchRequest()
        request.resultType = .dictionaryResultType
        request.returnsDistinctResults = true
        request.propertiesToFetch = [field]
        if let c = condition {
            request.predicate = NSPredicate(format: c, argumentArray: args)
        }
        
        if let list = try? context.fetch(request) as? [[String:String]] {
            var r: [String] = []
            for item in list {
                if let result = item[field] {
                    r.append(result)
                }
            }
            return Arr(r).distinct { $0 }
        }
        
        return []
    }
    
    private func buildRequest(_ condition: String?, _ args: [Any]?, _ sort: [NSSortDescriptor]?) -> NSFetchRequest<T>
    {
        let request: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        if let c = condition {
            request.predicate = NSPredicate(format: c, argumentArray: args)
        }
        request.sortDescriptors = sort
        return request;
    }
    
    func _buildQuery(condition: String, args: [Any], q: String?, fieldsToQuery: [String], sort: [NSSortDescriptor]) -> [T]
    {
        let list = (q ?? "")
            .components(separatedBy: CharacterSet(charactersIn: " .-;,"))
            .map({ $0.trim() })
            .compactMap({ $0.count > 0 ? $0 : nil })
        
        var innerSql: [[String]] = []
        var innerParams: [Any] = []
        for qStr in list {
            innerSql.append(fieldsToQuery.map({ "\($0) CONTAINS[cd] %@" }))
            fieldsToQuery.forEach({ _ in innerParams.append(qStr) })
        }
        
        let sqlAddon: String = innerSql
            .map({ $0.map({ "(\($0))" }).joined(separator: " or ") })
            .map({ "(\($0))" }).joined(separator: " and ")
        
        let sql = condition + (sqlAddon.count > 0 ? " AND (\(sqlAddon))" : "")
        
        return self._get(condition: sql, args: args + innerParams, sort: sort)
    }
    
    func listenOnError(caller: EventableDelegate) {
        event.listen(key: EV_ON_ERROR, caller: caller)
    }
    
    func fireError(reason: String?) {
        event.fire(key: EV_ON_ERROR, payload: reason)
    }
    
    func unlistenOnError(caller: EventableDelegate) {
        event.unlisten(caller: caller, key: EV_ON_ERROR)
    }
    
}

import UIKit
import CoreData

class LuggageItemRepo: BaseRepo<LuggageItem> {
    
    func remove(_ model: LuggageItem) {
        self._delete(model)
    }
    
}

import UIKit
import CoreData

class StatsRepo {
    
    // variables
    private var context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // methods
    
    func looksTotal(for user: User) -> Int
    {
        let request: NSFetchRequest<Look> = _buildRequest("user = %@", [user], nil)
        return (try? context.count(for: request)) ?? 0
    }
   
    func looksItensAvarage(for user: User) -> Int
    {
        let request: NSFetchRequest<NSFetchRequestResult> = Look.fetchRequest()
        request.resultType = .dictionaryResultType
        
        let count = NSExpressionDescription()
        count.expression = NSExpression(forKeyPath: "items.@count")
        count.name = "count"
        count.expressionResultType = .integer16AttributeType
        
        request.propertiesToFetch = ["category", count]
        request.predicate = NSPredicate(format: "user = %@ AND items.@count > 0", argumentArray: [user])
        
        let list = (((try? context.fetch(request) as? [[String: Any]]) as [[String : Any]]??)) ?? []
        
        // calculate avarage
        return Int(floor(list!.map({ $0["count"] as! Int }).average))
    }
    
    private func queryLooksItemsMostUsed(_ user: User) -> NSFetchRequest<Item>
    {
        let request: NSFetchRequest<Item> = Item.fetchRequest()
        request.predicate = NSPredicate(format: "user = %@ AND statLookItems.@count > 0", user)
        
        return request
    }
    
    func looksItemsMostUsedCount(for user: User) -> Int
    {
        return (try? context.count(for: queryLooksItemsMostUsed(user))) ?? 0
    }
    
    func looksItemsMostUsed(for user: User) -> [Item]
    {
        var results = (try? context.fetch(queryLooksItemsMostUsed(user))) ?? []
        results = results.sorted(by: { a, b -> Bool in
            return (a.statLookItems?.count ?? 0) > (b.statLookItems?.count ?? 0)
        })
        
        return Array(results.prefix(10))
    }
    
    private func queryLooksItemsNeverUsed(_ user: User) -> NSFetchRequest<Item>
    {
        let request: NSFetchRequest<Item> = Item.fetchRequest()
        request.predicate = NSPredicate(format: "user = %@ AND statLookItems.@count = 0", user)
        return request
    }
    
    func looksItemsNeverUsedCount(for user: User) -> Int {
        return (try? context.count(for: queryLooksItemsNeverUsed(user))) ?? 0
    }
    
    func looksItemsNeverUsed(for user: User) -> [Item] {
        return (try? context.fetch(queryLooksItemsNeverUsed(user))) ?? []
    }
    
    /// *****
    
    func closetTotal(for user: User) -> Int {
        let request: NSFetchRequest<Item> = Item.fetchRequest()
        request.predicate = NSPredicate(format: "user = %@", user)
        return (try? context.count(for: request)) ?? 0
    }
    
    private func clobDictionary(_ d: [String:Int], _ limit: Int=6) -> [String:Int]
    {
        let count = d.count
        if (limit <= 1 || count <= limit) { return d }
        
        let list = d.map({ k, v in (k, v) })
            .sorted(by: { a, b in a.1 > b.1 })
        
        let prefix = list.prefix(limit-1)
        let suffix = list.suffix(from: count-limit-1)

        var r: [String:Int] = [:]
        prefix.forEach { r[$0.0] = $0.1 }
        r["* Outros"] = suffix.reduce(0, { sum, i in sum + i.1 })
        
        return r
    }
    
    func closetColors(for user: User) -> [String:Int]
    {
        var r: [String:Int] = [:]
        
        let items = Repo.item.query(user: user, categories: [], colors: [], limit: 10_000_0000)
        items.forEach { i in
            let cl = (i.color1_desc ?? "-").trim().lowercased()
            r[cl] = (r[cl] ?? 0) + 1
        }
        
        return clobDictionary(r, 25);
    }
    
    func closetStatus(for user: User) -> [String:Int]
    {
        var r: [String:Int] = [:]
        
        let items = Repo.item.query(user: user, categories: [], colors: [], limit: 10_000_0000)
        items.forEach { i in
            let cl = (i.status ?? "-").trim().lowercased()
            r[cl] = (r[cl] ?? 0) + 1
        }
        
        return clobDictionary(r, 25);
    }
    
    func closetTherm(for user: User) -> [String:Int]
    {
        var r: [String:Int] = [:]
        
        let items = Repo.item.query(user: user, categories: [], colors: [], limit: 10_000_0000)
        items.forEach { i in
            let cl = (Res.i.therm.first(where: { $0.uid == i.therm_uid })?.name ?? "-")
                .trim().lowercased()
            r[cl] = (r[cl] ?? 0) + 1
        }
        
        return clobDictionary(r, 25)
    }
    
    func closetBrand(for user: User) -> [String:Int]
    {
        var r: [String:Int] = [:]
        
        let items = Repo.item.query(user: user, categories: [], colors: [], limit: 10_000_0000)
        items.forEach { i in
            let cl = (i.brand ?? "-").trim().lowercased()
            r[cl] = (r[cl] ?? 0) + 1
        }
        
        return clobDictionary(r, 25);
    }
    
    // closet calendar
    
    func closetScheduleMost(for user: User) -> [Item]
    {
        let list = Repo.item.query(user: user, q: nil)
            .sorted { $0.scheduled!.count > $1.scheduled!.count }
        
        return Array(list)
    }
    
}

// extensions
fileprivate extension Collection where Element: Numeric {
    var total: Element { return reduce(0, +) }
}

fileprivate extension Collection where Element: BinaryInteger {
    var average: Double { return isEmpty ? 0 : Double(total) / Double(count) }
}

fileprivate extension Collection where Element: BinaryFloatingPoint {
    var average: Element { return isEmpty ? 0 : total / Element(count) }
}

// **
extension StatsRepo {
    
    private func _buildRequest<T: NSManagedObject>(_ condition: String?, _ args: [Any]?, _ sort: [NSSortDescriptor]?) -> NSFetchRequest<T>
    {
        let request: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        if let c = condition {
            request.predicate = NSPredicate(format: c, argumentArray: args)
        }
        request.sortDescriptors = sort
        return request;
    }
    
    private func _groupBy(_ cl: NSManagedObject.Type, field: String, _ condition: String?=nil, _ args: [Any]?=nil) -> [String]
    {
        let request: NSFetchRequest<NSFetchRequestResult> = cl.fetchRequest()
        request.resultType = .dictionaryResultType
        request.propertiesToGroupBy = [field]
        request.propertiesToFetch = [field]
        if let c = condition {
            request.predicate = NSPredicate(format: c, argumentArray: args)
        }
        
        if let list = (((try? context.fetch(request) as? [[String : String]]) as [[String : String]]??)) ?? [] {
            var r: [String] = []
            for item in list {
                if let result = item[field] {
                    r.append(result)
                }
            }
            return Arr(r).distinct { $0 }
        }
        
        return []
    }
    
}

import UIKit
import CoreData

class LookRepo: BaseRepo<Look> {
    
    // private repo
    private let itemRepo = LookItemRepo()

    func saveFrom(modelLook: Look?, user: User, category: String?, imgs: [ImageViewEx], bknd: Int16, full: UIImage, thumb: UIImage) -> Look
    {
        let parent = modelLook ?? Look(context: context)
        parent.user = user
        parent.created_at = parent.created_at ?? Carbon.init().get()
        parent.category = category
        parent.bknd = bknd
        parent.full = full.jpegData(compressionQuality: 0.9)
        parent.thumb = thumb.jpegData(compressionQuality: 0.9)
        parent.updated_at = Carbon.init().get()
        
        // first commit
        try? context.save()
        
        // then affect other models
        self.itemRepo.sync(parent, imgs)
        
        return parent
    }
    
    
    func listCategories(user: User) -> [String]
    {
        return Arr(self._distinct(field: "category", "user = %@", [user]))
            .distinct({ $0 })
    }
    
    func query(for user: User, q: String?) -> [Look]
    {
        let fields = ["category"]
        let sort = [NSSortDescriptor(key: "updated_at", ascending: false)]
        
        return self._buildQuery(condition: "user = %@", args: [user], q: q, fieldsToQuery: fields, sort: sort)
    }
    
    func getListFor(user: User) -> [Look] {
        return self._get(condition: "user = %@", args: [user], sort: [
            NSSortDescriptor(key: "category", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:))),
            NSSortDescriptor(key: "created_at", ascending: false)
        ])
    }
    
    func getItemsfor(parent: Look) -> [LookItem] {
        return self.itemRepo.getFor(parent: parent)
    }
    
    func remove(_ item: Look) {
        self._delete(item)
    }
    
}

import UIKit
import CoreData

class ItemRepo: BaseRepo<Item> {
    
    @discardableResult func save(itemModel: Item?, user: User, category: String?, thermId: Int, img: UIImage, thumb: UIImage,
              color1: UIColor?, color2: UIColor?, brand: String?, status: String?) -> Item
    {
        let model = itemModel ?? Item(context: context)
        model.user = user
        model.category = category
        model.img = img.pngData()
        model.thumb = thumb.pngData()
        model.therm_uid = Int16(thermId)

        model.color1 = Cl.toHexOp(color: color1)
        model.color2 = Cl.toHexOp(color: color2)

        model.color1_desc = Res.i.colorNameFrom(cl: color1)
        model.color2_desc = Res.i.colorNameFrom(cl: color2)

        model.brand = brand
        model.status = status
        model.created_at = model.created_at ?? Carbon().get()
        
        try? context.save()
        
        // after math
        self.onChanged(user: user)
        
        return model
    }
    
    func query(user: User, q: String?) -> [Item]
    {
        let fields = ["category", "brand", "status", "color1_desc", "color2_desc"]
        let sort = [
            NSSortDescriptor(key: "category", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:))),
            NSSortDescriptor(key: "created_at", ascending: false)
        ]
        
        return self._buildQuery(condition: "user = %@", args: [user], q: q, fieldsToQuery: fields, sort: sort)
    }
    
/**
    func query(user: User, q: String?) -> [Item]
    {
        let list = (q ?? "")
            .components(separatedBy: CharacterSet(charactersIn: " .-;,"))
            .map({ $0.trim() })
            .compactMap({ $0.count > 0 ? $0 : nil })
        
        var sql = "user = %@"
        var innerSql = ""
        var innerParams: [Any] = []
        for s in list {
            if innerSql.count > 0 { innerSql += " and " }
            innerSql += """
                ((category CONTAINS[cd] %@)
                or (brand CONTAINS[cd] %@)
                or (status CONTAINS[cd] %@)
                or (color1_desc CONTAINS[cd] %@)
                or (color2_desc CONTAINS[cd] %@))
            """
            innerParams.append(contentsOf: [s, s, s, s, s])
        }
        if innerSql.count > 0 { sql = sql + " AND (\(innerSql.trim()))" }
        
        return self._get(condition: sql, args: [user]+innerParams, sort: [NSSortDescriptor(key: "created_at", ascending: false)])
    }
*/
    
    // deprecated
    func query(user: User, categories: [String], colors: [UIColor], limit: Int=10000) -> [Item]
    {
        var argList: [Any] = [user]
        
        // category
        let categorySql: String = categories.reduce("", { r, s in
            var res = r.count > 0 ? " OR " : ""
            res += "category ==[c] %@"
            
            argList.append(s)
            
            return r + res
        })
        
        // color
        let colorSql = colors.map({ Cl.toHex(color: $0) }).reduce("", {r, s in
            var res = r.count > 0 ? " OR " : ""
            res += "(color1 ==[c] %@ OR color2 ==[c] %@)"
            
            argList.append(contentsOf: [s, s])
            
            return r + res
        })
        
        var sql = "user == %@"
        if categorySql.count > 0 {
            sql += " AND (\(categorySql))"
        }
        if colorSql.count > 0 {
            sql += " AND (\(colorSql))"
        }
        
        return self._get(limit: limit, condition: sql, args: argList, sort: [NSSortDescriptor(key: "created_at", ascending: false)])
    }
    
    func listStatus(user: User) -> [String]
    {
        let list = self._distinct(field: "status", "user = %@", [user])
        
        return Arr(list).union(["Emprestado", "Desapego"])
    }
    
    func listBrand(user: User) -> [String]
    {
        return Arr(self._distinct(field: "brand", "user = %@", [user])).distinct { $0 }
    }
    
    func listCategories(user: User) -> [String]
    {
        let list = self._distinct(field: "category", "user = %@", [user])
        
        return Arr(list).union(["Camisa", "Calça", "Saia", "Vestido"])
    }
    
    func countCategories(user: User, category: String) -> Int {
        return self._count(condition: "user = %@ AND category ==[c] %@", args: [user, category])
    }
    
    func remove(_ item: Item)
    {
        // delete from db
        self._delete(item)
        
        // after math
        if let user = item.user {
            onChanged(user: user)
        }
    }
    
    func onChanged(user: User) {
        
    }
    
}

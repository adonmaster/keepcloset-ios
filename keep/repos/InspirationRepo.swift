import UIKit
import CoreData

class InspirationRepo: BaseRepo<Inspiration> {
    
    func get(for user: User) -> [Inspiration] {
        return self._get(condition: "user = %@", args: [user], sort: [
            NSSortDescriptor(key: "category", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:))),
            NSSortDescriptor(key: "created_at", ascending: false)
        ])
    }
    
    func query(for user: User, q: String?) -> [Inspiration]
    {
        let fields = ["desc", "category"]
        let sort = [
            NSSortDescriptor(key: "category", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:))),
            NSSortDescriptor(key: "created_at", ascending: false)
        ]
        
        return self._buildQuery(condition: "user = %@", args: [user], q: q, fieldsToQuery: fields, sort: sort)
    }
    
    
    @discardableResult func save(model: Inspiration?, user: User, category: String?, color1: UIColor?, color2: UIColor?,
              img: UIImage, thumb: UIImage, desc: String?) -> Inspiration
    {
        let m = model ?? Inspiration(context: context)
        
        m.user = user
        m.category = category
        m.color1 = Cl.toHexOp(color: color1)
        m.color2 = Cl.toHexOp(color: color2)
        m.created_at = m.created_at ?? Carbon().get()
        m.img = img.jpegData(compressionQuality: 0.9)
        m.thumb = img.jpegData(compressionQuality: 0.9)
        m.desc = desc
        
        try? context.save()
        
        return m
    }
    
    func listCategories(for user: User) -> [String] {
        return Arr(self._distinct(field: "category", "user = %@", [user]))
            .distinct({ $0 })
    }
    
    func remove(model: Inspiration) {
        self._delete(model)
    }
    
}

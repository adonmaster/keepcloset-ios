import UIKit
import CoreData

class ScheduleImgRepo: BaseRepo<ScheduleImg> {
    
    func getFor(user: User, date: Date) -> [ScheduleImg] {
        return self._get(
            condition: "user = %@ AND date == %@",
            args: [user, Carbon(date).formatSqlDate()]
        )
    }
    
    func get(user: User, forMonth dt: Date) -> [ScheduleImg] {
        return self._get(
            condition: "user = %@ AND date BEGINSWITH[cd] %@",
            args: [user, Carbon(dt).formatSqlMonth()]
        )
    }
    
    func createFor(user: User, date: Date, img: UIImage, thumb: UIImage, color1: String?=nil, color2: String?=nil, item: Item?, look: Look?) -> ScheduleImg
    {
        let model = ScheduleImg(context: self.context)
        model.user = user
        model.date = Carbon(date).formatSqlDate()
        model.color1 = color1
        model.color2 = color2
        model.img = img.pngData()
        model.thumb = thumb.pngData()
        
        model.item = item
        model.look = look
        
        user.addToScheduleImgs(model)
        
        try? context.save()
        
        return model
    }
    
    func updateColors(model: ScheduleImg, color1: String?, color2: String?)
    {
        model.color1 = color1
        model.color2 = color2
        
        try? context.save()
    }
    
    func remove(_ model: ScheduleImg) {
        self._delete(model)
    }
    
}
    


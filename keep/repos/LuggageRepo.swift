import UIKit
import CoreData

class LuggageRepo: BaseRepo<Luggage> {
    
    struct LuggageItemSudo {
        
        init(model: LuggageItem?, title: String, desc: String, thumb: UIImage, img: UIImage?,
             item: Item?, look: Look?, isPacked: Bool)
        {
            self.model = model
            self.title = title
            self.desc = desc
            self.thumb = thumb
            self.img = img
            self.item = item
            self.look = look
            self.isPacked = isPacked
        }
        
        let model: LuggageItem?
        let title: String
        let desc: String
        let thumb: UIImage
        let img: UIImage?
        let item: Item?
        let look: Look?
        let isPacked: Bool
        
        func loadImg(_ cb: @escaping (UIImage)->()) {
            if let i = img {
                cb(i)
            } else if let i = item {
                i.p.fullLoad(cb: cb)
            } else if let i = look {
                i.p.loadFull(cb)
            } else {
                cb(#imageLiteral(resourceName: "404"))
            }
        }
        
    }
    
    let itemRepo = LuggageItemRepo()

    
    // methods
    
    func getList(user: User) -> [Luggage] {
        return self._get(
            condition: "user == %@",
            args: [user],
            sort: [NSSortDescriptor(key: "dated_at", ascending: false)]
        )
    }
    
    
    func save(model: Luggage?, user: User, location: String, datedAt: Date, duration: String, items: [LuggageItemSudo]) -> Luggage
    {
        let model = model ?? Luggage(context: self.context)
        model.user = user
        model.location = location
        model.dated_at = datedAt
        model.duration = duration
        
        try? context.save()

        // items
        saveItems(parent: model, items: items)
        
        return model
    }
    
    func cloneAll(_ original: Luggage) -> Luggage
    {
        let new = original.clone(self.context)
        for child in getItemsInline(parent: original) {
            _ = child.cloneFor(context: self.context, parent: new)
        }
        
        try? context.save()
        return new
    }
    
    func delete(model: Luggage)
    {
        // remove items
        let items: [LuggageItem] = (model.items?.array as? [LuggageItem]) ?? []
        for item in items {
            itemRepo.remove(item)
        }
        
        self._delete(model)
    }
    
    private func saveItems(parent: Luggage, items: [LuggageItemSudo])
    {
        for item in items
        {
            let model = item.model ?? LuggageItem(context: self.context)
            
            model.parent = parent
            model.title = item.title
            model.desc = item.desc
            model.is_packed = item.isPacked
            model.thumb = item.thumb.pngData()
            model.img = item.img?.pngData()
            model.rel_item = item.item
            model.rel_look = item.look
        }
        
        try? context.save()
    }
    
    func itemPack(_ item: LuggageItemSudo, option: Bool) {
        if let model = item.model {
            model.is_packed = option
            try? context.save()
        }
    }
    
    func deleteItems(items: [LuggageItemSudo]) {
        for item in items {
            if let model = item.model {
                itemRepo.remove(model)
            }
        }
    }
    
    
    func getItems(parent: Luggage, cb: @escaping ([LuggageItemSudo])->())
    {
        let items = parent.items?.array as! [LuggageItem]
        
        Task.main {
            
            let r = items.map({ i -> LuggageRepo.LuggageItemSudo in
                
                let thumbData = i.thumb
                let thumb: UIImage! = thumbData==nil ? nil : UIImage(data: thumbData!)
                
                let imgData = i.img
                let img: UIImage! = imgData==nil ? nil : UIImage(data: imgData!)
                
                return LuggageItemSudo(
                    model: i,
                    title: i.p.title,
                    desc: i.desc ?? "Desconhecido",
                    thumb: thumb,
                    img: img,
                    item: i.rel_item,
                    look: i.rel_look,
                    isPacked: i.is_packed
                )
            })
            
            cb(r)
            
        }
    }
    
    func countItemPacked(parent: Luggage) -> Int
    {
        return self.itemRepo
            ._count(condition: "parent = %@ AND is_packed = %@", args: [parent, true])
    }
    
    private func getItemsInline(parent: Luggage) -> [LuggageItem] {
        return (parent.items?.array as? [LuggageItem]) ?? []
    }
    
}


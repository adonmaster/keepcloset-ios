class Tap<T: AnyObject> {
    
    private let obj: T
    
    init(_ obj: T) {
        self.obj = obj
    }
    
    func with(_ cb: (T)->Void) -> T {
        cb(obj)
        return obj;
    }
    
}

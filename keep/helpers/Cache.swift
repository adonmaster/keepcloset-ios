import UIKit

class Cache {
    
    static let i = Cache()
    
    private struct Item {
        let v: Any
        let expiredAt: Date
    }
    
    private var cache: [String:Item] = [:]
    
    func retrieve<T>(key: String, _ cb: ()->T) -> T
    {
        // go on
        if let i = cache[key], let v = i.v as? T {
            return v
        }
        
        let r = cb()
        cache[key] = Item(v: r, expiredAt: Carbon().add(6, .minute).get())
        
        return r
    }

    func clear() {
        cache = [:]
    }
    
    
}

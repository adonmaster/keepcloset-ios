import Alamofire
import UIKit
import Kingfisher

class Http {
    
    static let shared = Http()
    private init() {}
    
    let debug = false
    
    func url(_ uri: String) -> String {
        if uri.starts(with: "http") { return uri }
        return "http://keepcloset.com.br/\(uri)"
    }
    

    func downloadImage(url: URL?, cb: @escaping(UIImage?)->Void) {
        if let u = url {
            ImageDownloader.default.downloadImage(with: u, completionHandler:  { r in
                switch r {
                case .success(let data):
                    cb(data.image)
                case .failure:
                    cb(nil)
                }
            })
        }
    }
    
    func get<M: Decodable>(_ modelType: M.Type, uri: String, params: [String:String]) -> PromiseEx<M> {
        return PromiseEx { resolve, reject in
            AF.request(
                self.url(uri), method: .get, parameters: params,
                headers: self.generateHeader()
            )
                .validate(contentType: ["application/json"])
                .responseData(completionHandler: self.processResponse(uri: uri, resolve: resolve, reject: reject))
        }
    }
    
    func post<F: Encodable, M: Decodable>(_ modelType: M.Type, uri: String, _ param: F) -> PromiseEx<M> {
         return PromiseEx { resolve, reject in
             AF.request(
                 self.url(uri), method: .post, parameters: param,
                 encoder: JSONParameterEncoder.default,
                 headers: self.generateHeader()
             )
                 .validate(contentType: ["application/json"])
                 .responseData(completionHandler: self.processResponse(uri: uri, resolve: resolve, reject: reject))
         }
     }

     
    func postPut<F: Encodable, M: Decodable>(_ modelType: M.Type, uri: String, _ param: F?) -> PromiseEx<M> {
        return PromiseEx { resolve, reject in
            AF.request(
                self.url(uri), method: .put, parameters: param,
                encoder: JSONParameterEncoder.default,
                headers: self.generateHeader()
            )
              .validate(contentType: ["application/json"])
              .responseData(completionHandler: self.processResponse(uri: uri, resolve: resolve, reject: reject))
        }
    }
     
    func postDelete<F: Encodable, M: Decodable>(_ modelType: M.Type, uri: String, _ param: F?) -> PromiseEx<M> {
        return PromiseEx { resolve, reject in
            AF.request(
                self.url(uri), method: .delete, parameters: param,
                encoder: JSONParameterEncoder.default,
                headers: self.generateHeader()
            )
              .validate(contentType: ["application/json"])
              .responseData(completionHandler: self.processResponse(uri: uri, resolve: resolve, reject: reject))
        }
    }
    
    enum ParamValue {
        case jpeg(UIImage)
        case png(UIImage)
        case string(String)
        case json(Data)
        case pdf(Data)
        case int(Int)
        case float(Float)
    }
    
    func upload<M: Decodable>(_ modelType: M.Type, uri: String, params: [String:ParamValue]) -> PromiseEx<M> {
        return PromiseEx { resolve, reject in

            let multipart: (MultipartFormData)->Void = { m in
                for (field, val) in params {
                    switch val {
                    case .jpeg(let img):
                        let data = img.jpegData(compressionQuality: 0.9)!
                        m.append(data, withName: field, fileName: "\(field).png", mimeType: "image/jpeg")
                    case .png(let img):
                        m.append(img.pngData()!, withName: field, fileName: "\(field).png", mimeType: "image/png")
                    case .string(let s):
                        m.append(s.data(using: .utf8)!, withName: field, mimeType: "text/plain")
                    case .json(let data):
                        m.append(data, withName: field, mimeType: "application/json")
                    case .pdf(let data):
                        m.append(data, withName: field, mimeType: "application/pdf")
                    case .int(let i):
                        m.append("\(i)".data(using: .utf8)!, withName: field, mimeType: "text/plain")
                    case .float(let f):
                        m.append("\(f)".data(using: .utf8)!, withName: field, mimeType: "text/plain")
                    }
                }
            }
            
            // url
            let thre = UInt64.init()
            AF.upload(
                    multipartFormData: multipart, to: self.url(uri), usingThreshold: thre,
                    method: .post, headers: self.generateHeader()
                )
                .responseData(completionHandler: self.processResponse(uri: uri, resolve: resolve, reject: reject))
        }
    }

    private func processResponse<M: Decodable>(uri: String, resolve: @escaping (M)->(), reject: @escaping (String)->())
        -> (AFDataResponse<Data>) -> Void
    {
        return { r in
            switch r.result {
            case .success(let data):
                do {
                    
                    if self.debug, let responseString = String(data: data, encoding: .utf8) {
                        print("---HTTPREQUEST:GET:\(uri) -> "+responseString)
                    }
                    
                    let model = try JSONDecoder().decode(M.self, from: data)
                    resolve(model)
                } catch {
                    reject(error.localizedDescription)
                }
            case .failure(let err):
                reject(err.localizedDescription)
            }
        }
    }

    private func generateHeader(token: String?=nil) -> HTTPHeaders {
        var arr: [String: String] = [
            "Accept": "application/json"
        ]
        if let token = token {
            arr["Authorization"] = "Bearer \(token)"
        }
        return HTTPHeaders(arr)
    }
    
}

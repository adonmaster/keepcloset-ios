import UIKit
import Photos
import CropViewController

class ImgProviderNative {
    
    static let i = ImgProviderNative()
    
    func presentPicker(
        caller: ImgProviderNativeDelegate,
        _ source: UIImagePickerController.SourceType
    )
    {
        validatePermission(caller: caller) {
            
            reallyPresent(caller, source)
            
        }

    }
    
    func validatePermission(caller: UIViewController, onSafe: ()->()) {
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            PHPhotoLibrary.requestAuthorization { status in
                if status != .authorized {
                    Dialog(sender: caller)
                    .msg("Habilite acesso ao album de fotos e/ou câmera")
                    .ok().present()
                }
            }
        } else {
            onSafe()
        }
    }
    
    private func reallyPresent(
        _ caller: ImgProviderNativeDelegate,
        _ source: UIImagePickerController.SourceType
    ) {
        guard UIImagePickerController.isSourceTypeAvailable(source) else {
            Task.main(delayInSeconds: 1, block: {
                let msg = source != .camera
                    ? "Seu dispositivo não tem suporte ao album."
                    : "Seu dispositivo não tem suporte à câmera."

                Dialog(sender: caller).msg(msg).ok().present()
            })
            return
        }
        
        let picker = UIImagePickerController()
        picker.allowsEditing = false // we will use a third part library for that
        picker.sourceType = source
        picker.delegate = caller
        
        caller.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(
        _ caller: ImgProviderNativeDelegate,
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    )
    {
        picker.dismiss(animated: true, completion: nil)
        
        Task.main {
            
            // edited? never
            // not edited but sized?
            if let limit = caller.imgProviderNativeLimit() {
                if let pha = info[.phAsset] as? PHAsset {
                    self.requestFromPicker(pha, size: limit)
                        .then { img in
                            self.imgChoose(caller, img: img)
                        }
                        .catchIt { _ in }
                } else if let img = info[.editedImage] as? UIImage {
                    self.imgChoose(caller, img: Img(img).limit(limit).get())
                } else if let img = info[.originalImage] as? UIImage {
                    self.imgChoose(caller, img: Img(img).limit(limit).get())
                }
            }
            else {
                if let img = info[.editedImage] as? UIImage {
                    self.imgChoose(caller, img: img)
                } else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.imgChoose(caller, img: img)
                }
            }
            
        }
            
    } // func
    
    private func imgChoose(_ caller: ImgProviderNativeDelegate, img: UIImage) {
        if caller.imgProviderNativeAllowsEditing() {
            
            let vc = CropViewController(image: img)
            vc.delegate = caller
            caller.present(vc, animated: true, completion: nil)
            
        } else {
            caller.imgProviderNative(onImgs: [img])
        }
    }
    
    func cropViewController(
        _ caller: ImgProviderNativeDelegate,
        _ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int
    )
    {
        cropViewController.dismiss(animated: true, completion: nil)
        caller.imgProviderNative(onImgs: [image])
    }
    
    
    //***
    private func requestFromPicker(_ phasset: PHAsset?, size: Img.LimitType?=nil) -> PromiseEx<UIImage> {
        return PromiseEx { resolve, reject in
            
            guard let pha = phasset else {
                reject("Empty resource")
                return
            }
            
            var w = CGFloat(pha.pixelWidth)
            var h = CGFloat(pha.pixelHeight)
            let ratio = w/h
            if let sz = size {
                switch sz {
                case .width(let v):
                    w = w > v ? v : w
                    h = w / ratio
                case .height(let v):
                    h = h > v ? v : h
                    w = ratio * h
                }
            }
            
            let size = CGSize(width: w, height: h)
            let options = PHImageRequestOptions()
            options.isSynchronous = true
            Task.main {
                PHImageManager.default()
                    .requestImage(for: pha, targetSize: size, contentMode: .default, options: options) { img, dict in
                        if img == nil {
                            reject("Empty data")
                        } else {
                            resolve(img!)
                        }
                }
            }
            
        } // promise
    } // func
    
}

protocol ImgProviderNativeDelegate:
    UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate
{
    func imgProviderNative(onImgs imgs: [UIImage])
    func imgProviderNativeLimit() -> Img.LimitType?
    func imgProviderNativeAllowsEditing() -> Bool
    
    /**
     required stub:
     
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
     {
          ImgProviderNative.i.imagePickerController(self, picker, didFinishPickingMediaWithInfo: info)
      }
     
      func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int)
      {
          ImgProviderNative.i.cropViewController(self, cropViewController, didCropToImage: image, withRect: cropRect, angle: angle)
      }
     
     */
}

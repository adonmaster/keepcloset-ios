import Foundation

class Ev {
    
    public typealias Cb = (Any?)->();
    
    private static var listeners: [String: [NSObject:Cb]] = [:];
    
    public class func listen(key: String, receiver: NSObject, cb: @escaping Cb)
    {
        if (!listeners.keys.contains(key))
        {
            listeners[key] = [receiver:cb];
        }
            
        else
        {
            listeners[key]![receiver] = cb;
        }
    }
    
    public class func unlisten(key: String, receiver: NSObject?=nil)
    {
        guard listeners[key] != nil else { return }
        
        if (receiver==nil)
        {
            listeners.removeValue(forKey: key)
        }
            
        else
        {
            listeners[key]!.removeValue(forKey: receiver!)
        }
    }
    
    public class func fire(key: String, payload: Any?=nil) {
        Task.main {
            listeners[key]?.values.forEach { $0(payload) }
        }
    }
    
    public class func fireSync(key: String, payload: Any?=nil) {
        listeners[key]?.values.forEach { $0(payload) }
    }
    
    
}

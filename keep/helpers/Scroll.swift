import UIKit

/**
extension ScrollVC: UIScrollViewDelegate {

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageview
    }

    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        scrollCenterImage(sv: scrollView, iv: imageview)
    }

}
 
// later on image assignment
{
    imageview.image = img

    scrollSetupZoom(sv: scrollview, iv: imageview)
    scrollCenterImage(sv: scrollview, iv: imageview)
}

 */

class Scroll {
    
    class func scrollSetupZoom(sv: UIScrollView, iv: UIImageView, minZoomScale: CGFloat=0.3)
    {
        guard let img = iv.image else { return }
        iv.sizeToFit()
        
        // zoom it
        let imgScaledW = img.size.width
        let imgScaledH = img.size.height
        
        let wscale: CGFloat = (sv.frame.width * 0.8) / imgScaledW
        let hscale: CGFloat = (sv.frame.height * 0.8) / imgScaledH
        
        sv.minimumZoomScale = min(wscale, hscale, minZoomScale)
        if wscale < 1.0 || hscale < 1.0 {
            sv.zoomScale = min(wscale, hscale)
        }
    }
    
    class func scrollCenterImage(sv: UIScrollView, iv: UIImageView, force: Bool=false)
    {
        if force || iv.frame.height <= sv.frame.height {
            sv.contentInset.top = (sv.frame.height - iv.frame.height) / 2
        }
        
        if force || iv.frame.width <= sv.frame.width {
            sv.contentInset.left = (sv.frame.width - sv.contentSize.width) / 2
        }
    }
    
}

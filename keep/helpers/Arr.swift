import CoreData

class Arr<T> {
    
    private let list: Array<T>?
    
    init(_ list: Array<T>?) {
        self.list = list
    }
    
    convenience init(_ set: NSSet?) {
        if let s = set, let se = Array(s) as? [T] {
            self.init(se)
        } else {
            self.init([])
        }
    }
    
    func get() -> [T] {
        return list ?? []
    }
    
    var second: T? {
        if let l = list, l.count >= 2 {
            return l[1]
        }
        return nil
    }

    /**
    func categorize(_ cbCategory: (T)->String) -> [String:[T]]
    {
        var r: [String:[T]] = [:]
        for item in self.list ?? [] {
            let category = cbCategory(item).uppercased()
            if r[category]==nil {
                r[category] = [item]
            } else {
                r[category]!.append(item)
            }
        }
        return r
    }
    */
    
    func categorize(_ cbCategory: (T)->String, sorted: Bool) -> [(String, [T])]
    {
        var r: [(String, [T])] = []
        for item in self.list ?? [] {
            let category = cbCategory(item)
            if let index = Str.firstIndex(s: category, l: r.map({ $0.0 })) {
                r[index].1.append(item)
            } else if sorted {
                r.insertInsensitiveSorted((category, [item])) { $0.0 }
            } else {
                r.append((category, [item]))
            }
        }
        return r
    }
    
    func categoryFindFirst<T: Hashable>(of item: T) -> IndexPath? {
        if let data = self.list as? [(String, [T])] {
            for (s, category) in data.enumerated() {
                for (r, row) in category.1.enumerated() {
                    if item == row { return IndexPath(row: r, section: s) }
                }
            }
        }
        return nil
    }
    
    func uncategorize<M>() -> [M]
    {
        var r: [M] = []
        for i in self.list ?? [] {
            if let t = i as? (String, [M]) {
                r.append(contentsOf: t.1)
            }
        }
        return r
    }
    
    func distinct<C: Comparable>(_ cbRow: (T)->C) -> [T]
    {
        var r: [(C, T)] = []
        
        for item in list ?? [] {
            let v = cbRow(item)
            var contains = false
            for ii in r {
                if ii.0 == v {
                    contains = true
                    break
                }
            }
            if (!contains) { r.append((v, item)) }
        }
        return r.map { $0.1 }
    }
    
    func union(_ other: [T]) -> [T] {
        let p = (self.list ?? []) + other
        if let strings = p as? [String] {
            var r: [String] = []
            for s in strings {
                if !Str.inArray(s: s, l: r) {
                    r.append(s)
                }
            }
            return r as! [T]
        }
        return p
    }

    
}


// MARK: helppp
fileprivate extension Array {
    
    private func insertionBinaryIndex<C: Comparable>(of value: Element, cbComparable: (Element)->C) -> Index {
        var slice: SubSequence = self[...]
        
        while !slice.isEmpty {
            let middle = slice.index(slice.startIndex, offsetBy: slice.count / 2)
            if cbComparable(value) < cbComparable(slice[middle]) {
                slice = slice[..<middle]
            } else {
                slice = slice[index(after: middle)...]
            }
        }
        return slice.startIndex
    }
    
    mutating private func insertBinarySorted<C: Comparable>(_ value: Element, cbComparable: (Element)->C) {
        insert(value, at: insertionBinaryIndex(of: value, cbComparable: cbComparable))
    }

    
    // ...
    mutating func insertInsensitiveSorted(_ value: Element, cbComparable: (Element)->String) {
        var slice: SubSequence = self[...]
        while !slice.isEmpty {
            let middle = slice.index(slice.startIndex, offsetBy: slice.count / 2)
            if cbComparable(value).uppercased() < cbComparable(slice[middle]).uppercased() {
                slice = slice[..<middle]
            } else {
                slice = slice[index(after: middle)...]
            }
        }
        let indexResult = slice.startIndex
        
        insert(value, at: indexResult)
    }
}

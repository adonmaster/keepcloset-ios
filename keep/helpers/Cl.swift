import UIKit

class Cl {
    
    static let mc = GMColor.self
    static let white1 = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.1)
    static let white2 = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.2)
    static let white3 = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.3)
    static let white4 = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.4)
    static let white5 = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.5)

    static let red5 = UIColor.init(red: 1, green: 0, blue: 0, alpha: 0.5)
    static let red4 = UIColor.init(red: 1, green: 0, blue: 0, alpha: 0.4)
    static let red3 = UIColor.init(red: 1, green: 0, blue: 0, alpha: 0.3)
    static let red2 = UIColor.init(red: 1, green: 0, blue: 0, alpha: 0.2)
    static let red1 = UIColor.init(red: 1, green: 0, blue: 0, alpha: 0.1)
    
    static let green2 = UIColor.init(red: 0, green: 1, blue: 0, alpha: 0.2)
    
    static let black5 = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
    static let black4 = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
    static let black3 = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
    static let black2 = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.2)
    static let black1 = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.1)
    
    
    
    // *****************
    
    
    static private var nearestCache: [UIColor: UIColor] = [:]
    class func nearestClearCache() {
        nearestCache = [:]
    }
    
    class func nearest(color: UIColor, from palette: [UIColor]) -> UIColor
    {
        // cached?
        if let cachedColor = nearestCache[color] { return cachedColor }
        
        var r: (CGFloat, UIColor) = (CGFloat.greatestFiniteMagnitude, UIColor.green)
        
        for cur in palette {
            var r1: CGFloat=0, g1: CGFloat=0, b1: CGFloat=0
            color.getRed(&r1, green: &g1, blue: &b1, alpha: nil)
            var r2: CGFloat=0, g2: CGFloat=0, b2: CGFloat=0
            cur.getRed(&r2, green: &g2, blue: &b2, alpha: nil)
            
            let distance = sqrt(pow(r2 - r1, 2) + pow(g2 - g1, 2) + pow(b2 - b1, 2))
            if r.0 > distance { r = (distance, cur) }
        }
        
        return r.1
    }
    
    class func from(hex: String?, alpha: CGFloat=1.0) -> UIColor?
    {
        guard let hex = hex else { return nil }
        
        let hexString: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    class func toHex(color: UIColor) -> String
    {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb: Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
    
    class func toHexOp(color: UIColor?) -> String? {
        if let cl = color {
            return toHex(color: cl)
        }
        return nil
    }
}

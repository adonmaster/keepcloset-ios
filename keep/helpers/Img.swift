import UIKit


class Img {
    
    enum Err: Error {
        case runtimeError(String)
    }
    
    private var img: UIImage
    private var lastError: String?
    
    init(_ img: UIImage) {
        self.img = img;
    }
    
    convenience init(v: UIView, isOpaque: Bool=false, factorScale: CGFloat=1.0)
    {
        UIGraphicsBeginImageContextWithOptions(v.bounds.size, isOpaque, factorScale)
        v.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        self.init(img)
    }
    
    enum LimitType {
        case width(CGFloat)
        case height(CGFloat)
    }
    

    func limit(_ type: LimitType, isOpaque: Bool=true, factorScale: CGFloat=0) -> Img
    {
        // calculating perfect wdith and height
        let ow = img.size.width, oh = img.size.height;
        var h: CGFloat, w: CGFloat;
        switch type {
        case .width(let width):
            w = ow > width ? width : ow
            h = oh / ow * w
        case .height(let height):
            h = oh > height ? height : oh
            w = ow / oh * h
        }
        
        // generate uiimage with correct size
        let size: CGSize = CGSize(width: w, height: h)
        UIGraphicsBeginImageContextWithOptions(size, isOpaque, factorScale)
        img.draw(in: CGRect(origin: CGPoint.zero, size: size))
        let imgUnscaled = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        self.img = imgUnscaled
        
        return self;
    }
    
    enum RoundValue {
        case circular
        case corners(CGFloat)
    }
    
    func roundCorners(_ cornerRadius: RoundValue, stroke: (CGFloat, UIColor)?=nil) -> Img
    {
        let cr: CGFloat
        switch cornerRadius {
        case .circular: cr = img.size.width/2
        case .corners(let v): cr = v
        }
        
        let strokeSize = stroke?.0 ?? 0
        let halfStrokeSize = strokeSize == 0 ? 0 : strokeSize / 2
        let sizeFull = CGSize(width: img.size.width + strokeSize*2, height: img.size.height + strokeSize*2)
        let rectImg = CGRect(origin: CGPoint(x: strokeSize, y: strokeSize), size: img.size);
        let path = UIBezierPath(roundedRect: rectImg, cornerRadius: cr)
        
        // begin
        UIGraphicsBeginImageContextWithOptions(sizeFull, false, 0)
        let context = UIGraphicsGetCurrentContext()!
        
        context.saveGState()
        path.addClip()
        
        img.draw(in: rectImg)
        context.restoreGState()
        
        // stroke
        if let st = stroke {
            // transform, since stroke will draw a line in the middle of the path
            path.apply(CGAffineTransform(translationX: -halfStrokeSize, y: -halfStrokeSize))
            
            st.1.setStroke()
            path.lineWidth = st.0
            path.stroke()
        }
        
        let r = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        // end
        
        self.img = r
        
        return self
    }
    
    func scale(factor: CGFloat, isOpaque: Bool=true, scaleFactor: CGFloat=0) -> Img
    {
        let size = CGSize(width: img.size.width * factor, height: img.size.height * factor)
        
        UIGraphicsBeginImageContextWithOptions(size, isOpaque, scaleFactor);
        img.draw(in: CGRect(origin: CGPoint.zero, size: size));
        let imgScaled = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.img = imgScaled!;
        
        return self
    }
    
    func blur(radius: CGFloat) -> Img
    {
        do {
            let ocimage = CIImage(image: img)!
            
            guard let filter = CIFilter(name: "CIGaussianBlur") else { try fail("Img@CIGaussianBlur"); return self; }
            filter.setValue(ocimage, forKey: kCIInputImageKey)
            filter.setValue(radius, forKey: kCIInputRadiusKey)
            guard let pimage = filter.outputImage else { try fail("Img@pimage"); return self }
            
            // crop
            guard let rimage = CIContext(options: nil)
                .createCGImage(pimage, from: ocimage.extent) else { try fail("Img@rimage"); return self }
            
            // res
            img = UIImage(cgImage: rimage, scale: img.scale, orientation: img.imageOrientation)
        }
        catch {
            lastError = error.localizedDescription
        }
        
        return self
    }
    
    func trim(alphaGreaterThan: CGFloat=0) -> Img
    {
        var l = Int.max, t = Int.max, r = -Int.max, b = -Int.max
        var isEmpty = true
        Img.mapColor(from: self.img) { (color, point) in
            
            let alpha = color.cgColor.alpha
            guard alpha > alphaGreaterThan else { return true }
            
            if isEmpty { isEmpty = false }
            
            let curX = Int(point.x)
            let curY = Int(point.y)
            
            l = min(l, curX)
            t = min(t, curY)
            r = max(r, curX)
            b = max(b, curY)
            
            return true
        }

        // validate
        if !isEmpty && r - l > 1 && b - t > 1
        {
            let x = CGFloat(l), y = CGFloat(t), w = CGFloat(r - l), h = CGFloat(b - t)
            
            // ok we crop and deliver!
            let rect = CGRect(x: x, y: y, width: w, height: h)
            if let cgimage = self.img.cgImage?.cropping(to: rect) {
                self.img = UIImage(cgImage: cgimage, scale: self.img.scale, orientation: self.img.imageOrientation)
            }
            
        }
        
        return self
    }
    
    func flipVertically() -> Img
    {
        guard let cg = img.cgImage else { return self }
        
        let size = img.size
        
        UIGraphicsBeginImageContextWithOptions(size, false, 1)
        let bitmap = UIGraphicsGetCurrentContext()!

        bitmap.translateBy(x: size.width / 2, y: size.height / 2)
        bitmap.scaleBy(x: 1.0, y: 1.0)

        bitmap.translateBy(x: -size.width / 2, y: -size.height / 2)
        bitmap.draw(cg, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        self.img = image!
        
        return self
    }
    
    func fitInSize(targetSize: CGSize) -> Img
    {
        let size = img.size
        
        // Determine the scale factor that preserves aspect ratio
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        let scaleFactor = min(widthRatio, heightRatio)
        
        // Compute the new image size that preserves aspect ratio
        let scaledImageSize = CGSize(
            width: size.width * scaleFactor,
            height: size.height * scaleFactor
        )
        
        //
        let offPt = CGPoint(
            x: (targetSize.width - scaledImageSize.width)/2,
            y: (targetSize.height - scaledImageSize.height)/2
        )
        
        // Draw and return the resized UIImage
        img = UIGraphicsImageRenderer(size: targetSize).image { _ in
            img.draw(in: CGRect(origin: offPt, size: scaledImageSize))
        }
        
        return self
    }
    
    func subimage(_ rect: CGRect, factorScale: CGFloat=1.0) -> Img {
        if let c = self.img.cgImage?.cropping(to: rect) {
            self.img = UIImage(cgImage: c, scale: factorScale, orientation: .up)
        }
        return self
    }
    
    func getLastError() -> String? {
        return lastError
    }
    
    private func fail(_ msg: String) throws {
        throw Err.runtimeError(msg)
    }
    
    func get() -> UIImage {
        return img
    }
    
}


// class methods
extension Img {
    
    class func merge(big: UIImage, small: UIImage) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(big.size, true, 0.0);
        
        // draw big
        big.draw(in: CGRect(origin: CGPoint.zero, size: big.size));
        
        // calculate & draw small
        let x: CGFloat = (big.size.width - small.size.width) / 2
        let y: CGFloat = (big.size.height - small.size.height) / 2
        small.draw(in: CGRect(origin: CGPoint(x: x, y: y), size: small.size))
        
        // res
        let result = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return result!;
    }

    
    class func mapColor(from img: UIImage, cb: ((UIColor, CGPoint))->Bool)
    {
        guard let cgimage = img.cgImage, let pixelData = cgimage.dataProvider?.data else { return }
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        for x in 0..<cgimage.width {
            for y in 0..<cgimage.height {
                
                let pixelInfo: Int = ((Int(img.size.width) * y) + x) * 4
                let b = CGFloat(data[pixelInfo+0]) / CGFloat(255.0)
                let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
                let r = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
                let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
                
                let color = UIColor(red: r, green: g, blue: b, alpha: a)
                let point = CGPoint(x: x, y: y)
                
                if !cb((color, point)) { return }
            }
        }
    }
    
    class func extractColor(from img: UIImage, amount: Int, from palette: [UIColor]) -> [UIColor]
    {
        var colorCount: [UIColor:Int] = [:]
        
        Cl.nearestClearCache()
        Img.mapColor(from: img) { (color, _) in
            guard color.cgColor.alpha > 0 else { return true }
            
            // distancia
            let cl = Cl.nearest(color: color, from: palette)
            let n = colorCount[cl] ?? 0
            colorCount[cl] = n + 1
            
            return true
        }
        
        // report
        // colorCount.forEach { print("\(Cl.toHex(color: $0.key)): \($0.value)") }
        
        // selecting most(n) prominent colors
        let colors = colorCount
            .sorted(by: { $0.value > $1.value })
            .prefix(amount)
        
        // validate, the second color must be at last 60% of the 1st color
        if colors.count <= 0 { return [] }
        if colors.count == 1 { return [colors.first!.key] }
        
        var result: [UIColor:Int] = [:]
        var lastColor = colors.first!
        for item in colors {
            if Double(item.value) > 0.22 * Double(lastColor.value) {
                result[item.key] = item.value
            }
            lastColor = item
        }
        
        return result.map { $0.key }
    }
    
    class func isEmpty(image: UIImage?) -> Bool
    {
        guard let img = image else { return true }
        
        var isEmpty = true
        Img.mapColor(from: img) { (color, _) in
            
            guard color.cgColor.alpha > 0 else { return true }
            
            if isEmpty {
                isEmpty = false
                return false
            }
            
            return true
        }
        
        return isEmpty
    }
    
}

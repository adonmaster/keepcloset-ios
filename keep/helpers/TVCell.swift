import UIKit

class TVCell {
    
    private var tv: UITableView
    private var cache: [Int: [IndexPath]] = [:]
    
    init(_ tv: UITableView) {
        self.tv = tv
    }
    
    func set(_ tv: UITableView) {
        self.tv = tv
    }
    
    private func append(_ operation: Int, _ row: Int, _ section: Int=0)
    {
        var arr = cache[operation] ?? []
        arr.append(IndexPath(row: row, section: section))
        cache[operation] = arr
    }
    
    func insert(row: Int, section: Int=0) {
        append(1, row, section)
    }
    
    func delete(row: Int, section: Int=0) {
        append(-1, row, section)
    }
    
    func reload(row: Int, section: Int=0) {
        append(0, row, section)
    }
    
    func flush() {
        
        tv.performBatchUpdates({
            
            // delete list
            if let list = cache[-1] {
                tv.deleteRows(at: list, with: .automatic)
            }
            
            // reload list
            if let list = cache[0] {
                tv.reloadRows(at: list, with: .automatic)
            }
            
            // insert list
            if let list = cache[1] {
                tv.insertRows(at: list, with: .automatic)
            }
            
        }) { _ in
            self.cache = [:]
        }
        
    }
}

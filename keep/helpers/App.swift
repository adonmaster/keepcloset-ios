import UIKit
import Alamofire

class App {
    
    private init() {}
    static var i: App = App();
    
    func isNetworking(_ option: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = option;
    }
    
    var version: String {
        get {
            return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.2.3"
        }
    }
    
    var versionBuildNumbe: String {
        get {
            return Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "0.0.0";
        }
    }
    
    func isNetworkAvailable() -> Bool {
        if let manager = NetworkReachabilityManager() {
            return manager.isReachable
        }
        return true
    }
    
    func openLink(_ surl: String) {
        if let url = URL(string :surl) {
            UIApplication.shared.open(url, options: [:]) { _ in
                // on completion
            }
        }
    }
    
    func presentConfig() {
        let url = URL(string: UIApplication.openSettingsURLString)!
        UIApplication.shared.open(url, options: [:])
    }
    
    func registerForegroundEvent(receiver: NSObject, cb: @escaping Ev.Cb) {
        Ev.listen(key: "App@foregroundEvent", receiver: receiver, cb: cb)
    }
    
    func unregisterForegroundEvent(receiver: NSObject) {
        Ev.unlisten(key: "App@foregroundEvent", receiver: receiver)
    }
    
    func fireForegroundEvent() {
        Ev.fire(key: "App@foregroundEvent")
    }
}

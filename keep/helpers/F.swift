import UIKit

class F {
    
    private init() {}
    
    static let app = FBase(dir: .applicationSupportDirectory, domain: .userDomainMask)
    static let tmp = FBase(dir: .cachesDirectory, domain: .userDomainMask)
    
}


class FBase {
    
    enum EnumResult<T> {
        case success(T)
        case error(String)
    }
    
    enum FImageFileExtension {
        case jpeg
        case png
    }
    
    let dir: FileManager.SearchPathDirectory
    let domain: FileManager.SearchPathDomainMask
    
    init(dir: FileManager.SearchPathDirectory, domain: FileManager.SearchPathDomainMask) {
        self.dir = dir
        self.domain = domain
    }
    
    private func f() -> FileManager {
        return FileManager.default
    }
    
    func url(for fileName: String) -> URL? {
        return try? f()
            .url(for: dir, in: domain, appropriateFor: nil, create: true)
            .appendingPathComponent(fileName)
    }
    
    func exists(url: URL) -> Bool {
        return f().fileExists(atPath: url.path)
    }
    
    func write(image value: UIImage, filename: String, ext: FImageFileExtension = .jpeg) -> EnumResult<URL>
    {
        let data: Data?;
        switch ext {
        case .jpeg:
            data = value.jpegData(compressionQuality: 1)
        case .png:
            data = value.pngData()
        }
        
        if let d = data {
            do {
                let u = url(for: filename)!
                try d.write(to: u)
                return .success(u)
            } catch {
                return EnumResult.error(error.localizedDescription)
            }
        }
        
        return .error("Não existe dados de nada!")
    }
    
    func delete(url: URL) {
        try? f().removeItem(at: url)
    }
    
    func copy(_ from: URL, to: URL) -> EnumResult<URL> {
        guard exists(url: from) else { return .error("Arquivo não encontrado!") }
        
        do {
            // delete first
            if (exists(url: to)) { delete(url: to) }
            
            // then copy
            try f().copyItem(at: from, to: to)
            return .success(to)
        } catch {
            return EnumResult.error(error.localizedDescription)
        }
    }
    
    func dynamicPath(filename: String) -> String
    {
        var prefix: String!
        switch dir {
        case .applicationSupportDirectory: prefix = "{#appSupport}/"
        case .cachesDirectory: prefix = "{#cache}/"
        default: prefix = "{#}/"
        }
        
        return "\(prefix!)\(filename)"
    }
    
    func urlFrom(dynamicPath: String) -> URL?
    {
        let kApp = "{#appSupport}/"
        let kCache = "{#cache}/"
        
        if dynamicPath.hasPrefix(kApp) {
            let filename = String(dynamicPath.dropFirst(kApp.count))
            return url(for: filename)
        } else if dynamicPath.hasPrefix(kCache) {
            let filename = String(dynamicPath.dropFirst(kCache.count))
            return url(for: filename)
        }
        
        return nil
    }
    
}

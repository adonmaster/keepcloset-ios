import Foundation

class Carbon {
    
    // static
    
    static var locale: Locale {
        return Locale(identifier: Locale.preferredLanguages.first ?? "pt_BR")
    }
    
    class func parse(_ s: String, _ format: String="dd/MM/Y HH:mm:ss") -> Carbon? {
        let fr = Tap(DateFormatter()).with { f in
            f.locale = Locale(identifier: "en_US_POSIX")
            f.timeZone = TimeZone.current
        }
        fr.dateFormat = format
        guard let d = fr.date(from: s) else { return nil }
        
        return Carbon(d)
    }
    
    class func parseFromSqlDate(_ s: String) -> Carbon? {
        return parse(s, "Y-MM-dd")
    }
    
    class func parseFromJson(_ s: String) -> Carbon? {
        return parse(s, "Y-MM-dd HH:mm:ss")
    }
    
    // instance
    
    private var dt: Date
    private let calendar: Calendar
    private lazy var formatter: DateFormatter = Tap(DateFormatter()).with { f in
        f.locale = Carbon.locale
        f.timeZone = TimeZone.current
    }
    
    private var now: Date { return Date(timeIntervalSinceNow: 0) }
    
    init(_ dt: Date?=nil) {
        self.dt = dt ?? Date(timeIntervalSinceNow: 0)
        calendar = Calendar.current
    }
    
    func add(_ value: Int, _ unit: Calendar.Component) -> Carbon {
        dt = calendar.date(byAdding: unit, value: value, to: dt)!
        return self
    }
    
    func get() -> Date {
        return dt
    }
    
    func isFuture(_ fromDate: Date?=nil) -> Bool {
        return dt > fromDate ?? now
    }
    
    func isToday() -> Bool {
        let pattern = "dd-MM-Y"
        return Carbon(dt).format(pattern: pattern) == Carbon().format(pattern: pattern)
    }
    
    func isSameDate(_ date: Date) -> Bool {
        return formatSqlDate() == Carbon(date).formatSqlDate()
    }
    
    func format(pattern: String="dd-MM-Y") -> String {
        formatter.dateFormat = pattern
        
        return formatter.string(from: dt)
    }
    
    func formatBrDateTime() -> String {
        return format(pattern: "dd/MM/Y HH:mm")
    }
    
    func formatSqlDate() -> String {
        return format(pattern: "Y-MM-dd")
    }
    
    func formatSqlMonth() -> String {
        return format(pattern: "Y-MM")
    }
    
    func formatSql() -> String {
        return format(pattern: "Y-MM-dd HH:mm:ss")
    }
}

// MARK: time ago

extension Carbon {
    
    func diffForHuman() -> String
    {
        let now = Date()
        let diff = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now, to: dt)
        
        let cbP: (Int,String,String,String,String)->String? = { i, sp, sn, mp, mn in
            if i == 0 { return nil }
            if i == 1 { return sp }
            if i == -1 { return sn }
            if i > 1 { return mp.replacingOccurrences(of: "#n", with: String(abs(i))) }
            if i < 1 { return mn.replacingOccurrences(of: "#n", with: String(abs(i))) }
            return nil
        }
        
        if let s = cbP(diff.year!, "em um ano", "um ano atrás", "em #n anos", "#n anos atrás") {
            return s
        }
        
        if let s = cbP(diff.month!, "em um mês", "mês passado", "em #n meses", "#n meses atrás") {
            return s
        }
        
        if let s = cbP(diff.day!, "em um dia", "ontem", "em #n dias", "#n dias atrás") {
            return s
        }
        
        if let s = cbP(diff.hour!, "em uma hora", "uma hora atrás", "em #n horas", "#n horas atrás") {
            return s
        }
        
        if let s = cbP(diff.minute!, "em um minuto", "um minuto atrás", "em #n minutos", "há #n minutos") {
            return s
        }
        
        if let s = cbP(diff.second!, "em um segundo", "um segundo atrás", "em alguns segundos", "segundos atrás") {
            return s
        }
        
        return "agorinha"
    }
    
}

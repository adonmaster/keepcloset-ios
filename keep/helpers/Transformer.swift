import UIKit

class Transformer<A,B> {
    
    private let cb: (A?)->B
    
    init(_ cb: @escaping (A?)->B) {
        self.cb = cb
    }
    
    func transform(_ source: A?) -> B {
        return cb(source)
    }
    
}

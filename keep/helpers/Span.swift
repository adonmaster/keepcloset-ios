import Foundation
import UIKit

class Span {
    
    private var s: String
    private var attributes: [NSAttributedString.Key:Any] = [:]
    private var cache: NSMutableAttributedString = NSMutableAttributedString()
    
    init(_ s: String) {
        self.s = s
        totalCount += s.count
    }
    
    private var totalCount = 0
    var count: Int {
        return totalCount
    }
    
    func reset() -> Self {
        s = ""
        totalCount = 0
        attributes = [:]
        cache = NSMutableAttributedString()
        return self
    }
    
    func color(_ cl: UIColor) -> Self {
        attributes[.foregroundColor] = cl
        return self
    }
    
    func font(name: String, size: CGFloat) -> Self {
        attributes[.font] = UIFont(name: name, size: size)
        return self
    }
    
    private func flush() {
        cache.append(NSAttributedString(string: s, attributes: attributes))
        self.s = ""
        self.attributes = [:]
    }
    
    func append(_ s: String) -> Self
    {
        flush()
        
        self.s = s
        totalCount += s.count
        
        return self
    }
    
    func get() -> NSAttributedString
    {
        flush()
        return cache
    }
}

extension String {
    
    func span() -> Span {
        return Span(self)
    }
    
}

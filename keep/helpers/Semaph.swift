

class Semaph {
    
    private let mOnFinish: ()->()
    private var mValue = 0 {
        didSet {
            if mValue<=0 {
                mOnFinish()
            }
        }
    }
    
    init(onFinish: @escaping ()->()) {
        mOnFinish = onFinish
    }
    
    func inc() {
        mValue += 1
    }
    
    func dec() {
        mValue -= 1
    }
    
}

import UIKit

class Eventable {
    
    private var cache: [String:[EventableDelegate]] = [:]
    
    func listen(key: String, caller: EventableDelegate)
    {
        var listeners: [EventableDelegate] = cache[key] ?? []
        if !listeners.contains(where: { caller === $0 }) {
            listeners.append(caller)
        }
        cache[key] = listeners
    }
    
    func unlisten(caller: EventableDelegate, key: String?=nil)
    {
        let keys: [String] = key == nil ? Array(cache.keys) : [key!]
        for k in keys
        {
            var listeners: [EventableDelegate] = cache[k] ?? []
            var index = listeners.lastIndex(where: { caller === $0 })
            while index != nil {
                listeners.remove(at: index!)
                index = listeners.lastIndex(where: { caller === $0 })
            }
            
            // commit
            if listeners.count == 0 {
                cache.removeValue(forKey: k)
            } else {
                cache[k] = listeners
            }
        }
    }
    
    func fire(key: String, payload: Any?=nil)
    {
        if let listeners: [EventableDelegate] = cache[key] {
            for listener in listeners {
                Task.main {
                    listener.eventable(on: key, payload: payload)
                }
            }
        }
    }
    
}

protocol EventableDelegate: AnyObject {
    
    func eventable(on key: String, payload: Any?)
    
}

import UIKit;
import FontAwesome;

class Keyboards {
    
    class func setInputAccessoryButton(textField: AnyObject, title: String, target: AnyObject?, action: Selector)
    {
        // create tool bar
        let toolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: 320, height: 50));
        
        // flexible space
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        
        // button
        let done = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: target, action: action);
        
        // config
        toolbar.items = [flexSpace, done];
        toolbar.sizeToFit();
        
        // do the thing
        if textField is UITextField {
            (textField as! UITextField).inputAccessoryView = toolbar;
        }
        else if textField is UITextView {
            (textField as! UITextView).inputAccessoryView = toolbar;
        }
    }
    
    class func setInputAccessoryButton(textField: AnyObject, withImage img: UIImage, target: AnyObject?, action: Selector)
    {
        // create tool bar
        let toolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: 320, height: 50));
        
        // flexible space
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        
        // button
        let bt = UIBarButtonItem(image: img, style: .plain, target: target, action: action);
        
        // config
        toolbar.items = [flexSpace, bt];
        toolbar.sizeToFit();
        
        // do the thing
        if textField is UITextField {
            (textField as! UITextField).inputAccessoryView = toolbar;
        }
        else if textField is UITextView {
            (textField as! UITextView).inputAccessoryView = toolbar;
        }
    }
    
    class func setInputAccessoryButton(textField: AnyObject, withAwesome code: String, color: UIColor, target: AnyObject?, action: Selector)
    {
        let img = UIImage.fontAwesomeIcon(
            name: FontAwesome.fromCode(code) ?? .check,
            style: .solid,
            textColor: color,
            size: CGSize(width: 30, height: 30)
        )
        
        setInputAccessoryButton(textField: textField, withImage: img, target: target, action: action)
    }
    
}

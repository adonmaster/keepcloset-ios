import UIKit

class Anim {
    
    class func floater(_ v: UIView, _ duration: CFTimeInterval=1, _ x: CGFloat=0, y: CGFloat=15.0)
    {
        let hover = CABasicAnimation(keyPath: "position")
        hover.isAdditive = true
        hover.fromValue = NSValue(cgPoint: CGPoint.zero)
        hover.toValue = NSValue(cgPoint: CGPoint(x: x, y: y))
        hover.autoreverses = true
        hover.duration = duration
        hover.repeatCount = Float.infinity
        
        v.layer.add(hover, forKey: "hoverAnimation")
    }
    
    class func hide(_ v: UIView) {
        if v.isHidden  { return }
        UIView.animate(withDuration: 0.4, animations: {
            v.alpha = 0
        }) { (_) in
            v.isHidden = true
            v.alpha = 1
        }
    }
    
}

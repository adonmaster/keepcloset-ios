import UIKit

class Math {
    
    class func calculateSize(_ width: CGFloat, bestCellWidth: CGFloat, minSpace: CGFloat) -> CGFloat?
    {
        let n: CGFloat = floor((width + minSpace) / (bestCellWidth + minSpace))
        if n <= 1 { return width }
        
        let f: (CGFloat)->CGFloat? = { p in
            let ncur = n + p
            let space = (ncur - 1) * minSpace
            let curwidth = width - space
            let rem = curwidth.truncatingRemainder(dividingBy: ncur)
            return rem == 0 ? curwidth/ncur : nil
        }
        
        var cont: CGFloat = 1
        var r: CGFloat! = nil
        while r == nil && cont < 100 {
            if n-cont > 1 { r = f(-cont) }
            if r != nil { return r }
            r = f(cont)
            
            cont += 1.0
        }
        
        return r
    }
    
    class func scale(rect: CGRect, factor: CGFloat) -> CGRect {
        return CGRect(x: rect.minX * factor, y: rect.minY * factor, width: rect.width * factor, height: rect.height * factor)
    }
    
}

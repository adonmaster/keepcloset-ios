import UIKit
import Photos
import TLPhotoPicker


struct Media {
    
    static func presentPicker(caller: UIViewController, _ sourceType: UIImagePickerController.SourceType, delegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate))
    {
        // halt
        guard UIImagePickerController.isSourceTypeAvailable(sourceType) else {
            let msg = sourceType != .camera
                ? "Seu dispositivo não tem acesso ao album."
                : "Seu dispositvo não tem acesso a camera."
            
            Dialog(sender: caller)
                .title("Atenção").msg(msg)
                .ok().cancel(false)
                .present()
            
            return;
        }
        
        // go on
        let picker = Tap(UIImagePickerController()).with {
            $0.allowsEditing = false
            $0.sourceType = sourceType
            $0.delegate = delegate
        }
        
        caller.present(picker, animated: true)
    }

    // multiple
    static func presentPickerMultiple(identifier: String?, caller: (MediaDelegate & UIViewController)?, type: PHAssetMediaType = .image, singleSelect: Bool=false)
    {
        let vc = TLPhotosPickerViewController(withPHAssets: { phassets in
            
            caller?.media(for: identifier, onSelect: phassets)
            
        }, didCancel: nil)
        var config = TLPhotosPickerConfigure()
        config.mediaType = type
        config.usedCameraButton = false
        config.allowedVideo = false
        config.singleSelectedMode = singleSelect
        vc.configure = config
        
        caller?.present(vc, animated: true, completion: nil)
    }
    
    enum SizeLimit {
        case width(CGFloat)
        case height(CGFloat)
    }
    
    static func request(_ phasset: PHAsset?, size: SizeLimit?=nil, cb: @escaping (UIImage)->())
    {
        guard let pha = phasset else {
            cb(#imageLiteral(resourceName: "404"))
            return
        }
        
        var w: CGFloat = CGFloat(pha.pixelWidth)
        var h: CGFloat = CGFloat(pha.pixelHeight)
        let ratio = w/h
        if let sz = size {
            switch sz {
            case .width(let v):
                w = w > v ? v : w
                h = w / ratio
            case .height(let v):
                h = h > v ? v : h
                w = ratio * h
            }
        }
        
        let size = CGSize(width: w, height: h)
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        Task.main {
            PHImageManager.default().requestImage(for: pha, targetSize: size, contentMode: .default, options: options) { img, dict in
                if let i = img {
                    cb(i)
                } else {
                    cb(#imageLiteral(resourceName: "404"))
                }
            }
        }
    }
}

@objc protocol MediaDelegate {
    
    func media(for identifier: String?, onSelect assets: [PHAsset])

    
}

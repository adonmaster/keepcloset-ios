import Foundation

class Str {
    
    class func mergeUriPart(_ base: String, _ uri: String) -> String {
        let a: String = base.hasSuffix("/") ? String(base.dropLast()) : base;
        let b: String = uri.hasPrefix("/") ? String(uri.dropFirst()) : uri;
        return a + "/" + b;
    }
    
    class func trim(_ str: String?) -> String? {
        guard let s = str else { return nil }
        return s.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    class func gtrim(_ str: String?, def: String="") -> String {
        return trim(str) ?? def
    }
    
    class func plural<T>(_ n: Int, _ zero: T, _ one: T, _ many: (Int)->T) -> T {
        if n <= 0 { return zero }
        if n == 1 { return one }
        return many(n)
    }
    
    class func plural(_ n: Int, _ zero: String, _ one: String, _ many: String) -> String {
        return plural(n, zero, one) { n in
            return many.replacingOccurrences(of: "$0", with: String(n))
        }
    }
    
    class func validate(_ s: String?) -> String? {
        if let r = s, r.trim().count > 0 { return s }
        return nil
    }
    
    class func grant(_ s: String?, def: String) -> String {
        return validate(s) ?? def
    }
    
    class func inArray(s: String, l: [String]) -> Bool {
        return l.contains(where: { $0.trim().caseInsensitiveCompare(s.trim()) == .orderedSame })
    }
    
    class func firstIndex(s: String, l: [String]) -> Int? {
        return l.firstIndex(where: { $0.caseInsensitiveCompare(s) == .orderedSame })
    }
    
    class func capitalizeFirst(s: String) -> String {
        return s.prefix(1).uppercased() + s.lowercased().dropFirst()
    }
    
    class func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}

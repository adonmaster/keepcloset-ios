import UIKit;

class Task {
    
    private static var debounces: [String:Int] = [:]
    
    class func main(block: @escaping ()->Void) {
        DispatchQueue.main.async(execute: block)
    }
    
    class func main(delayInSeconds: Double, block: @escaping ()->Void) {
        DispatchQueue.global().async {
            Thread.sleep(forTimeInterval: delayInSeconds);
            self.main(block: block);
        }
    }
    
    class func main<T: Any>(initalValue: T, delayInSeconds: Double, block: @escaping (T)->Void) {
        main(delayInSeconds: delayInSeconds) {
            block(initalValue)
        }
    }
    
    class func debounce(key: String, delayInSeconds: Double, _ cb: @escaping ()->Void)
    {
        debounces[key] = (debounces[key] ?? 0) + 1
        
        main(delayInSeconds: delayInSeconds) {
            let i = (debounces[key] ?? 1) - 1
            debounces[key] = i
            
            if i > 0 { return }
            cb()
        }
    }
    
}

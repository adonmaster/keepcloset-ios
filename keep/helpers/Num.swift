import UIKit

class Num {
    
    class func toDigits(_ f: Double, digits: Int=2) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = digits
        return formatter.string(from: f as NSNumber)!
    }
    
}

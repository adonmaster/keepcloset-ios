import UIKit

class FragHeader: UIView
{
    class func f(owner: Any?) -> FragHeader {
        return Bundle.main.loadNibNamed("frag-header", owner: owner, options: nil)!.first as! FragHeader
    }
}

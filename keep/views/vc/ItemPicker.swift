import UIKit

class ItemPicker: UIViewController {
    
    // ui
    @IBOutlet weak var uiCv: UICollectionView!
    
    // public
    var delegate: ItemPickerDelegate?
    var maxSelection: Int = 1
    
    // state
    private var mUser: User!
    private var mData: [(String, [Item])] = []
    private var mSelection: [Item] = []
    
    
    // methods
    
    override func viewDidLoad() {
        mUser = Repo.user.active()!
        mData = Arr(Repo.item.query(user: mUser, categories: [], colors: []))
            .categorize({ $0.category ?? "Sem categoria".l }, sorted: true)
        mSelection = []
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onOk(_ sender: Any) {
        dismiss(animated: true) {
            if self.mSelection.count > 0 {
                self.delegate?.itemPicker(onSelected: self.mSelection)
            }
        }
    }
    
}

// MARK: collection view
extension ItemPicker: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let w = (collectionView.frame.width - 4) / 3
        return CGSize(width: w, height: w)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return mData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mData[section].1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ItemPickerCell
        
        let m = mData[indexPath.section].1[indexPath.row]
        m.p.thumbLoad(imageView: cell.uiImg)
        
        let isSelected = mSelection.contains(m)
        cell.uiCover.isHidden = !isSelected
        cell.uiImg.alpha = isSelected ? 0.3 : 1
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var indexesToReload: [IndexPath] = [indexPath]
        let m = mData[indexPath.section].1[indexPath.row]
        
        if let i = mSelection.firstIndex(of: m) {
            mSelection.remove(at: i)
        } else {
            mSelection.append(m)
            if mSelection.count > 0 && mSelection.count > maxSelection && maxSelection > 0 {
                let removed = mSelection.removeFirst()
                if let removedIndex = Arr(mData).categoryFindFirst(of: removed) {
                    indexesToReload.append(removedIndex)
                }
                
            }
        }
        
        // update collection
        collectionView.reloadItems(at: indexesToReload)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        if let cell = collectionView
            .dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
            as? ItemPickerHeader
        {
            let m = mData[indexPath.section]
            
            cell.uiTitle.text = m.0.trim().uppercased()
            
            return cell;
        }

        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
    }
}


// MARK: delegate
protocol ItemPickerDelegate {
    
    func itemPicker(onSelected items: [Item])
    
}

import UIKit

class ClosetLuggagePackCell: UITableViewCell {
    
    @IBOutlet weak var uiImg: ImageViewRounded!
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiDesc: UILabel!
    
}

import UIKit

class ClosetLuggageCell: UITableViewCell {
    
    // out
    var tableIndex: Int?
    var delegate: ClosetLugaggeCellDelegate?
    
    // ui
    @IBOutlet weak var uiPlace: UILabel!
    @IBOutlet weak var uiDate: UILabel!
    @IBOutlet weak var uiDuration: UILabel!
    @IBOutlet weak var btnPack: UIButton!
    
    // methods
    func outSet(delegate: ClosetLugaggeCellDelegate, index: Int) {
        self.tableIndex = index
        self.delegate = delegate
    }

    @IBAction func onEdit(_ sender: Any) {
        if let i = tableIndex {
            delegate?.closetLuggageCell(onEdit: i)
        }
    }
    
    @IBAction func onPack(_ sender: Any) {
        if let i = tableIndex {
            delegate?.closetLuggageCell(onPack: i)
        }
    }
    
    @IBAction func onDuplicate(_ sender: Any) {
        if let i = tableIndex {
            delegate?.closetLuggageCell(onDuplicate: i)
        }
    }
    
    
}

// MARK: delegate
protocol ClosetLugaggeCellDelegate {
    func closetLuggageCell(onEdit index: Int)
    func closetLuggageCell(onPack index: Int)
    func closetLuggageCell(onDuplicate index: Int)
}

import UIKit

class ClosetLuggage: UIViewController {
    
    // ui
    @IBOutlet weak var uiTv: UITableView!
    @IBOutlet weak var uiEmpty: UIStackView!
    
    // state
    var mUser: User!
    var mData: [Luggage] = []
    
    // methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        mUser = Repo.user.active()!
        mData = Repo.luggage.getList(user: mUser)
        
        uiTv.rowHeight = UITableView.automaticDimension
        uiTv.separatorStyle = .singleLine
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        tabBarController?.navigationItem.rightBarButtonItems = []
        
        mData.forEach({ $0.p.clearCache() })
        uiTv.reloadData()
    }
    
    @IBAction func backToClosetLuggage(_ segue: UIStoryboardSegue) {
        mData = Repo.luggage.getList(user: mUser)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ClosetLuggageEdit {
            vc.outModel = sender as? Luggage
        } else if let vc = segue.destination as? ClosetLuggagePack {
            vc.outModel = sender as? Luggage
        }
    }
    
    @IBAction func onTvLongPress(_ gesture: UILongPressGestureRecognizer)
    {
        guard gesture.state == .began else { return }
        let point = gesture.location(in: uiTv)
        guard let i = uiTv.indexPathForRow(at: point) else { return }

        uiTv.selectRow(at: i, animated: true, scrollPosition: .top)
        
        Dialog(sender: self)
            .title("Atenção".l).msg("Escolha uma das opções:".l)
            .action(key: "Duplicar".l, cb: { _ in
                Task.main {
                    self.closetLuggageCell(onDuplicate: i.row)
                }
            })
            .action(key: "Empacotar".l, cb: { _ in
                Task.main {
                    self.closetLuggageCell(onPack: i.row)
                }
            })
            .destructive("Apagar registro".l).cancel()
            .present(onOk: nil, onCancel: nil, onDestructive: {
                self.tableView(delete: i)
            }, {
                self.uiTv.deselectRow(at: i, animated: true)
            })
    }
    
}


// MARK: table view
extension ClosetLuggage: UITableViewDelegate, UITableViewDataSource, ClosetLugaggeCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        uiEmpty?.isHidden = mData.count > 0
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ClosetLuggageCell
        
        let m = mData[indexPath.row]
        cell.outSet(delegate: self, index: indexPath.row)
        cell.uiPlace.text = m.location
        cell.uiDate.text = m.p.datedAt
        cell.uiDuration.text = m.p.duration
        
        let count = m.p.itemsCount
        let packedCount = m.p.itemsPackedCount
        
        let btnTitle = packedCount < count
            ? "Empacotando".l+" (\(packedCount)/\(count)) ..."
            : "Empacotado".l+" (\(packedCount)/\(count))!"
        cell.btnPack.setTitle(btnTitle, for: .normal)
        
        let btnColor = packedCount < count ? Cl.mc.red700Color() : Cl.mc.green700Color()
        cell.btnPack.setTitleColor(btnColor, for: .normal)
        
        return cell
    }
    
    func closetLuggageCell(onEdit index: Int) {
        performSegue(withIdentifier: "sgEdit", sender: mData[index])
    }
    
    func closetLuggageCell(onPack index: Int) {
        performSegue(withIdentifier: "sgPack", sender: mData[index])
    }
    
    func closetLuggageCell(onDuplicate index: Int) {
        Dialog(sender: self)
            .title("Bagagem".l)
            .msg("Quer mesmo duplicar este registro?".l)
            .ok().cancel()
            .present(onOk: {
                let model = Repo.luggage.cloneAll(self.mData[index])
                Task.main {
                    self.mData.insert(model, at: index)
                    self.uiTv.reloadData()
                }
            })
    }
    
    func tableView(delete i: IndexPath)
    {
        let m = mData[i.row]
        
        // remove db
        Repo.luggage.delete(model: m)
        
        Task.main {
            self.mData.remove(at: i.row)
            self.uiTv.deleteRows(at: [i], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: "sgEdit", sender: mData[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
}

import UIKit

class ClosetLuggageItemSlider: UIPageViewController {
    
    private var mData: [LuggageRepo.LuggageItemSudo] = []
    private var mIndex: Int = 0
    
    func out(models: [LuggageRepo.LuggageItemSudo], index: Int) {
        mData = models
        mIndex = index
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        dataSource = self
        setViewControllers([newvc(mIndex)], direction: .forward, animated: false, completion: nil)
    }
    
    func newvc(_ i: Int) -> ClosetLuggageItemPreview {
        let vc = storyboard!.instantiateViewController(withIdentifier: "previewItem") as! ClosetLuggageItemPreview
        vc.outSet(model: mData[i], index: i)
        return vc
    }
    
    func curvc() -> ClosetLuggageItemPreview? {
        return viewControllers?.first as? ClosetLuggageItemPreview
    }
    
}

extension ClosetLuggageItemSlider: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        if !completed { return }
        
        if let curIndex = curvc()?.outGetIndex() {
            mIndex = curIndex
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex-1
        if i < 0 || mData.count == 0 { return nil }
        
        return newvc(i)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex+1
        if i > mData.count-1 { return nil }
        
        return newvc(i)
    }
    
    
    
}

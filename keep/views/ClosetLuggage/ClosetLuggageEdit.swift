import UIKit
import FontAwesome

class ClosetLuggageEdit: UIViewController {
    
    @IBOutlet weak var uiLocation: TextFieldEx!
    @IBOutlet weak var uiBtnDate: UIButton!
    @IBOutlet weak var uiDuration: UITextField!
    @IBOutlet weak var uiTv: UITableView!
    @IBOutlet weak var uiEmpty: UIStackView!
    
    // states
    var outModel: Luggage?
    private var mUser: User!
    
    private var stVisible = true
    
    private var mDate = Date(timeIntervalSinceNow: 0) {
        didSet {
            let dateStr = "\(Carbon(mDate).diffForHuman()) (\(Carbon(mDate).format(pattern: "dd/MMM")))"
            self.uiBtnDate?.setTitle(dateStr, for: .normal)
        }
    }
    
    private var mItems: [LuggageRepo.LuggageItemSudo] = []
    private var mItemsToRemove: [LuggageRepo.LuggageItemSudo] = []
    
    // methods
    
    override func viewDidLoad()
    {
        mUser = Repo.user.active()!
        if let parent = outModel {
            Repo.luggage.getItems(parent: parent) { r in
                self.mItems = r
                self.uiTv?.reloadData()
            }
        } else {
            mItems = []
        }
        
        uiTv.separatorStyle = .singleLine
        uiTv.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if stVisible {
            stVisible = false
            
            let dt = outModel?.dated_at
            mDate = dt ?? Carbon().get()
            if dt == nil { uiBtnDate.setTitle("Aperte para mudar data...".l, for: .normal) }
            uiDuration.text = outModel?.duration
            uiLocation.text = outModel?.location
        }
    }

    @IBAction func onAdd(_ sender: Any)
    {
        Dialog(sender: self)
            .stylesheet(popoverSourceView: sender as? UIView)
            .title("Babagem item".l)
            .msg("O que você quer inserir?".l)
            .action(key: "Closet", cb: { _ in
                Task.main(delayInSeconds: 0.2, block: {
                    self.performSegue(withIdentifier: "sgItemPicker", sender: nil)
                })
            })
            .action(key: "Look", cb: { _ in
                Task.main(delayInSeconds: 0.2, block: {
                    self.performSegue(withIdentifier: "sgLookPicker", sender: nil)
                })
            })
            .action(key: "Camera", cb: { _ in
                Media.presentPicker(caller: self, .camera, delegate: self)
            })
            .action(key: "Texto".l, cb: { _ in
                Task.main(delayInSeconds: 0.2, block: {
                    self.performSegue(withIdentifier: "sgPickText", sender: nil)
                })
            })
            .cancel()
            .present()
    }
    
    private func addItems(_ items: [Item])
    {
        Task.main {
            for i in items {
                let data = i.thumb
                let thumb = data == nil ? #imageLiteral(resourceName: "404") : UIImage(data: data!)!
                
                self.add(
                    title: "Closet", desc: i.category, thumb: thumb,
                    img: nil, item: i, look: nil
                )
            }
        }
    }
    
    private func addLooks(_ looks: [Look]) {
        Task.main {
            for look in looks {
                let data = look.thumb
                let thumb = data == nil ? #imageLiteral(resourceName: "404") : UIImage(data: data!)!
                
                self.add(
                    title: "Look", desc: look.category, thumb: thumb,
                    img: nil, item: nil, look: look
                )
            }
        }
    }
    
    private func addImg(_ img: UIImage) {
        Task.main {
            let full = Img(img).limit(.width(850), isOpaque: false, factorScale: 1).get()
            let thumb = Img(full).limit(.width(250), isOpaque: false, factorScale: 1).get()
            self.add(title: "Imagem".l, desc: "sem descrição".l, thumb: thumb, img: full, item: nil, look: nil)
        }
    }
    
    private func addText(_ s: String)
    {
        let thumb = #imageLiteral(resourceName: "img-grey-box")
        add(title: s, desc: "sem descrição", thumb: thumb, img: thumb, item: nil, look: nil)
    }
    
    private func add(title: String?, desc: String?, thumb: UIImage, img: UIImage?, item: Item?, look: Look?)
    {
        let model = LuggageRepo.LuggageItemSudo(
            model: nil,
            title: title ?? "Desconhecido".l,
            desc: desc ?? "Desconhecido".l,
            thumb: thumb,
            img: img,
            item: item,
            look: look,
            isPacked: false
        )
        
        mItems.append(model)
        
        let index = IndexPath(row: mItems.count-1, section: 0)
        uiTv.insertRows(at: [index], with: .automatic)
        uiTv.selectRow(at: index, animated: true, scrollPosition: .top)
    }
    
    private func dialogError(_ msg: String) {
        Dialog(sender: self)
            .title("Opa!".l)
            .msg(msg)
            .ok()
            .present()
    }
    
    @IBAction func onSave(_ sender: Any)
    {
        guard let location = uiLocation.text, location.trim().count > 1 else {
            dialogError("Lugar não preenchido".l)
            return
        }
        
        guard let duration = uiDuration.text, duration.trim().count > 0 else {
            dialogError("Duração não preenchida".l)
            return
        }
        
        // save parent and items
        let model = Repo.luggage.save(
            model: outModel,
            user: mUser,
            location: location.trim(),
            datedAt: mDate,
            duration: duration.trim(),
            items: mItems
        )
        
        // remove items
        Repo.luggage.deleteItems(items: mItemsToRemove)
        mItemsToRemove = []
        
        performSegue(withIdentifier: "sgBackToClosetLuggage", sender: model)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DatePicker {
            vc.outSet(delegate: self, date: mDate)
        } else if let vc = segue.destination as? ItemPicker {
            vc.maxSelection = -1
            vc.delegate = self
        } else if let vc = segue.destination as? ClosetLuggageTextPicker {
            vc.delegate = self
        } else if let vc = segue.destination as? LookPicker {
            vc.maxSelection = -1
            vc.delegate = self
        } else if let vc = segue.destination as? ClosetLuggageItemSlider, let i = uiTv.indexPathForSelectedRow {
            vc.out(models: mItems, index: i.row)
        }
    }
    
}

// MARK: tableview
extension ClosetLuggageEdit: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        uiEmpty.isHidden = mItems.count > 0
        return mItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ClosetLuggageEditCell
        let m = mItems[indexPath.row]
        
        cell.uiTitle.text = Str.capitalizeFirst(s: m.title)
        cell.uiDesc.text = m.desc.lowercased()
        cell.uiImg.image = m.thumb
        
        return cell
    }
    
    @objc func tableViewLong(_ gesture: UILongPressGestureRecognizer) {
        //
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    @IBAction func tableViewLongPress(_ gesture: UILongPressGestureRecognizer) {
        guard gesture.state == .began else { return }
        let point = gesture.location(in: uiTv)
        guard let i = uiTv.indexPathForRow(at: point) else { return }
        
        mItemsToRemove.append(mItems[i.row])
        mItems.remove(at: i.row)
        
        uiTv.deleteRows(at: [i], with: .automatic)
    }
    
}

// MARK: item picker
extension ClosetLuggageEdit: ItemPickerDelegate {
    
    func itemPicker(onSelected items: [Item]) {
        addItems(items)
    }
    
}

// MARK: look picker
extension ClosetLuggageEdit: LookPickerDelegate {
    
    func lookPicker(onSelect models: [Look]) {
        addLooks(models)
    }
    
}

// MARK: text picker
extension ClosetLuggageEdit: ClosetLuggageTextPickerDelegate {

    func closetLuggageTextPicker(onText s: String) {
        let list = s.trim().components(separatedBy: CharacterSet(charactersIn: ",;."))
        for item in list {
            addText(item.trim())
        }
    }
    
}

// MARK: date picker delegate
extension ClosetLuggageEdit: DatePickerDelegate {
    
    func datePicker(onPick date: Date) {
        mDate = date
    }
    
}

// MARK: Image picker delegates
extension ClosetLuggageEdit: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let img = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)]
            as? UIImage
        {
            addImg(img)
        }
        
        picker.dismiss(animated: true)
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }
}

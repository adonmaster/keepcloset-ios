import UIKit

class ClosetLuggageTextPicker: UIViewController {
    
    @IBOutlet weak var edText: UITextField!
    
    var delegate: ClosetLuggageTextPickerDelegate?
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        edText.becomeFirstResponder()
    }
    
    @IBAction func onOk(_ sender: Any) {
        dismiss(animated: true) {
            if let s = self.edText.text {
                self.delegate?.closetLuggageTextPicker(onText: s.trim().uppercased())
            }
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: delegate
protocol ClosetLuggageTextPickerDelegate {
    
    func closetLuggageTextPicker(onText s: String)
    
}

import UIKit

class ClosetLuggageItemPreview: UIViewController {
    
    @IBOutlet weak var uiImg: UIImageView!
    @IBOutlet weak var uiIndex: UILabel!
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiDesc: UILabel!
    
    
    private var mModel: LuggageRepo.LuggageItemSudo?
    private var mIndex: Int?
    
    func outSet(model: LuggageRepo.LuggageItemSudo?, index: Int?) {
        mModel = model
        mIndex = index
    }
    
    func outGetIndex() -> Int? {
        return mIndex
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if let model = mModel {
            Task.main {
                model.loadImg { self.uiImg?.image = $0 }
                self.uiIndex?.text = "#\((self.mIndex ?? 0)+1)"
                self.uiTitle?.text = Str.capitalizeFirst(s: model.title)
                self.uiDesc?.text = model.desc.uppercased()
            }
        }
    }
    
}

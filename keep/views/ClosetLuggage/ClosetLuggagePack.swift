import UIKit

class ClosetLuggagePack: UIViewController {
    
    // ui
    @IBOutlet weak var uiTv: UITableView!
    @IBOutlet weak var uiInfo: UILabel!
    @IBOutlet weak var uiCoverHeader: UIView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    
    // state
    var outModel: Luggage!
    private var mItens: [LuggageRepo.LuggageItemSudo] = [] {
        didSet {
            mSelection = []
            for i in mItens {
                if i.isPacked { mSelection.append(i) }
            }
        }
    }
    private var mSelection: [LuggageRepo.LuggageItemSudo] = []
    
    // methods
    
    override func viewDidLoad() {
        Repo.luggage.getItems(parent: outModel) { [weak self] r in
            self?.mItens = r
            self?.uiTv?.reloadData()
            self?.updateDesktopInfo()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        updateDesktopInfo()
    }
    
    private func updateDesktopInfo()
    {
        let n = mSelection.count
        let pending = n == 0 || n < mItens.count
        
        lblDate.text = outModel.p.datedAt
        lblDuration.text = outModel.p.duration
        lblLocation.text = outModel.location
        
        uiInfo.text = pending
            ? "Empacotando".l+" \(n)/\(mItens.count) ..."
            : "Tudo empacotado!".l
        uiInfo.textColor = pending ? Cl.mc.red700Color() : Cl.mc.green700Color()
        
        // cover
        UIView.animate(withDuration: 0.2) {
            self.uiCoverHeader.backgroundColor = pending ? Cl.mc.red100Color() : Cl.mc.green100Color()
        }
    }
    
}

// MARK: table
extension ClosetLuggagePack: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mItens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ClosetLuggagePackCell
        let m = mItens[indexPath.row]
        
        cell.uiImg.backgroundColor = UIColor.white
        cell.uiImg.image = m.thumb
        cell.uiTitle.text = Str.capitalizeFirst(s: m.title)
        cell.uiDesc.text = m.desc.lowercased()
        cell.uiDesc.alpha = 0.5
        
        let isSelected = mSelection.firstIndex(where: { $0.model == m.model }) != nil
        cell.backgroundColor = isSelected ? UIColor.black : UIColor.white
        cell.uiTitle.textColor = isSelected ? UIColor.white : UIColor.black
        cell.uiDesc.textColor = isSelected ? UIColor.white : UIColor.black
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let m = mItens[indexPath.row]
        let index = mSelection.firstIndex(where: { $0.model == m.model })
        
        // db
        Repo.luggage.itemPack(m, option: index == nil)
        
        // local
        if let i = index {
            mSelection.remove(at: i)
        } else {
            mSelection.append(m)
        }
        
        if let mIndex = mItens.firstIndex(where: { $0.model == m.model }) {
            tableView.reloadRows(at: [IndexPath(row: mIndex, section: 0)], with: .automatic)
        }
        
        updateDesktopInfo()
    }
    
}

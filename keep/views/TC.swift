import UIKit

class TC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if tcHeadIt() { headIt(item: navigationItem) }
    }
    
    func headIt(item: UINavigationItem?) {
        item?.titleView = FragHeader.f(owner: self)
    }
    
    @IBAction func showMenuLeft() {
        slideMenuController()?.openLeft()
    }
    
    func tcHeadIt() -> Bool {
        return true
    }
}

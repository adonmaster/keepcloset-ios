import UIKit

class ClosetMainMenuCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    
}

import UIKit
import WebKit

class SupportWebVC: VC {
    
    @IBOutlet weak var uiWeb: WKWebView!
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    
    var outUrl: String = "http://google.com"
    
    var mLoading = false {
        didSet {
            if mLoading {
                uiLoading.startAnimating()
            } else {
                uiLoading.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiWeb.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mLoading = true
        uiWeb.load(URLRequest(url: URL(string: outUrl)!))
        uiWeb.allowsBackForwardNavigationGestures = false
    }
    
}

// MARK: web view delegate
extension SupportWebVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        mLoading = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        mLoading = false
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        mLoading = false
    }
    
}

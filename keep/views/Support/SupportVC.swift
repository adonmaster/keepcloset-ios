import UIKit

class SupportVC: VC {
    
    @IBOutlet weak var uiTv: UITableView!
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    
    private var mData: [SupportRModel.Item] = [] {
        didSet {
            uiTv?.reloadData()
        }
    }
    private var mLoading = false {
        didSet {
            if mLoading {
                uiLoading.startAnimating()
            } else {
                uiLoading.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Task.main(delayInSeconds: 0.2) {
            
            self.mLoading = true
            let url = Http.shared.url("mobile/support")
            Http.shared.get(SupportRModel.self, uri: url, params: [:])
                .then { rmodel in
                    self.mData = rmodel.pack
                }
                .catchIt { err in
                    Dialog.init(sender: self)
                        .title("Atenção".l).msg(err)
                        .ok().present()
                }
                .done {
                    self.mLoading = false
                }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SupportWebVC, let url = sender as? String {
            vc.outUrl = url
        }
    }
    
}

// MARK: table view
extension SupportVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let m = mData[indexPath.row]
        
        cell.textLabel?.text = m.title
        cell.detailTextLabel?.text = m.desc
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let m = mData[indexPath.row]
        
        performSegue(withIdentifier: "sgShow", sender: m.url)
    }
    
}

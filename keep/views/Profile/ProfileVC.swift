import UIKit

class ProfileVC: VC {
    
    @IBOutlet weak var imgAvatar: ImageViewRounded!
    @IBOutlet weak var edName: TextFieldEx!
    @IBOutlet weak var edEmail: TextFieldEx!
    @IBOutlet weak var btnSocial: UIButton!
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    
    private var mModel: User!
    private var mLoading = false {
        didSet {
            imgAvatar?.alpha = mLoading ? 0.3 : 1.0
            if mLoading {
                uiLoading?.startAnimating()
            } else {
                uiLoading?.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        edName.delegate = self
        
        updateDesktop()
    }
    
    func updateDesktop()
    {
        guard let user = Repo.user.active() else { return }
        
        mModel = user
        
        user.p.thumb(to: imgAvatar)
        edName.text = user.name
        edEmail.text = user.email
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Repo.user.listenUserChanged(caller: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        Repo.user.unlistenUserChanged(caller: self)
    }
    
    @IBAction func backToProfile(_ segue: UIStoryboardSegue) {
        if let source = segue.source as? ProfilePreviewAvatarVC {
            if let img = source.outImage {
                Repo.user.save(user: mModel, avatar: img)
                updateDesktop()
            }
        }
    }
    
    @IBAction func onRemoveImage(_ sender: Any)
    {
        Dialog(sender: self)
            .msg("Escolha uma das ações abaixo:".l)
            .destructive("Remover imagem".l)
            .cancel()
            .present(onDestructive: {
                Task.main(delayInSeconds: 0.2, block: {
                    Repo.user.removeAvatar(user: self.mModel)
                    self.updateDesktop()
                    self.imgAvatar.image = #imageLiteral(resourceName: "img-bknd-guy")
                })
            })
    }
    
    @IBAction func onImportImage(_ sender: Any)
    {
        mLoading = true
        FbSignIn.i.signIn(caller: self, onSuccess: { [weak self] data in
            
            if let url = data.imgUrl {
                
                Http.shared.downloadImage(url: url) { image in
                    
                    guard let img = image else { return }
                    guard let vc = self else { return }
                    Repo.user.save(user: vc.mModel, avatar: img)
                    vc.imgAvatar?.image = img
                }
                
            } else {
                guard let vc = self else { return }
                Dialog(sender: vc).title("Erro".l).msg("Sem imagem disponível".l).ok().present()
            }
            
        }, onError: { str in
            Dialog(sender: self).title("Erro".l).msg(str).ok().present()
        }) {
            self.mLoading = false
        }
    }
}

// MARK: repo changed
extension ProfileVC: EventableDelegate {
    
    func eventable(on key: String, payload: Any?) {
        // just one key
        updateDesktop()
    }
    
}

// MARK: ed name delegate
extension ProfileVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let v = textField.text ?? ""
        if textField === edName && mModel.name != v && !v.isEmpty {
            Repo.user.save(user: mModel, name: v)
        }
    }
    
}

import UIKit

class ProfilePreviewAvatarVC: VC {
    
    @IBOutlet weak var img: UIImageView!
    var outImage: UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        outImage = nil
        if let user = Repo.user.active() {
            user.p.avatar(to: img)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? AvatarPickerVC {
            destination.delegate = self
        }
    }
}

// MARK: avatar picker delegate
extension ProfilePreviewAvatarVC: AvatarPickerDelegate {

    func avatarPicked(didChooseImage img: UIImage) {
        self.img.image = img
        
        outImage = img
        performSegue(withIdentifier: "uwProfile", sender: nil)
    }
    
}

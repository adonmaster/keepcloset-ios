import UIKit


class FragItemPicker: UIViewController {

    // classes & structs
    
    enum SudoItemType {
        case item
        case insp
    }
    
    class SudoItem {
        
        let thumb: Data?
        let full: Data?
        let color1: UIColor?
        let color2: UIColor?
        let type: SudoItemType
        let item: Item?
        let insp: Inspiration?
        
        init(_ thumb: Data?, _ full: Data?, _ color1: UIColor?, _ color2: UIColor?,
             _ type: SudoItemType, _ item: Item?, _ insp: Inspiration?)
        {
            self.thumb = thumb
            self.full = full
            self.color1 = color1
            self.color2 = color2
            self.type = type
            
            self.item = item
            self.insp = insp
        }
        
    }
    
    // ui
    
    @IBOutlet weak var uiCv: UICollectionView!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnColor: UIButton!
    
    private let keyInspiracoes = "* Inspirações".l
    
    var delegate: FragItemPickerDelegate?
    
    private var mData: [SudoItem] = [] {
        didSet {
            uiCv.reloadData()
        }
    }
    private var mUser: User!
    private var mCategories: [String] = []
    private var mColors: [UIColor] = []
    
    
    // methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mUser = Repo.user.active()!
        
        uiCv.dragInteractionEnabled = true
        updateData()
        updateCategory()
        updateColor()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let dest = segue.destination as? StrPicker {
            dest.delegate = self
        }
        
        else if let destination = segue.destination as? ColorPicker {
            destination.delegate = self
        }
    }
    
    private func updateData()
    {
        // only inspirations
        if mCategories.contains(keyInspiracoes)
        {
            let inspirations = Repo.inspiration.get(for: mUser)
            
            mData = inspirations.map({ SudoItem($0.thumb, $0.img, nil, nil, .insp, nil, $0) })
        }
        
        // items
        else {
            let items = Repo.item.query(
                user: mUser,
                categories: mCategories.map { $0 },
                colors: mColors,
                limit: 200
            )
            
            mData = items.map({ SudoItem($0.thumb, $0.img, $0.p.color1, $0.p.color2, .item, $0, nil) })
        }

        uiCv.reloadData()
    }
    
    private func updateCategory()
    {
        updateData()
        
        // filter category
        let span = Span("")
        if mCategories.count > 0 {
            let s = mCategories.map({ $0 }).joined(separator: ", ")
            _ = span.append(s)
                .font(name: "Helvetica Neue-Condensed Bold", size: 17)
                .color(UIColor.black)
        } else {
            _ = span.append("Todas categorias".l).color(GMColor.grey300Color())
        }
        btnCategory?.setAttributedTitle(span.get(), for: .normal)
    }
    
    private func updateColor()
    {
        updateData()
        
        let span = Span("")
        for cl in mColors {
            _ = span.append("").color(cl).font(name: "FontAwesome", size: 17)
        }
        if span.count == 0 {
            _ = span.append("Todas cores")
                .font(name: "Helvetica Neue-Condensed Bold", size: 17)
                .color(GMColor.grey300Color())
        }
        btnColor?.setAttributedTitle(span.get(), for: .normal)
    }
    
}

// MARK: collection
extension FragItemPicker: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FragItemPickerCell
        let m = mData[indexPath.row]
        
        Task.main {
            if let data = m.thumb, let img = UIImage(data: data) {
                cell.uiImg.image = img
            } else {
                cell.uiImg.image = #imageLiteral(resourceName: "404")
            }
        }
        cell.uiCover.alpha = 0.0
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as! FragItemPickerCell
        cell.uiCover.alpha = 1.0
        UIView.animate(withDuration: 0.3, animations: {
            cell.uiCover.alpha = 0
        }) { _ in
            
            self.delegate?.fragItemPicker(onSelect: self.mData[indexPath.row])
            
        }
    }
}

// MARK: delegate
protocol FragItemPickerDelegate {
    
    func fragItemPicker(onSelect item: FragItemPicker.SudoItem)
    
}

// MARK: category & color picker
extension FragItemPicker: StrPickerDelegate, ColorPickerDelegate {
    
    func strPickerOnCancel() {
        mCategories = []
        updateCategory()
    }
    
    func strPickerOnRetrieveData(for indentifier: String?) -> [String] {
        return [keyInspiracoes] + Repo.item.listCategories(user: mUser)
            .map({ $0.uppercased() })
            .sorted()
    }
    
    func strPicker(for identifier: String?, onSelect s: String) {
        mCategories = [s]
        updateCategory()
    }
    
    func colorPicker(didSelect selection: [UIColor], identifier: Int?) {
        mColors = selection
        updateColor()
    }
    
}



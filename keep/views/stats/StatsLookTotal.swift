import UIKit

class StatsLookTotal: UIViewController {
    
    @IBOutlet weak var uiTotal: UILabel!
    @IBOutlet weak var uiItemsAvarage: UILabel!
    @IBOutlet weak var uiItemsMostUsed: UILabel!
    @IBOutlet weak var uiItemsNeverUsed: UILabel!
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    
    private lazy var mNeverUsed = {
        return Repo.stats.looksItemsNeverUsed(for: mUser)
    }()
    
    private lazy var mMostUsed = {
        return Repo.stats.looksItemsMostUsed(for: mUser)
    }()
    
    // state
    private var mUser: User!
    private var loading = false {
        didSet {
            if loading {
                uiLoading.startAnimating()
            } else {
                uiLoading.stopAnimating()
            }
        }
    }
    
    // methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mUser = Repo.user.active()!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateDesktop()
    }
    
    private func updateDesktop()
    {
        uiTotal.text = "??"
        uiItemsAvarage.text = "??"
        uiItemsMostUsed.text = "??"
        uiItemsNeverUsed.text = "??"
        
        loading = true
        Task.main {
            self.uiTotal.text = "\(Repo.stats.looksTotal(for: self.mUser))"
            self.uiItemsAvarage.text = "\(Repo.stats.looksItensAvarage(for: self.mUser))"
            self.uiItemsMostUsed.text = "\(Repo.stats.looksItemsMostUsedCount(for: self.mUser))"
            self.uiItemsNeverUsed.text = "\(Repo.stats.looksItemsNeverUsedCount(for: self.mUser))"
            
            self.loading = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? PicGrid {
            vc.delegate = self
            
            if segue.identifier == "sgMostUsed" {
                vc.outTitle = "itens mais usados".l
                vc.identifier = "mostUsed"
            } else if segue.identifier == "sgNeverUsed" {
                vc.outTitle = "itens nunca usados".l
                vc.identifier = "neverUsed"
            }
        }
    }
}

// MARK: pic grid
extension StatsLookTotal: PicGridDelegate {
    
    func picGridCountAsync(for identifier: String?) -> Int
    {
        let list: [Item] = identifier == "mostUsed"
            ? mMostUsed
            : (identifier == "neverUsed" ? mNeverUsed : [])
        
        return list.count
    }
    
    func picGridAsync(for identifier: String?, on cell: PicGridCell, for index: Int)
    {
        let m: Item? = identifier == "mostUsed"
            ? mMostUsed[index]
            : (identifier == "neverUsed" ? mNeverUsed[index] : nil)
        
        m?.p.thumbLoad(imageView: cell.uiImg)
    }
    
    
    
    
}

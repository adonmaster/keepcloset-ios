import UIKit
import Charts

class StatsCloset: UIViewController {
    
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    @IBOutlet weak var uiTotal: UILabel!
    @IBOutlet weak var uiChartColors: PieChartView!
    @IBOutlet weak var uiChartStatus: PieChartView!
    @IBOutlet weak var uiChartTherm: PieChartView!
    @IBOutlet weak var uiChartBrand: PieChartView!
    
    private lazy var mScheduleMost = {
        return Repo.stats.closetScheduleMost(for: mUser)
    }()

    
    private var mUser: User!
    private var mLoading = false {
        didSet {
            if mLoading {
                uiLoading.startAnimating()
            } else {
                uiLoading.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        mUser = Repo.user.active()!
        
        uiTotal.text = "-"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        mLoading = true
        Task.main {
            self.uiTotal.text = "\(Repo.stats.closetTotal(for: self.mUser))"
            self.updateChart()
            self.mLoading = false
        }
    }
    
    private func configChart(_ chart: PieChartView)
    {
        chart.chartDescription.enabled = false
        chart.drawHoleEnabled = true
        chart.isUserInteractionEnabled = true
        chart.legend.enabled = true
        chart.centerText = ""
        chart.drawEntryLabelsEnabled = false
        chart.entryLabelFont = .systemFont(ofSize: 12, weight: .light)
    }
    
    private func percentIt(_ l: [String:Int]) -> [String:Double] {
        let total = l.reduce(0) { $0 + $1.value }
        var r: [String:Double] = [:]
        l.keys.forEach { s in
            r[s] = Double(l[s]!) * 100.0 / Double(total)
        }
        return r
    }
    
    private func configChartData(_ chart: PieChartView, _ list: [String:Int], _ colorCb: ((Int,String)->UIColor)?=nil)
    {
        let entries: [PieChartDataEntry] = percentIt(list).map { t -> PieChartDataEntry in
            return PieChartDataEntry(value: t.value, label: t.key.lowercased())
        }
        
        let dataset = PieChartDataSet(entries: entries, label: "")
        if let colorCallback = colorCb {
            var index = 0
            dataset.colors = entries.map({ entry in
                let cl = colorCallback(index, entry.label!.lowercased())
                index += 1
                return cl
            })
        }
        dataset.drawValuesEnabled = false
        
        chart.data = PieChartData(dataSet: dataset)
        let nf = NumberFormatter()
        nf.numberStyle = .none
        chart.data!.setValueFormatter(DefaultValueFormatter(formatter: nf))
        
        chart.centerText = ""
        chart.delegate = self
        
        chart.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
    }
    
    private func updateChartColors()
    {
        let list = Repo.stats.closetColors(for: mUser)
        configChart(uiChartColors)
        configChartData(uiChartColors, list) { i, colorName in
            return Res.i.colorPaletteLite.first(where: { $0.0.lowercased() == colorName })?.1 ?? Cl.mc.grey400Color()
        }
    }
    
    private func updateChartStatus()
    {
        let list = Repo.stats.closetStatus(for: mUser)
        configChart(uiChartStatus)
        configChartData(uiChartStatus, list) { i, status in
            return Res.i.colorChartFrom(index: i)
        }
    }
    
    private func updateChartTherm() {
        let list = Repo.stats.closetTherm(for: mUser)
        configChart(uiChartTherm)
        configChartData(uiChartTherm, list) { i, status in
            if let therm = Res.i.therm.first(where: { $0.name == status }) {
                return therm.color
            }
            return Cl.mc.grey400Color()
        }
    }
    
    private func updateChartBrand() {
        let list = Repo.stats.closetBrand(for: mUser)
        configChart(uiChartBrand)
        configChartData(uiChartBrand, list) { i, status in
            return Res.i.colorChartFrom(index: i, reverseIt: true)
        }
    }
    
    private func updateChart()
    {
        updateChartColors()
        updateChartStatus()
        updateChartTherm()
        updateChartBrand()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PicGrid {
            vc.delegate = self
            vc.identifier = segue.identifier
            if segue.identifier == "sgScheduleMost" {
                vc.outTitle = "Mais usados no calendário"
            } else if segue.identifier == "sgScheduleLess" {
                vc.outTitle = "Menos usados no calendário"
            }
        }
    }
}

// MARK: PicGrid
extension StatsCloset: PicGridDelegate {
    
    func picGridData(_ identifier: String?) -> [Item] {
        if identifier == "sgScheduleMost" {
            return mScheduleMost
        }
        return []
    }
    
    func picGridCountAsync(for identifier: String?) -> Int {
        return picGridData(identifier).count
    }
    
    func picGridOnCellDesc(for identifier: String?, index: Int) -> String? {
        let m = picGridData(identifier)[index]
        return "\(m.scheduled?.count ?? 0)"
    }
    
    func picGridAsync(for identifier: String?, on cell: PicGridCell, for index: Int) {
        let m = picGridData(identifier)[index]
        m.p.thumbLoad(imageView: cell.uiImg)
    }
    
}


// MARK: chart view delegate
extension StatsCloset: ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let pieChart = chartView as! PieChartView
        let e = entry as! PieChartDataEntry
        
        pieChart.centerAttributedText = Span("\(Num.toDigits(e.value))%")
            .font(name: "Helvetica Neue", size: 24.0)
            .get()
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        let pieChart = chartView as! PieChartView
        pieChart.centerText = ""
    }
    
}

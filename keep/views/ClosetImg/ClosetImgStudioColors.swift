import UIKit

class ClosetImgStudioColors: UIViewController {
    
    @IBOutlet weak var uiImg: UIImageView!
    @IBOutlet weak var uiSegment: UISegmentedControl!
    @IBOutlet weak var uiCv: UICollectionView!
    @IBOutlet weak var uiColor1: UIView!
    @IBOutlet weak var uiColor2: UIView!
    
    private var resolved = false
    
    private var sImg: UIImage? {
        didSet {
            updateDesktop()
        }
    }
    
    private var sColor1: UIColor? {
        didSet {
            updateDesktop()
        }
    }
    
    private var sColor2: UIColor? {
        didSet {
            updateDesktop()
        }
    }
    
    private lazy var mData = {
        return Res.i.colorPaletteOnlyColors + [UIColor.clear]
    }()
    
    
    // **** methods
    
    func outGetColor() -> (UIColor?, UIColor?) {
        return (sColor1, sColor2)
    }
    
    var outWasResolved: Bool {
        get {
            return resolved
        }
    }
    
    func outSetColor(img: UIImage, color1: UIColor?, color2: UIColor?) {
        sImg = img
        sColor1 = color1
        sColor2 = color2
    }
    
    func updateDesktop()
    {
        uiImg?.image = sImg
        uiColor1?.backgroundColor = sColor1 ?? UIColor.clear
        uiColor2?.backgroundColor = sColor2 ?? UIColor.clear
        uiCv?.reloadData()
    }
    
    
    // ui
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        resolved = false
        
        updateDesktop()
    }
    
    @IBAction func onSegment(_ sender: Any) {
        uiCv.reloadData()
    }
    
    @IBAction func onDone(_ sender: Any) {
        resolved = true
        performSegue(withIdentifier: "sgBackToStudio", sender: nil)
    }
}

extension ClosetImgStudioColors: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 40.0, height: 40.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mData.count
    }
    
    private var cColorFromCurrentSegment: UIColor? {
        set(v) {
            let c: UIColor? = v == UIColor.clear
                ? nil
                : v
            if uiSegment.selectedSegmentIndex == 0 {
                sColor1 = c
            } else if uiSegment.selectedSegmentIndex == 1 {
                sColor2 = c
            }
        }
        get {
            if uiSegment.selectedSegmentIndex == 0 {
                return sColor1
            } else {
                return sColor2
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let m = mData[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ColorCCell

        cell.backgroundColor = cColorFromCurrentSegment == m
            ? UIColor.white
            : UIColor.clear
        cell.uiView.backgroundColor = m
        cell.lblClear.isHidden = m != UIColor.clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let m = mData[indexPath.row]
        cColorFromCurrentSegment = m
    }
    
}

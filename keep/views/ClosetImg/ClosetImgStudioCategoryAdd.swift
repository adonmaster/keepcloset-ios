import UIKit

class ClosetImgStudioCategoryAdd: UIViewController {
    
    @IBOutlet weak var uiEd: UITextField!
    
    private var sDesc: String?
    
    func outGetDesc() -> String? {
        return Str.validate(sDesc)
    }
    
    @IBAction func onDone(_ any: Any) {
        sDesc = uiEd.text
        performSegue(withIdentifier: "sgBackToClosetImgStudioCatAdd", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiEd.becomeFirstResponder()
    }
}

import UIKit

class ClosetImgPreview: VC {
    
    @IBOutlet weak var uiScrollView: UIScrollView!
    @IBOutlet weak var uiImageView: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblTherm: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewColor1: UIView!
    @IBOutlet weak var viewColor2: UIView!
    
    private var sItem: Item!
    private var sImage: UIImage? {
        didSet {
            uiImageView.image = sImage
            
           // Scroll.scrollSetupZoom(sv: uiScrollView, iv: uiImageView)
        }
    }
    
    func outSetItem(_ item: Item) {
        sItem = item
    }
    
    func outGetItem() -> Item {
        return sItem
    }
    
    func outGetImage() -> UIImage? {
        return sImage
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblCategory.text = sItem.p.category.uppercased()
        lblBrand.attributedText = sItem.p.brand
        lblStatus.attributedText = sItem.p.status
        
        lblTherm.text = sItem.p.therm
        lblTherm.textColor = sItem.p.therm(inverted: false)
        
        viewColor1.backgroundColor = sItem.p.color1 ?? UIColor.clear
        viewColor2.backgroundColor = sItem.p.color2 ?? UIColor.clear
        
        sItem.p.fullLoad { [weak self] img in
            self?.sImage = img
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //uiScrollView.layoutIfNeeded()
        //Scroll.scrollCenterImage(sv: uiScrollView, iv: uiImageView, force: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // deprecated (slider)
        if let destination = segue.destination as? ClosetImgStudio {
            destination.outSetFrom(item: sItem, fullImg: sImage)
        }
    }
    
}

// delegate scroll view
extension ClosetImgPreview: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return uiImageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView)
    {
        //Scroll.scrollCenterImage(sv: scrollView, iv: uiImageView)
    }
    
}

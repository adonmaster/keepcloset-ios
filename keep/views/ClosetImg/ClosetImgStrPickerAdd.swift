import UIKit

class ClosetImgStrPickerAdd: UIViewController {
    
    @IBOutlet weak var uiEd: TextFieldEx!
    
    var outResult: String? {
        didSet {
            self.uiEd?.text = outResult
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        uiEd.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        uiEd.text = ""
        outResult = nil
    }
    
    @IBAction func onSave(_ sender: Any)
    {
        if let s = uiEd.text, s.trim().count > 0 {
            outResult = s.trim()
            performSegue(withIdentifier: "sgBackToClosetImgStrPicker", sender: nil)
        }
    }
}

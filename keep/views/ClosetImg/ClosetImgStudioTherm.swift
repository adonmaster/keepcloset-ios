import UIKit

class ClosetImgStudioTherm: UIViewController {
    
    @IBOutlet weak var uiTableView: UITableView!
    
    private var mData = Res.i.therm
    
    private var sTherm: Int? {
        didSet {
            
        }
    }
    
    // # Methods
    
    func outSetTherm(therm: Int?) {
        self.sTherm = therm
    }
    
    func outGetTherm() -> Int? {
        return sTherm
    }
    
    
    // # UI methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateDesktop()
    }
    
    func updateDesktop() {
        if let i = sTherm {
            let m = mData.firstIndex { $0.uid == i }!
            uiTableView.selectRow(at: IndexPath(row: m, section: 0), animated: false, scrollPosition: .bottom)
        }
    }
    
}

// MARK: table view
extension ClosetImgStudioTherm: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ThermCell
        
        let m = mData[indexPath.row]
        cell.lbl.textColor = m.color
        cell.lbl.text = m.desc
        
        cell.lblDesc.text = m.name
        cell.lblDesc.textColor = m.color
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let m = mData[indexPath.row]
        sTherm = m.uid
        
        performSegue(withIdentifier: "sgBackToStudio", sender: nil)
    }
    
}


//
//  Cut10VCFirst.swift
//  keep
//
//  Created by Adonio Silva on 19/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class Cut10FirstVC: UIViewController, UIScrollViewDelegate {
    
    //
    var outImg: UIImage!
    
    //
    @IBOutlet var uiDrawer: Cut10Drawer!
    @IBOutlet var uiContainer: UIView!
    @IBOutlet var uiScroll: UIScrollView!
    @IBOutlet var uiImg: UIImageView!
    @IBOutlet var uiLoading: UIView!
    @IBOutlet var uiBtnDone: UIBarButtonItem!
    
    @IBOutlet var uiBtnBrush: UIButton!
    @IBOutlet var uiBtnErase: UIButton!
    @IBOutlet var uiBtnZoom: UIButton!
    @IBOutlet var uiBtnWand: UIButton!
        
    @IBOutlet var uiNvIndicator: NVActivityIndicatorView!

    
    //
    private var isLoading = false {
        didSet {
            uiLoading?.isHidden = !isLoading
        }
    }
    
    private enum Mode {
        case brush
        case erase
        case zoom
        case none
    }
    private var mMode = Mode.none {
        didSet {
            uiBtnBrush?.alpha = mMode == .brush ? 1 : 0.3
            uiBtnErase?.alpha = mMode == .erase ? 1 : 0.3
            uiBtnZoom?.alpha = mMode == .zoom ? 1 : 0.3
            
            uiDrawer?.touchesEnabled = mMode == .brush || mMode == .erase
            uiDrawer?.isErasing = mMode == .erase
            uiScroll?.isScrollEnabled = mMode == .zoom
        }
    }
    
    private var isWand = false {
        didSet {
            let color = isWand ? UIColor.systemBlue : Cl.mc.grey200Color()
            uiBtnWand?.backgroundColor = color
        }
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiImg.image = outImg
        isLoading = false

        uiNvIndicator.setup()
        
        uiScroll.delegate = self
        
        mMode = .brush
        isWand = true
        
        uiDrawer.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Session.shared.getAndSetTrueAfter("Cut10First@hadHelpAppear") {
            performSegue(withIdentifier: "sgHelp", sender: nil)
        }
        
    }
    
    // scroll view
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return uiContainer
    }
    
    //
    @IBAction func uiBtnModeBrush(_ sender: Any) {
        mMode = .brush
    }
    @IBAction func uiBtnModeErase(_ sender: Any) {
        mMode = .erase
    }
    @IBAction func uiBtnModeZoom(_ sender: Any) {
        mMode = .zoom
    }
    @IBAction func uiBtnModeWand(_ sender: Any) {
        isWand = !isWand
    }
    @IBAction func uiBtnClear(_ sender: Any) {
        uiDrawer.clear()
    }
    

    @IBAction func uiDone(_ sender: Any) {
        performSegue(withIdentifier: "sgDone", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? Cut10DoneVC {
            vc.outImgResized = Img(v: uiImg).get()
            vc.outImgOriginal = outImg
            vc.outImgMaskNegative = uiDrawer.exportMaskNegative()
        }
    }
    
    @IBAction func unwindToCut10First(_ unwindSegue: UIStoryboardSegue) {
        // no need
    }
}


// MARK: drawer delegate
extension Cut10FirstVC: DrawerDelegate {

    func drawerOnTouchReleasedDebounced()
    {
        if isLoading { return }
        if !isWand { return }
        
        isLoading = true
        let img = Img(v: uiImg).get()
        let maskOriginal = uiDrawer.exportMask()

        Task.main { [weak self] in
            if let maskRes = OpenCVWrapper.extractForegroundMask(img, maskOriginal) {
                self?.uiDrawer.importMask(_mask: maskRes)
            }
            self?.isLoading = false
        }
    }
    
    func drawerParentScrollView() -> UIScrollView? {
        return uiScroll
    }
    
}

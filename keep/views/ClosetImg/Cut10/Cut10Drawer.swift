//
//  Cut10Drawer.swift
//  keep
//
//  Created by Adonio Silva on 26/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit

struct DrawerLine {
    var lines = [CGPoint]()
    var strokeWidth: CGFloat = 100.0
    var isEraser = false
}

class Cut10Drawer: UIView {
    
    weak var delegate: DrawerDelegate?
    var DEBOUNCE_SECONDS = 0.88
    var touchesEnabled = true
    var isErasing = false

    //
    
    //
    private var lines = [DrawerLine]()
    private var maskImport: UIImage? = nil
    

    //
    func clear() {
        lines.removeAll()
        maskImport = nil
        
        setNeedsDisplay()
    }

    func exportMask() -> UIImage {
        return drawEx(mode: .export)!
    }
    
    func exportMaskNegative() -> UIImage {
        return drawEx(mode: .exportNegative)!
    }
    
    func importMask(_mask: UIImage) {
        lines.removeAll()
        maskImport = _mask
        
        setNeedsDisplay()
    }
    
    //
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if !touchesEnabled { return }
        guard let touch = touches.first else { return }
        
        var brushSize = UIScreen.main.bounds.width * 0.13
        brushSize = brushSize / (delegate?.drawerParentScrollView()?.zoomScale ?? 1)
        
        let newLine = DrawerLine(
            strokeWidth: brushSize,
            isEraser: isErasing
        )
        lines.append(newLine)
        lines[lines.count - 1].lines.append(touch.location(in: self))
        
        // flag
        Task.debounce(key: "drawer.touches.debounce", delayInSeconds: DEBOUNCE_SECONDS) { }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if !touchesEnabled { return }
        guard let touch = touches.first else { return }
        
        lines[lines.count - 1].lines.append(touch.location(in: self))
        
        setNeedsDisplay()

        // flag
        Task.debounce(key: "drawer.touches.debounce", delayInSeconds: DEBOUNCE_SECONDS) { }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !touchesEnabled { return }
        
        Task.debounce(key: "drawer.touches.debounce", delayInSeconds: DEBOUNCE_SECONDS) { [weak self] in
            self?.delegate?.drawerOnTouchReleasedDebounced()
        }
    }
    
    override func draw(_ rect: CGRect) {
        drawEx(mode: .draw)
    }
    
    enum DrawMode {
        case draw
        case export
        case exportNegative
        func isExporting() -> Bool {
            return self == .export || self == .exportNegative
        }
    }
    
    @discardableResult func drawEx(mode: DrawMode) -> UIImage?
    {
        if mode == .export || mode == .exportNegative {
            UIGraphicsBeginImageContext(frame.size)
        }

        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        var brushColor: UIColor
        var bkndFillColor = UIColor.white
        switch mode {
        case .draw:
            brushColor = UIColor.red
            bkndFillColor = UIColor.clear
        case .export:
            brushColor = UIColor.white
            bkndFillColor = UIColor.black
        case .exportNegative:
            brushColor = UIColor.black
            bkndFillColor = UIColor.white
        }
        
        if mode.isExporting() {
            context.setFillColor(bkndFillColor.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        }

        // export mask
        if let mask = maskImport {
            context.saveGState()
            
            context.translateBy(x: 0, y: frame.size.height)
            context.scaleBy(x: 1, y: -1)
            
            context.clip(to: CGRect(origin: CGPoint.zero, size: frame.size), mask: mask.cgImage!)
            context.setFillColor(brushColor.cgColor)
            context.fill(CGRect(origin: CGPoint.zero, size: frame.size))
            
            context.restoreGState()
        }

        // lines
        context.setStrokeColor(brushColor.cgColor)
        context.setLineJoin(.round)
        context.setLineCap(.round)
        
        for line in lines {
            guard let firstPt = line.lines.first else { continue }
            
            context.setLineWidth(line.strokeWidth)
            context.setBlendMode(line.isEraser ? .clear : .normal)
            
            // ao inves de apagar, qdo estah se exportando vou fazer stroke de linhas pretas
            if mode.isExporting() && line.isEraser {
                context.setBlendMode(.normal)
                context.setStrokeColor(bkndFillColor.cgColor)
            }
            
            context.beginPath()
            context.move(to: firstPt)
            
            for pt in line.lines.dropFirst() {
                if line.lines.count > 0 { context.addLine(to: pt) }
            }
            
            context.strokePath()
        }
        
        var exportImg: UIImage? = nil
        if mode.isExporting() {
            exportImg = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }
        
        return exportImg
    }
}

protocol DrawerDelegate: NSObject {
    
    func drawerOnTouchReleasedDebounced()
    func drawerParentScrollView() -> UIScrollView?
    
}

//
//  Cut10DoneVC.swift
//  keep
//
//  Created by Adonio Silva on 27/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class Cut10DoneVC: UIViewController {
    
    //
    @IBOutlet var uiImg: UIImageView!
    @IBOutlet var uiProgress: NVActivityIndicatorView!
    
    //
    var outImgResized: UIImage!
    var outImgOriginal: UIImage!
    var outImgMaskNegative: UIImage!
    
    //
    var cLoading = false {
        didSet {
            uiProgress?.isHidden = !cLoading
        }
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiProgress.setup()
        
        // pricy operation.... but just fractions of seconds
        cLoading = true
        Task.main {
            self.assignImg()
            self.cLoading = false
        }
    }
    
    @IBAction func uiDone(_ sender: Any) {
        if cLoading { return }
        
        performSegue(withIdentifier: "unClosetImgStudio", sender: nil)
    }
    
}

// MARK: utils
extension Cut10DoneVC {
    
    func assignImg()
    {
        guard
            let _img = outImgOriginal,
            let _mask = outImgMaskNegative
            else { return }
        
        let imgLimited = Img(_img).limit(.width(1200)).get()
        let img = imgLimited.cgImage!
        let maskAdjusted = adjustMask(imgLimited, _mask, UIColor.white)
        let mask = maskAdjusted.cgImage!
        
        // prepare to mask original image
        let maskref = CGImage(
            maskWidth: mask.width, height: mask.height, bitsPerComponent: mask.bitsPerComponent,
            bitsPerPixel: mask.bitsPerPixel, bytesPerRow: mask.bytesPerRow,
            provider: mask.dataProvider!, decode: nil, shouldInterpolate: false
        )!
        
        if let masked = img.masking(maskref) {
            uiImg.image = UIImage(cgImage: masked).trimmingTransparentPixels(maximumAlphaChannel: 15)
        }
    }
    
    private func adjustMask(_ img: UIImage, _ mask: UIImage, _ fillBknd: UIColor = UIColor.blue) -> UIImage
    {
        // Determine the scale factor that preserves aspect ratio
        let wRatio = img.size.width / mask.size.width

        // Compute the new image size that preserves aspect ratio
        let scaledImageSize = CGSize(width: mask.size.width * wRatio, height: mask.size.height * wRatio)
        
        //
        let offPt = CGPoint(
            x: (img.size.width - scaledImageSize.width)/2,
            y: (img.size.height - scaledImageSize.height)/2
        )
        
        // Draw and return the resized UIImage
        
        return UIGraphicsImageRenderer(size: img.size).image { c in
            fillBknd.setFill()
            c.fill(CGRect(origin: CGPoint.zero, size: img.size))
            
            mask.draw(in: CGRect(origin: offPt, size: scaledImageSize))
        }
    }
    
}

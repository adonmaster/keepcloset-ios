import UIKit

class ClosetImgMainCVHeader: UICollectionReusableView {
    
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnColor: UIButton!
    @IBOutlet weak var lblInfo: UILabel!
    
}

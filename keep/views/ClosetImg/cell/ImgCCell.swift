import UIKit

class ImgCCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var viewCover: UIView!
    
    func highlightIt(_ option: Bool) {
        viewCover.isHidden = !option
    }
    
}

import UIKit

class ClosetImgStudioCategory: UIViewController {
    
    @IBOutlet weak var uiTableView: UITableView!
    
    private var sCategory: String?
    
    private var sCategories: [String] = [] {
        didSet {
            mData = sCategories.map { $0 }
        }
    }
    
    private var mUser: User!
    
    private var mData: [String] = [] {
        didSet {
            updateDesktop()
        }
    }
    
    // # Methods
    
    func outSetCategory(category: String?) {
        self.sCategory = category
    }
    
    func outGetCategory() -> String? {
        return sCategory
    }
    
    // ui
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mUser = Repo.user.active()!
        sCategories = Repo.item.listCategories(user: mUser)
            .map({ $0.uppercased() })
            .sorted()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateDesktop()
    }

   
    func updateDesktop() {
        uiTableView?.reloadData()
        if let s = sCategory {
            if !mData.contains(s) { mData.append(s) }
            let i = mData.firstIndex { $0 == s }!
            uiTableView?.selectRow(at: IndexPath(row: i, section: 0), animated: false, scrollPosition: .bottom)
        }
    }
    
    // unwind
    @IBAction func backToClosetImgStudioCategory(_ segue: UIStoryboardSegue)
    {
        if let source = segue.source as? ClosetImgStudioCategoryAdd, let desc = source.outGetDesc() {
            if !mData.contains(desc) { mData.append(desc) }

            let i: Int! = mData.firstIndex { $0.lowercased() == desc.lowercased() }
            uiTableView.scrollToRow(at: IndexPath(row: i, section: 0), at: .top, animated: true)
        }
    }
    
}

// MARK: table
extension ClosetImgStudioCategory: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let m = mData[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = m
        
        let n = Repo.item.countCategories(user: mUser, category: m)
        cell.detailTextLabel?.textColor = n <= 0 ? GMColor.grey500Color() : GMColor.blue700Color()
        cell.detailTextLabel?.text = Str.plural(n, "sem item".l, "1 item") { "\($0) "+"itens".l }
        
        cell.isSelected = m.lowercased() == sCategory?.lowercased()
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        sCategory = mData[indexPath.row]
        
        performSegue(withIdentifier: "backToStudio", sender: nil)
    }
    
}

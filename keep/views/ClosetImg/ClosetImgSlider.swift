import UIKit

class ClosetImgSlider: UIPageViewController {
    
    // state
    private var mData: [Item] = []
    private var mIndex = 0
    
    // method
    
    func outSet(items: [Item], curIndex: Int) {
        mData = items
        mIndex = curIndex
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // closet pic nav item
        navigationItem.titleView = FragHeader.f(owner: self)
        
        delegate = self
        dataSource = self
        
        setViewControllers([newvc(mIndex)], direction: .forward, animated: false) { _ in
            // done
        }
    }
    
    private func newvc(_ index: Int) -> ClosetImgPreview
    {
        let vc = storyboard!.instantiateViewController(withIdentifier: "imgPreview") as! ClosetImgPreview
        let model = mData[index]
        vc.outSetItem(model)
        return vc
    }
    
    private func curvc() -> ClosetImgPreview? {
        return viewControllers?.first as? ClosetImgPreview
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // edit
        if let destination = segue.destination as? ClosetImgStudio, let cur = curvc() {
            destination.outSetFrom(item: mData[mIndex], fullImg: cur.outGetImage())
        }
    }
    
}

extension ClosetImgSlider: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        if !completed { return }
        
        if let cur = curvc(), let i = mData.firstIndex(of: cur.outGetItem()) {
            mIndex = i
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex-1
        if i < 0 || mData.count == 0 { return nil }
        
        return newvc(i)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex+1
        if i > mData.count-1 { return nil }
        
        return newvc(i)
    }

}

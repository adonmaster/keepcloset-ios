import UIKit

class ClosetImgStudio: VC {
    
    // declarations
    @IBOutlet weak var uiImg: UIImageView!
    @IBOutlet weak var uiBtnWand: UIButton!
    @IBOutlet weak var uiBtnCategory: UIButton!
    @IBOutlet weak var uiBtnTherm: UIButton!
    @IBOutlet weak var uiBtnColors: UIButton!
    @IBOutlet weak var uiBtnBrand: UIButton!
    @IBOutlet weak var uiBtnStatus: UIButton!
    
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    
    private var loading = false {
        didSet {
            if loading {
                uiLoading?.startAnimating()
            } else {
                uiLoading?.stopAnimating()
            }
        }
    }
    
    private var mUser: User!
    private var mModel: Item?
    
    private var sImg: UIImage? {
        didSet {
            updateDesktop()
        }
    }
    
    private var sWanded = false {
        didSet {
            updateDesktop()
        }
    }
    
    private var sCategory: String? {
        didSet {
            updateDesktop()
        }
    }
    
    private var sTherm: Int? {
        didSet {
            updateDesktop()
        }
    }
    
    private var sColor1: UIColor? {
        didSet {
            updateDesktop()
        }
    }
    
    private var sColor2: UIColor? {
        didSet {
            updateDesktop()
        }
    }
    
    private var sBrand: String? {
        didSet {
            updateDesktop()
        }
    }
    
    private var sStatus: String? {
        didSet {
            updateDesktop()
        }
    }
    
    // state
    
    
    // methods
    
    func outSetFrom(img: UIImage) {
        outSet(model: nil, img: img)
    }
    
    func outSetFrom(item: Item, fullImg: UIImage?=nil)
    {
        outSet(model: item, therm: Int(item.therm_uid), category: item.category,
            color1: item.color1, color2: item.color2,
            brand: item.brand, status: item.status, img: fullImg
        )
        
        // load image
        if fullImg==nil {
            item.p.fullLoad { [weak self] img in
                self?.sImg = img
            }
        }
    }
    
    private func outSet(model: Item?, therm: Int?=nil, category: String?=nil, color1: String?=nil,
                        color2: String?=nil, brand: String?=nil, status: String?=nil, img: UIImage?)
    {
        mModel = model
        sTherm = therm
        sCategory = (category ?? "").count > 0 ? category : nil
        sColor1 = Cl.from(hex: color1)
        sColor2 = Cl.from(hex: color2)
        sBrand = brand
        sStatus = status

        sImg = img
        
        if mModel != nil { sWanded = true }
    }
    
    override func viewDidLoad() {
        mUser = Repo.user.active()!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateDesktop()
    }
    
    func updateDesktop()
    {
        // go on!
        uiImg?.image = sImg
        
        let okColor = Cl.mc.green500Color()
        let pendingColor = Cl.mc.red500Color()
        
        uiBtnWand?.setTitleColor(sWanded ? okColor : pendingColor, for: .normal)
        uiBtnCategory?.setTitleColor(sCategory != nil ? okColor : pendingColor, for: .normal)
        
        uiBtnTherm?.setTitleColor(sTherm != nil ? okColor : pendingColor, for: .normal)
        if let uid = sTherm {
            let res = Res.i.therm.first { $0.uid == uid }!
            uiBtnTherm?.setTitle(res.desc, for: .normal)
            uiBtnTherm?.setTitleColor(res.color, for: .normal)
        }
        
        // colors
//        uiBtnColors?.backgroundColor = sColor1 != nil || sColor2 != nil ? okColor : pendingColor
        var asColor: NSAttributedString = Span("").color(UIColor.black).get()
        if sColor1 != nil || sColor2 != nil {
            let square = ""
            let span = Span("")
            if let cl = sColor1 {
                _ = span.append(square).color(cl)
            }
            if let cl = sColor2 {
                _ = span.append(" " + square).color(cl)
            }
            asColor = span.get()
        }
        uiBtnColors?.setAttributedTitle(asColor, for: .normal)
        
        // brand
        uiBtnBrand?.setTitleColor(sBrand != nil ? okColor : pendingColor, for: .normal)
        
        // status
        uiBtnStatus?.setTitleColor(sStatus != nil ? okColor : pendingColor, for: .normal)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // category
        if let dest = segue.destination as? ClosetImgStudioCategory {
            dest.outSetCategory(category: sCategory)
        }
        // therm
        else if let dest = segue.destination as? ClosetImgStudioTherm {
            dest.outSetTherm(therm: sTherm)
        }
        // color
        else if let dest = segue.destination as? ClosetImgStudioColors {
            dest.outSetColor(img: sImg!, color1: sColor1, color2: sColor2)
        }
        // brand
        else if let dest = segue.destination as? ClosetImgStrPicker, segue.identifier == "sgStatusPicker" {
            dest.title = "Escolha o status".l
            dest.outSet(identifier: "status", item: nil, delegate: self) { () -> [String] in
                return Repo.item.listStatus(user: self.mUser)
                    .sorted()
                    .map({ $0.uppercased() })
            }
        }
        // status
        else if let dest = segue.destination as? ClosetImgStrPicker, segue.identifier == "sgBrandPicker"  {
            dest.title = "Escolha a marca".l
            dest.outSet(identifier: "brand", item: nil, delegate: self) { () -> [String] in
                return Repo.item.listBrand(user: self.mUser)
                    .sorted()
                    .map({ $0.uppercased() })
            }
        }
        // cut 10 - direct
        else if let dest = VC.resolveTopVC(segue.destination) as? Cut10FirstVC, let i = sImg {
            dest.outImg = i
        }
    }

    
    @IBAction func onDone(_ sender: Any)
    {
        guard let img = sImg else { return }
        
        loading = true
        Task.main {
            let full = Img(img).limit(.height(1600), isOpaque: false, factorScale: 1).get()
            let thumb = Img(img).limit(.height(240), isOpaque: false, factorScale: 1).get()
            
            _ = Repo.item.save(
                itemModel: self.mModel,
                user: self.mUser,
                category: self.sCategory,
                thermId: self.sTherm ?? 3,
                img: full,
                thumb: thumb,
                color1: self.sColor1,
                color2: self.sColor2,
                brand: self.sBrand,
                status: self.sStatus
            )
            
            self.loading = false
            self.performSegue(withIdentifier: "backToClosetImgMain", sender: nil)
        }
    }
    
    // unwind
    @IBAction func backToClosetImgStudio(_ segue: UIStoryboardSegue)
    {
        // commit
        
        // category
        if let source = segue.source as? ClosetImgStudioCategory {
            sCategory = source.outGetCategory()
        }
        // therm
        else if let source = segue.source as? ClosetImgStudioTherm {
            sTherm = source.outGetTherm()
        }
        // colors
        else if let source = segue.source as? ClosetImgStudioColors {
            if source.outWasResolved {
                let colors = source.outGetColor()
                sColor1 = colors.0
                sColor2 = colors.1
            }
        }
        // cut 10
        else if let vc = segue.source as? Cut10DoneVC
        {
            loading = true
            
            Task.main(delayInSeconds: 0.3) { [weak self] in
                guard let _img = vc.uiImg.image else { return }
                
                let img = Img(_img).trim(alphaGreaterThan: 0.2).get()
                self?.sImg = img
                
                // after editing, assign new colors
                let thumb = Img(img).limit(.width(250), isOpaque: false, factorScale: 1).get()
                let colors = Img.extractColor(from: thumb, amount: 2, from: Res.i.colorPaletteLiteColors)
                
                self?.sColor1 = colors.first
                self?.sColor2 = Arr(colors).second
                self?.sWanded = true
                
                self?.loading = false
            }
        }
    }
    
}


// MARK: pickers
extension ClosetImgStudio: ClosetImgStrPickerDelegate {

    func closetImgStrPicker(for identifider: String?, onSelect item: String) {
        if identifider == "brand" {
            sBrand = item
        } else if identifider == "status" {
            sStatus = item
        }
    }
    
}

import UIKit
import Foundation
import NVActivityIndicatorView


class ClosetImgMain: VC {
    
    @IBOutlet weak var constraintTopHint: NSLayoutConstraint!
    @IBOutlet weak var uiViewTip: ViewRoundCorners!
    @IBOutlet weak var uiCv: UICollectionView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var uiTip: ViewRoundCorners!
    @IBOutlet var uiLoading: UIView!
    @IBOutlet var uiNvIndicator: NVActivityIndicatorView!
    
    
    // state
    private var mUser: User!
    private var mData = [(String, [Item])]()
    private var cSelected: [IndexPath] = [] {
        didSet {
            let cb: (UICollectionView, [IndexPath])->() = {
                $0.reloadItems(at: $1)
            }
            uiCv?.withValidIndexes(oldValue, cb: cb)
            uiCv?.withValidIndexes(cSelected, cb: cb)
        }
    }
    
    private var mPendingUrls = [URL]()
    
    private var mFilter: String? {
        didSet {
            updateDesktop()
            updateFilterDesktop()
        }
    }
    private var mLoading = false {
        didSet {
            self.uiLoading?.isHidden = !mLoading
        }
    }
    
    
    // methods ui
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        mLoading = false
        uiNvIndicator.setup()
        
        btnFilter.titleLabel?.textAlignment = .center

        mUser = Repo.user.active()!
        
        Repo.pref.firstView(uiViewTip, key: "ClosetImgMain@tips") {
            Anim.floater(uiViewTip)
        }
        constraintTopHint.constant = 0
        uiTip.isHidden = constraintTopHint.constant <= 0
        
        updateDesktop()
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(notificationOnApplicationDidBecomeActive(_:)),
            name: .exApplicationDidBecomeActive, object: nil
        )
    }
    
    @objc func notificationOnApplicationDidBecomeActive(_ sender: Any) {
        processPending()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(onSearch(_:)))
        ]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        processPending()
    }
    
    private func processPending() {
        // load pending urls
        let key = SharedPref.key_shared_pending_closet
        mPendingUrls = Arr(SharedPref.shared.get(key: key)).distinct { $0.absoluteString }
        
        // call for pending share import
        let pendingCount = mPendingUrls.count
        if pendingCount > 0 && !ShareImportVC.isShowing {
            performSegue(withIdentifier: "sgShareImport", sender: nil)
        }
    }
    
    func updateDesktop()
    {
        updateFilterDesktop()
        
        // data
        let data = Repo.item.query(user: mUser, q: mFilter)
        mData = Arr(data).categorize({ $0.category ?? "Sem categoria".l }, sorted: true)
        
        // collection view
        uiCv?.reloadData()
    }
    
    func updateFilterDesktop()
    {
        var mDataCount = 0
        mData.forEach {
            $0.1.forEach { _ in mDataCount += 1 }
        }
        
        var msg = ""
        var suffix = ""
        if let s = mFilter {
            msg = "filtro para: \"".l+"\(s)\", "
            suffix = "\n(Aperte para remover filtro)".l
        }
        msg += Str.plural(mDataCount, "sem resultados".l, "um resultado".l, { "\($0) "+"resultados".l })
        
        btnFilter?.setTitle(msg + suffix, for: .normal)
    }
    
    @IBAction func onAdd(_ sender: Any) {
        Anim.hide(uiViewTip)

        // ipad stuff
        Dialog(sender: self)
            .stylesheet(popoverSourceView: sender as? UIView)
            .title("Qual opção?".l)
            .msg("Câmera ou album?".l)
            .action(key: "Câmera".l, cb: {_ in
                Media.presentPicker(caller: self, .camera, delegate: self)
            })
            .action(key: "Album", cb: {_ in
                Media.presentPicker(caller: self, .photoLibrary, delegate: self)
            })
            .cancel()
            .present();
    }
    
    @IBAction func onFilterTap(_ sender: Any) {
        mFilter = nil
    }
    
    @objc func onSearch(_ btn: Any) {
        performSegue(withIdentifier: "sgModalSearch", sender: nil)
    }
    
    // unwind
    @IBAction func backToClosetImgMain(_ segue: UIStoryboardSegue) {
        updateDesktop()
    }
    
    // prepare
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // add
        if let vc = segue.destination as? ClosetImgStudio {
            let img = sender as! UIImage
            vc.outSetFrom(img: img)
        }
        
        // slider
        else if let dest = segue.destination as? ClosetImgSlider, let i = uiCv.indexPathsForSelectedItems?.first {
            let models: [Item] = Arr(mData).uncategorize()
            let index = models.firstIndex(of: mData[i.section].1[i.row])!
            dest.outSet(items: models, curIndex: index)
        }
        
        // search
        else if let dest = segue.destination as? ModalSearch {
            dest.outValue = mFilter
            dest.outPh = "Insira marca, status, cor ou categoria...".l
            dest.delegate = self
        }

        // share import
        else if let dest = segue.destination as? ShareImportVC {
            dest.delegate = self
        }
    }
}


// MARK: collection view
extension ClosetImgMain: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return mData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        let n = mData.count
        
        // show hint after 1st item added
        if n > 0 {
            Repo.pref.firstView(uiViewTip, key: "ClosetImgMain@firstItemTip") {
                self.constraintTopHint.constant = 120
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
                
                // disappear
                Task.main(delayInSeconds: 5, block: { [weak self] in
                    self?.constraintTopHint.constant = 0
                    UIView.animate(withDuration: 0.4, animations: {
                        self?.view.layoutIfNeeded()
                    })
                })
            }
        }
        
        updateFilterDesktop()
        
        return mData[section].1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let m = mData[indexPath.section].1[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImgCCell
        
        // load img
        m.p.thumbLoad(imageView: cell.img)
        
        // highlight it
        cell.highlightIt(cSelected.contains(indexPath))
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (collectionView.frame.width - 4) / 3
        return CGSize(width: w, height: w)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        let identifier = kind == UICollectionView.elementKindSectionFooter ? "footer" : "header"
        if let cell = collectionView
            .dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: identifier, for: indexPath)
            as? ClosetImgMainHeader
        {
            let m = mData[indexPath.section]
            
            cell.uiTitle.text = m.0.trim().uppercased()
            
            return cell;
        }

        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: identifier, for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
    {
        return CGSize(width: 0, height: section==mData.count-1 ? 60 : 0)
    }
    
    
    @IBAction func collectionViewLongClick(gesture: UILongPressGestureRecognizer)
    {
        guard gesture.state == .began else { return }
        let point = gesture.location(in: uiCv)
        guard let i = uiCv.indexPathForItem(at: point) else { return }
        
        cSelected = [i]
        
        Dialog(sender: self)
            .title("Atenção".l).msg("Escolha uma das opções:".l)
            .destructive("Apagar registro".l).cancel()
            .present(onOk: nil, onCancel: {
                self.cSelected = []
            }, onDestructive: {
                self.collectionView(delete: i)
            })
    }
    
    func collectionView(delete item: IndexPath)
    {
        // perform updates outside dialog's domains
        Task.main(delayInSeconds: 0.2) {

            // remove db
            Repo.item.remove(self.mData[item.section].1[item.row])
            
            // local
            self.mData[item.section].1.remove(at: item.row)
            
            // ui
            let isSectionEmpty = self.mData[item.section].1.count == 0
            if isSectionEmpty {
                self.mData.remove(at: item.section)
                self.uiCv.deleteSections(IndexSet(integer: item.section))
            } else {

                self.uiCv.deleteItems(at: [item])
            }
            
            self.cSelected = []
        }
        
    }
    
}

// MARK: Image Picker
extension ClosetImgMain: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let img = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        {
            performSegue(withIdentifier: "sgStudio", sender: img)
        }
        
        picker.dismiss(animated: true)
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }
    
}

// MARK: modal search
extension ClosetImgMain: ModalSearchDelegate {
    
    func modalSearch(onQuery text: String?) {
        mFilter = text
    }
    
}

// MARK: share import delegate
extension ClosetImgMain: ShareImportDelegate {

    func shareImportRetrieveUrls() -> [URL] {
        return mPendingUrls
    }
    
    func shareImportRetrieveCategories() -> [String] {
        return Repo.item.listCategories(user: mUser)
            .map({ $0.uppercased() })
            .sorted()
    }
    
    func shareImportOnDone(urls: [URL], category: String?) {
        mLoading = true
        Task.main {
            urls.forEach { url in
                if !FileManager.default.fileExists(atPath: url.path) { return }
                
                // prepare images
                let img = UIImage(contentsOfFile: url.path)!
                let full = Img(img).limit(.height(1600), isOpaque: false, factorScale: 1).get()
                let thumb = Img(img).limit(.height(240), isOpaque: false, factorScale: 1).get()
                
                // save in db
                Repo.item.save(
                    itemModel: nil,
                    user: self.mUser, category: category, thermId: 3, img: full, thumb: thumb,
                    color1: nil, color2: nil, brand: nil, status: nil
                )
            }
            
            self.shareImportCleanup(urls: urls)
            
            self.updateDesktop()
            
            self.mLoading = false
        }
        
    }
    
    func shareImportCleanup(urls: [URL]) {
        // delete files
        urls.forEach { try? FileManager.default.removeItem(at: $0) }
        
        // remove urls
        SharedPref.shared.put(key: SharedPref.key_shared_pending_closet, list: [])
    }
    
}

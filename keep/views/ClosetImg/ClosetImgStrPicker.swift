import UIKit

class ClosetImgStrPicker: UIViewController {
    
    // ui
    @IBOutlet weak var uiTv: UITableView!

    // state
    private var delegate: ClosetImgStrPickerDelegate?
    private var sIdentifier: String?
    private var sItem: String?
    private var sListGetter: (()->[String])?
    
    private var mData: [String] = []
    
    private var stTime = false
    
    // methods
    
    func outSet(identifier: String?, item: String?, delegate: ClosetImgStrPickerDelegate,
                cbListGetter: @escaping ()->[String])
    {
        sIdentifier = identifier
        sItem = item
        self.delegate = delegate
        sListGetter = cbListGetter
    }
    
    override func viewDidLoad() {
        uiTv.rowHeight = UITableView.automaticDimension
        
        mData = sListGetter?() ?? []
        uiTv.reloadData()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !stTime {
            stTime = true
            
            if mData.count == 0 {
                performSegue(withIdentifier: "sgStrPickerAdd", sender: nil)
            }
        }
    }
 
    @IBAction func backToClosetImgStrPicker(_ segue: UIStoryboardSegue)
    {
        if let vc = segue.source as? ClosetImgStrPickerAdd, let s = vc.outResult
        {
            var i: Int! = Str.firstIndex(s: s, l: mData)
            let insertIt = i == nil
            if insertIt {
                mData.insert(s, at: 0)
                i = 0
            }
             
            Task.main(delayInSeconds: 0.3)
            {
                let index = IndexPath(row: i, section: 0)
                if insertIt {
                    self.uiTv.performBatchUpdates({
                        self.uiTv.insertRows(at: [index], with: .automatic)
                    }, completion: { _ in
                        self.uiTv.selectRow(at: index, animated: true, scrollPosition: .top)
                    })
                }
            }
        }
    }
    
}

// MARK: table
extension ClosetImgStrPicker: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ClosetImgStrPickerCell
        
        cell.uiTitle.text = mData[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        performSegue(withIdentifier: "sgBack", sender: nil)
        
        delegate?.closetImgStrPicker(for: sIdentifier, onSelect: mData[indexPath.row])
    }
    
}

// MARK: delegate
protocol ClosetImgStrPickerDelegate {
    
    func closetImgStrPicker(for identifider: String?, onSelect item: String)
    
}


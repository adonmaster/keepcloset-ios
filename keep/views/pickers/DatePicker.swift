import UIKit

class DatePicker: UIViewController {
    
    // ui
    @IBOutlet weak var uiDate: UIDatePicker!
    
    // var
    private var mDelegate: DatePickerDelegate?
    private var mDate: Date = Carbon().get()
    
    // methods
    override func viewDidLoad() {
        uiDate.locale = Carbon.locale
    }
    
    func outSet(delegate: DatePickerDelegate, date: Date?) {
        self.mDelegate = delegate
        self.mDate = date ?? Carbon().get()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        uiDate.setDate(self.mDate, animated: false)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onOk(_ sender: Any) {
        dismiss(animated: true) {
            self.mDelegate?.datePicker(onPick: self.uiDate.date)
        }
    }
    
}

// MARK: delegate
protocol DatePickerDelegate {
    
    func datePicker(onPick date: Date)
    
}

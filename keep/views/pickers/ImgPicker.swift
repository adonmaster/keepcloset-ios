import UIKit

class ImgPicker: UIViewController {
    
    @IBOutlet weak var uiCv: UICollectionView!
    
    var delegate: ImgPickerDelegate?
    var identifier: String?
    
    private var mSelection: [Int] = []
    private var mMax: Int {
        return delegate?.imgPickerMaxSelection?(for: identifier) ?? 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onOk(_ sender: Any) {
        dismiss(animated: true) {
            //
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true) {
            //
        }
    }
}

// MARK: collection
extension ImgPicker: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let w = (collectionView.frame.width - 4) / 3
        return CGSize(width: w, height: w)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return delegate?.imgPickerCount(for: identifier) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImgPickerCell
        delegate?.imgPickerAssignThumb(for: identifier, index: indexPath.row, assign: cell.uiImg)
        
        let isSelected = mSelection.contains(indexPath.row)
        cell.uiImg.alpha = isSelected ? 0.3 : 1
        cell.uiCheck.isHidden = isSelected
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var involved: [Int] = [indexPath.row]
        
        if let i = mSelection.firstIndex(of: indexPath.row) {
            mSelection.remove(at: i)
        } else {
            mSelection.append(indexPath.row)
        }
        if mSelection.count > mMax {
            involved.append(mSelection.removeFirst())
        }
        
        collectionView.reloadItems(at: involved.map({ IndexPath(row: $0, section: 0) }))
    }

}

// MARK: delegate
@objc protocol ImgPickerDelegate {
    
    func imgPicker(for identifier: String?, selected indexes: [Int])
    func imgPickerAssignThumb(for identifier: String?, index: Int, assign imgView: UIImageView)
    func imgPickerCount(for identifier: String?) -> Int
    @objc optional func imgPickerMaxSelection(for identifier: String?) -> Int
    
    
}

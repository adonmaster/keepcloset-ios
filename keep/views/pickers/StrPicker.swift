import UIKit

class StrPicker: UIViewController {
    
    // ui
    @IBOutlet weak var uiTv: UITableView!
    @IBOutlet weak var uiBtnAdd: UIButton!
    @IBOutlet weak var uiBtnAddHeight: NSLayoutConstraint!
    
    // public
    var identifier: String?
    var delegate: StrPickerDelegate?
    
    // state
    private var mData: [String] = []
    private var mStViewed = false
    private var mIsEditMode: Bool {
        return self.delegate?.strPickerIsEditMode?(for: identifier) ?? true
    }
    
    // methods
    
    override func viewDidLoad() {
        uiTv.rowHeight = UITableView.automaticDimension
        
        Task.main {
            self.mData = self.delegate?.strPickerOnRetrieveData(for: self.identifier) ?? []
            self.uiTv?.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)

        // edit mode
        uiBtnAdd?.isHidden = !mIsEditMode
        uiBtnAddHeight?.constant = 0
        uiBtnAddHeight?.isActive = !mIsEditMode
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if !mStViewed && mData.count == 0 {
            mStViewed = true
            performSegue(withIdentifier: "sgAdd", sender: nil)
        }
    }

    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.strPickerOnCancel?(for: self.identifier)
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool
    {
        if identifier == "sgAdd" { return mIsEditMode }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? StrPickerAdd {
            vc.delegate = self
        }
    }
    
}

// MARK: table
extension StrPicker: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StrPickerCell
        
        cell.uiTitle.text = mData[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        dismiss(animated: true) {
            let s = self.mData[indexPath.row]
            self.delegate?.strPicker(for: self.identifier, onSelect: s)
        }
    }

}

// MARK: add picker
extension StrPicker: StrPickerAddDelegate {

    func strPickerAdd(on s: String)
    {
        mData.insert(s, at: 0)
        
        Task.main {
            let i = IndexPath(row: 0, section: 0)
            self.uiTv.performBatchUpdates({
                self.uiTv.insertRows(at: [i], with: .automatic)
            }, completion: { _ in
                self.uiTv.selectRow(at: i, animated: true, scrollPosition: .top)
            })
        }
    }
    
}

// MARK: delegate
@objc protocol StrPickerDelegate {
    
    func strPickerOnRetrieveData(for identifier: String?) -> [String]
    func strPicker(for identifier: String?, onSelect s: String)
    @objc optional func strPickerOnCancel(for identifier: String?)
    @objc optional func strPickerIsEditMode(for identifier: String?) -> Bool
    
    
}

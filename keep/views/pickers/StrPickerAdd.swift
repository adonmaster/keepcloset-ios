import UIKit

class StrPickerAdd: UIViewController {
    
    @IBOutlet weak var uiEd: TextFieldEx!
    
    var delegate: StrPickerAddDelegate?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        uiEd.becomeFirstResponder()
    }
    
    @IBAction func onOk(_ sender: Any) {
        dismiss(animated: true) {
            if let s = self.uiEd.text, s.count > 0 {
                self.delegate?.strPickerAdd(on: s.trim())
            }
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true)
    }
}

// MARK: protocol
protocol StrPickerAddDelegate {
    
    func strPickerAdd(on s: String)
    
}

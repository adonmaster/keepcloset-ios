import UIKit

class ColorPicker: UIViewController {
    
    @IBOutlet weak var lblColors: UILabel!
    
    var delegate: ColorPickerDelegate?
    var outLimitColors: Int? = nil
    var outIdentifier: Int? = nil
    
    private let mData: [UIColor] = Res.i.colorPaletteOnlyColors
    
    private var mSelection: [UIColor] = [] {
        didSet {
            updateColorLabel()
        }
    }
    
    // methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateColorLabel()
    }
    
    private func updateColorLabel()
    {
        let square = ""
        
        let span = Span("")
        for i in mSelection {
            if span.count > 0 { _ = span.append(" ") }
            _ = span.append(square).color(i)
        }
        
        lblColors?.attributedText = span.get()
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        delegate?.colorPicker(didSelect: mSelection, identifier: outIdentifier)
        dismiss(animated: true, completion: nil)
    }
}

// MARK: delegate
protocol ColorPickerDelegate {
    func colorPicker(didSelect selection: [UIColor], identifier: Int?)
}

// MARK: collection
extension ColorPicker: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ColorPickerCell
        let m = mData[indexPath.row]
        
        cell.backgroundColor = m
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let m = mData[indexPath.row]
        
        if let i = mSelection.firstIndex(of: m) {
            mSelection.remove(at: i)
        } else {
            mSelection.append(m)
        }
        
        if let l = outLimitColors {
            if mSelection.count > l, mSelection.count > 0 {
                mSelection.remove(at: 0)
            }
        }
    }
    
}

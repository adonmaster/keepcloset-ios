import UIKit

class WrapperVC: SlideMenuVC {
    
    override func mainStoryboardToLoad() -> String? {
        return Repo.pref.getLastStoryboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        Repo.pref.first("WrapperVc@showWizIntro", cb: {
            self.performSegue(withIdentifier: "sgWizIntro", sender: nil)
        })
        
        // bkup intro wiz
        let key_seen = "WrapperVc@showWizIntroBkupSeen"
        if !Repo.pref.get(key_seen) {
            let key_count = "WrapperVc@showWizIntroBkupCount"
            let i: Int = Repo.pref.get(key_count)
            if i <= 15 {
                Repo.pref.set(key_count, i + 1)
            } else {
                Repo.pref.set(key_seen, true)
                self.performSegue(withIdentifier: "sgWizIntroBkup", sender: nil)
            }
        }
    }
    
}

import UIKit

class MainVC: VC {
    
    @IBOutlet weak var imgAvatar: ImageViewRounded!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateDesktop()
        Repo.user.listenUserChanged(caller: self)
        Repo.user.listenOnError(caller: self)
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        Repo.user.unlistenOnError(caller: self)
        Repo.user.unlistenUserChanged(caller: self)
    }
    
    private func updateDesktop()
    {
        if let model = Repo.user.active()
        {
            lblTitle.text = model.name
            lblDesc.text = model.email
            model.p.avatar(to: imgAvatar)
        }
    }
}

// MARK: repo event
extension MainVC: EventableDelegate {

    func eventable(on key: String, payload: Any?)
    {
        if key == Repo.user.EV_USER_CHANGED {
            updateDesktop()
        }
        
        else if key == Repo.user.EV_ON_ERROR {
            if let reason = payload as? String {
                errorIt(msg: reason)
            }
        }
    }
    
}

import UIKit

class DrawerVC: VC {
    
    @IBOutlet weak var imgAvatar: ImageViewRounded!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!

    private let mMenuData: [MenuItem] = [
        .cell("Início".l, #imageLiteral(resourceName: "menu-icon-home"), .closet),
        .cell("Estatísticas".l, #imageLiteral(resourceName: "menu-icon-graph"), .stats),
        .divider,
        .cell("Suporte".l, #imageLiteral(resourceName: "menu-icon-info"), .support),
        .cell("Siga o KeepCloset".l, #imageLiteral(resourceName: "menu-icon-user-add"), .about),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateDesktop()
        Repo.user.listenUserChanged(caller: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imgAvatar.roundIt()
    }

    func updateDesktop() {
        let model = Repo.user._first()!
        
        lblTitle.text = model.name!.uppercased()
        lblDesc.text = model.email
        model.p.avatar(to: imgAvatar)
    }
    
    
    // MARK: menu item struct
    enum MenuItem {
        case cell(String, UIImage, MenuCaller)
        case divider
    }
    
    enum MenuCaller {
        case none
        case closet
        case profile
        case about
        case stats
        case support
    }

}

// MARK: eventable
extension DrawerVC: EventableDelegate {
    
    func eventable(on key: String, payload: Any?) {
        if key == Repo.user.EV_USER_CHANGED {
            updateDesktop()
        }
    }
    
}


// MARK: table view
extension DrawerVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mMenuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch mMenuData[indexPath.row] {
        case .cell(let desc, let img, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCell
            cell.img.image = img
            cell.lblDesc.text = desc
            
            return cell
        case .divider:
            return tableView.dequeueReusableCell(withIdentifier: "divider", for: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch mMenuData[indexPath.row]
        {
        case .cell(_, _, let caller):
            switch caller {
            case .none:
                performSegue(withIdentifier: "sgDummy", sender: nil)
            case .closet:
                performSegue(withIdentifier: "sgCloset", sender: nil)
            case .about:
                performSegue(withIdentifier: "sgAbout", sender: nil)
            case .stats:
                performSegue(withIdentifier: "sgStats", sender: nil)
            case .support:
                performSegue(withIdentifier: "sgSupport", sender: nil)
            default:
                break;
            }
        default:
            break;
        }
        
        closeLeft()
    }
}

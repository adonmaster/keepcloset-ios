import UIKit

class AboutVC: VC {
    
    @IBOutlet weak var lblVersion: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        lblVersion.text = "Versão".l+" \(App.i.version)";
    }
    
    // ui
    
    @IBAction func onClickFb(_ sender: Any) {
        App.i.openLink("https://web.facebook.com/keepclosetapp")
    }
    
    @IBAction func onClickInsta(_ sender: Any) {
        App.i.openLink("https://instagram.com/keepclosetapp")
    }

}

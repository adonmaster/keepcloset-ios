//
//  ShareImportVC.swift
//  keep
//
//  Created by Adonio Silva on 30/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit

class ShareImportVC: UIViewController {
    
    static var isShowing = false
    
    //
    @IBOutlet weak var uiCv: UICollectionView!
    @IBOutlet weak var uiEdCategory: UITextField!
    @IBOutlet var uiLblInfo: UILabel!
    
    //
    weak var delegate: ShareImportDelegate? = nil
    
    //
    private var mData = [URL]() {
        didSet {
            uiCv?.reloadData()
        }
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Self.isShowing = true
        mData = delegate?.shareImportRetrieveUrls() ?? []
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Self.isShowing = false
    }
    
    @IBAction func onSearchCategory(_ sender: Any) {
        performSegue(withIdentifier: "sgStrPicker", sender: nil)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.shareImportCleanup(urls: self.mData)
        }
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.shareImportOnDone(urls: self.mData, category: self.uiEdCategory.text?.nillify)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? StrPicker {
            vc.delegate = self
            vc.identifier = "category"
        }
    }
}


// MARK: delegate
protocol ShareImportDelegate: NSObject {
    
    func shareImportRetrieveUrls() -> [URL]
    func shareImportRetrieveCategories() -> [String]
    func shareImportOnDone(urls: [URL], category: String?)
    func shareImportCleanup(urls: [URL])
    
}

// MARK: str picker delegate
extension ShareImportVC: StrPickerDelegate {

    func strPickerOnRetrieveData(for identifier: String?) -> [String] {
        return delegate?.shareImportRetrieveCategories() ?? []
    }
    
    func strPicker(for identifier: String?, onSelect s: String) {
        uiEdCategory.text = s
    }
    
}

// MARK: ShareImportVC
extension ShareImportVC: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let n = mData.count
        uiLblInfo.text = Str.plural(n, "sem item para exibição".l, "uma foto para importar".l, "$0 fotos para importar".l)
        return n
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)  as! ShareImportCell
        
        let m = mData[indexPath.row]
        
        Task.main {

            if let imgData = try? Data(contentsOf: m), let img = UIImage(data: imgData, scale: 0.5) {
                cell.uiImg.image = img
            } else {
                cell.uiImg.image = #imageLiteral(resourceName: "404")
            }
        }
        
        return cell
    }

    
}

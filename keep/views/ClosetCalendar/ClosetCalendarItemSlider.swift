import UIKit

class ClosetCalendarItemSlider: UIPageViewController {
    
    private var mIndex = 0
    private var mData: [ScheduleImg] = []
    
    func outSet(models: [ScheduleImg], index: Int) {
        mData = models
        mIndex = index
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assert(mData.count > 0)
        
        delegate = self
        dataSource = self
        setViewControllers([newvc(mIndex)], direction: .forward, animated: false, completion: nil)
    }
    
    private func newvc(_ i: Int) -> ClosetCalendarItemPreview {
        let vc = storyboard!.instantiateViewController(withIdentifier: "preview") as! ClosetCalendarItemPreview
        vc.outSet(model: mData[i], index: i)
        return vc
    }
    
}

extension ClosetCalendarItemSlider: UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        if !completed { return }
        
        if let cur = viewControllers?.first as? ClosetCalendarItemPreview {
            mIndex = cur.outGetIndex()
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex-1
        if i < 0 || mData.count == 0 { return nil }
        
        return newvc(i)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex+1
        if i > mData.count-1 { return nil }
        
        return newvc(i)
    }

}

import UIKit
import CropViewController


class ClosetCalendarDetail: VC {
    
    // ui
    @IBOutlet weak var uiCv: UICollectionView!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var uiEmpty: UIView!
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    
    // state
    private var mDate = Date()
    private var mUser: User!
    private var mData: [ScheduleImg] = []
    private var mLoading: Bool = false {
        didSet {
            if mLoading {
                uiLoading.startAnimating()
            } else {
                uiLoading.stopAnimating()
            }
        }
    }
    
    
    // methods
    
    func outSetDate(_ dt: Date) {
        self.mDate = dt
        self.mUser = Repo.user.active()!
        updateData()
    }
    
    private func updateData() {
        mData = Repo.scheduleImg.getFor(user: mUser, date: mDate)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // update date desktop
        let c = Carbon(mDate)
        
        lblMonth.text = c.format(pattern: "MMMM").uppercased()
        lblYear.text = c.format(pattern: "yyyy")
        lblDate.text = c.format(pattern: "dd")
        lblDay.text = c.format(pattern: "eeee").uppercased()
    }
    
    @IBAction func onAdd(_ sender: Any) {
        Dialog(sender: self)
            .stylesheet(popoverSourceView: sender as? UIView)
            .action(key: "Album de fotos".l, cb: { _ in
                ImgProviderNative.i.presentPicker(caller: self, .photoLibrary)
            })
            .action(key: "Câmera".l, cb: { _ in
                ImgProviderNative.i.presentPicker(caller: self, .camera)
            })
            .action(key: "Closet", cb: { _ in
                Task.main {
                    self.performSegue(withIdentifier: "sgItemPicker", sender: nil)
                }
            })
            .action(key: "Look", cb: { _ in
                Task.main {
                    self.performSegue(withIdentifier: "sgLookPicker", sender: nil)
                }
            })
            .cancel()
            .title("Calendário".l)
            .msg("Escolha uma das opções".l)
            .present()
    }
    
    private func save(_ img: UIImage, color1: String?=nil, color2: String?=nil, item: Item?, look: Look?)
    {
        var cl1: String! = color1
        var cl2: String! = color2
        
        let thumb = Img(img).limit(.height(200), isOpaque: false).get()
        if color1 == nil {
            let colors = Img.extractColor(from: thumb, amount: 2, from: Res.i.colorPaletteLiteColors)
            cl1 = Cl.toHexOp(color: colors.first)
            cl2 = Cl.toHexOp(color: Arr(colors).second)
        }
        
        let model = Repo.scheduleImg.createFor(
            user: mUser,
            date: mDate,
            img: img,
            thumb: thumb,
            color1: cl1,
            color2: cl2,
            item: item,
            look: look
        )
        
        mData.append(model)
        
        uiCv.insertItems(at: [IndexPath(row: mData.count-1, section: 0)])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? ColorPicker {
            vc.delegate = self
            vc.outLimitColors = 2
            let identifier = uiCv.indexPathsForSelectedItems?.first?.row
            vc.outIdentifier = identifier
        }
        else if let vc = segue.destination as? ItemPicker {
            vc.delegate = self
            vc.maxSelection = -1
        }
        else if let vc = segue.destination as? LookPicker {
            vc.maxSelection = 1
            vc.delegate = self
        }
        else if let vc = segue.destination as? ClosetCalendarItemSlider, let i = uiCv.indexPathsForSelectedItems?.first {
            vc.outSet(models: mData, index: i.row)
        }
    }
    
    @IBAction func onLongGesture(_ gesture: UILongPressGestureRecognizer)
    {
        guard gesture.state == .began else { return }
        let point = gesture.location(in: uiCv)
        guard let i = uiCv.indexPathForItem(at: point) else { return }
        
        Dialog(sender: self)
            .title("Atenção".l).msg("Escolha uma das opções:".l)
            .action(key: "Alterar cores".l, cb: { _ in
                self.uiCv.selectItem(at: i, animated: true, scrollPosition: .top)
                Task.main(delayInSeconds: 0.2) {
                    self.performSegue(withIdentifier: "sgColorPicker", sender: nil)
                }
            })
            .destructive("Apagar registro".l).cancel()
            .present(onOk: nil, onCancel: {
                // do nothing?
            }, onDestructive: {
                self.collectionView(delete: i.row)
            })

    }
}

// MARK: collection view
extension ClosetCalendarDetail: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let n = mData.count
        uiEmpty.isHidden = n > 0
        return n
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ClosetCalendarDetailCell
        
        let m = mData[indexPath.row]
        if let thumbData = m.thumb {
            cell.uiImg.image = UIImage(data: thumbData)
        } else {
            cell.uiImg.image = #imageLiteral(resourceName: "404")
        }
        cell.uiCl1.backgroundColor = m.color1==nil ? UIColor.clear : Cl.from(hex: m.color1!)
        cell.uiCl2.backgroundColor = m.color2==nil ? UIColor.clear : Cl.from(hex: m.color2!)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let w = (collectionView.frame.width - 4) / 3
        return CGSize(width: w, height: w + 30)
    }
    
    private func collectionView(delete index: Int)
    {
        let model = mData[index]
        
        // local
        mData.remove(at: index)
        
        // db
        Repo.scheduleImg.remove(model)
        
        // collection view
        uiCv.deleteItems(at: [IndexPath(row: index, section: 0)])
    }
    
}

// MARK: Image picker delegates
extension ClosetCalendarDetail: ImgProviderNativeDelegate {
    
    func imgProviderNative(onImgs imgs: [UIImage]) {
        if let img = imgs.first {
            mLoading = true
            Task.main {
                self.save(img, item: nil, look: nil)
                self.mLoading = false
            }
        }
    }
    
    func imgProviderNativeLimit() -> Img.LimitType? {
        .width(800)
    }
    
    func imgProviderNativeAllowsEditing() -> Bool {
        return false
    }
    
    // required
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        ImgProviderNative.i.imagePickerController(self, picker, didFinishPickingMediaWithInfo: info)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int)
    {
        ImgProviderNative.i.cropViewController(self, cropViewController, didCropToImage: image, withRect: cropRect, angle: angle)
    }
    
}

// MARK: item picker
extension ClosetCalendarDetail: ItemPickerDelegate {

    func itemPicker(onSelected items: [Item])
    {
        mLoading = true
        Task.main {
            for item in items {
                let data = item.img
                var img: UIImage! = data == nil ? nil : UIImage(data: data!)
                img = img ?? #imageLiteral(resourceName: "404")
                
                self.save(img, color1: item.color1, color2: item.color2, item: item, look: nil)
            }
            self.mLoading = false
        }
    }
    
}

// MARK: LookPicker
extension ClosetCalendarDetail: LookPickerDelegate {
    
    func lookPicker(onSelect models: [Look]) {
        mLoading = true
        Task.main {
            if let look = models.first {
                look.p.loadFull { img in
                    self.save(img, item: nil, look: look)
                }
            }
            self.mLoading = false
        }
    }
    
}


// MARK: Color picker
extension ClosetCalendarDetail: ColorPickerDelegate {

    func colorPicker(didSelect selection: [UIColor], identifier: Int?) {
        
        // update colors
        if let index = identifier {
            Repo.scheduleImg.updateColors(
                model: mData[index],
                color1: Cl.toHexOp(color: selection.first),
                color2: Cl.toHexOp(color: Arr(selection).second)
            )
            uiCv.reloadItems(at: [IndexPath(row: index, section: 0)])
        }
        
    }
    
}

import UIKit
import JTAppleCalendar

class ClosetCalendarMain: UIViewController {
    
    // uid
    
    @IBOutlet weak var uiCalendarView: JTACMonthView!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    
    
    // state
    private var mFirstView = false
    private var mUser: User!
    private var mData: [String:[ScheduleImg]] = [:]
    private var mLastUpdatedDate = Carbon().add(-15, .year).get()
    private var mLastDateSelected = Carbon().get()

    
    // methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        mUser = Repo.user.active()!
        setupCalendarView()
    }
    
    func setupCalendarView()
    {
        uiCalendarView.calendarDelegate = self
        uiCalendarView.calendarDataSource = self

        uiCalendarView.minimumLineSpacing = 2
        uiCalendarView.minimumInteritemSpacing = 2
        uiCalendarView.visibleDates({ (item) in
            let date = item.monthDates.first!.date
            
            self.lblMonth.text = Carbon(date).format(pattern: "MMMM").uppercased()
            self.lblYear.text = Carbon(date).format(pattern: "yyyy").uppercased()
        })
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        tabBarController?.navigationItem.rightBarButtonItems = []
        
        if !mFirstView {
            mFirstView = true
            
            mLastUpdatedDate = Carbon().get()
            uiCalendarView.scrollToDate(mLastUpdatedDate, triggerScrollToDateDelegate: false, animateScroll: false)
        }
        
        updateCalendarData(date: mLastUpdatedDate, force: true)
    }
    
    private func updateCalendarData(date: Date, force: Bool=false)
    {
        if !Carbon(mLastUpdatedDate).isSameDate(date) || force
        {
            mLastUpdatedDate = date
            
            Task.main {
                
                // categorize
                self.mData = [:]
                let d = Repo.scheduleImg.get(user: self.mUser, forMonth: date)
                for model in d {
                    if self.mData[model.date!] == nil {
                        self.mData[model.date!] = []
                    }
                    self.mData[model.date!]?.append(model)
                }
                
                // date array to update
                let dates = self.mData.keys.map { Carbon.parseFromSqlDate($0)!.get() } + [self.mLastDateSelected]
                self.uiCalendarView.reloadDates(dates)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ClosetCalendarDetail {
            if let selectDate = uiCalendarView.selectedDates.first {
                vc.outSetDate(selectDate)
                mLastDateSelected = selectDate
            }
        }
    }
    
}

// MARK: calendar

extension ClosetCalendarMain: JTACMonthViewDelegate, JTACMonthViewDataSource {
    
    private func calendarUpdateCellImage(_ cell: JTACDayCell, _ date: Date)
    {
        let c = cell as! ClosetCalendarCell
        if let list = mData[Carbon(date).formatSqlDate()],
            let imgModel = list.last,
            let imgData = imgModel.thumb
        {
            Task.main {
                c.imgItem1.image = UIImage(data: imgData)
            }
        } else {
            c.imgItem1.image = nil
        }
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath)
    {
        calendarUpdateCellImage(cell, date)
    }

    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        return ConfigurationParameters(
            startDate: Carbon().add(-5, .year).get(),
            endDate: Carbon().add(5, .year).get(),
            numberOfRows: 6,
            firstDayOfWeek: .sunday
        )
    }
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell
    {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "cell", for: indexPath) as! ClosetCalendarCell
        
        let isInThisMonth = cellState.dateBelongsTo == .thisMonth
        cell.isHidden = !isInThisMonth
        if isInThisMonth {
            
            updateDesktopCellCover(cell, cellState.isSelected, cellState)
            
        }
        
        return cell
    }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath)
    {
        updateDesktopCellCover(cell, true, cellState)
        
        performSegue(withIdentifier: "sgDetail", sender: nil)
    }
    
    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath)
    {
        updateDesktopCellCover(cell, false, cellState)
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo)
    {
        let date = visibleDates.monthDates.first!.date
        
        updateCalendarData(date: date)
        
        lblMonth.text = Carbon(date).format(pattern: "MMMM").uppercased()
        lblYear.text = Carbon(date).format(pattern: "yyyy").uppercased()
    }
    
    private func updateDesktopCellCover(_ cell: JTACDayCell?, _ isSelected: Bool, _ state: CellState)
    {
        if let c = cell as? ClosetCalendarCell
        {
            c.bkndCover.isHidden = true
//            c.bkndCover.isHidden = !isSelected
//            c.bkndCover.backgroundColor = Cl.black1
//            c.lbl.textColor = isSelected ? UIColor.white : UIColor.black
            c.lbl.textColor = UIColor.black
            c.lbl.text = state.text
            
            let isThisMonth = state.dateBelongsTo == .thisMonth
            c.lbl.alpha = isThisMonth ? 1 : 0.5
            
            if Carbon(state.date).isToday() {
                c.lbl.textColor = Cl.mc.red700Color()
//                c.bkndCover.backgroundColor = Cl.mc.red700Color()
//                c.bkndCover.isHidden = false
            }
            else if Carbon(state.date).isFuture() {
//                c.lbl.textColor = isSelected ? UIColor.white : Cl.mc.red700Color()
                c.lbl.textColor = UIColor.black
            }
            
            // data image stuff
            calendarUpdateCellImage(c, state.date)
        }
    }
    
}

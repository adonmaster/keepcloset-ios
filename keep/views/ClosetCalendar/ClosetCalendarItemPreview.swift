import UIKit

class ClosetCalendarItemPreview: UIViewController {
    
    @IBOutlet weak var uiImg: UIImageView!
    @IBOutlet weak var uiColors: UILabel!
    
    private var mModel: ScheduleImg!
    private var mIndex = 0
    
    func outSet(model: ScheduleImg, index: Int) {
        mModel = model
        mIndex = index
    }
    
    func outGetIndex() -> Int {
        return mIndex
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mModel.p.loadFull { self.uiImg.image = $0 }
        uiColors.attributedText = mModel.p.colors
    }
    
}

import UIKit

class RegisterCodeVC: VC {
    
    @IBOutlet weak var edCode: TextFieldEx!
    
    var outCode: String?
    
    @IBAction func onSubmit(_ sender: Any) {
        outCode = edCode.text
        performSegue(withIdentifier: "sgBack", sender: self)
    }
    
}

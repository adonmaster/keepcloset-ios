class RegisterMainController: Controller<RegisterMainView> {
    
    private var sLoading = false {
        didSet {
            if sLoading { view?.viewCode(onError: nil) }
            view?.viewCode(onLoading: sLoading)
        }
    }

    var loading: Bool {
        set {
            sLoading = newValue
        }
        get {
            return sLoading
        }
    }
    
    override func afterAttached() {
        view?.viewCode(onLoading: sLoading)
    }
    
    
    func requestCode(email: String)
    {
        sLoading = true
        
        // beta
        if Env.beta_user_email == email {
            Task.main(delayInSeconds: 2) {
                self.view?.viewCodeOnSuccess()
                self.sLoading = false
            }
            
            return;
        }
        
        // non beta
        Http.shared.post(RModel.self, uri: "api/register/code", RegisterParam(email: email))
            .then { rmodel in
                self.view?.viewCodeOnSuccess()
            }
            .catchIt { reason in
                self.view?.viewCode(onError: reason)
            }
            .done {
                self.sLoading = false
            }
    }
    
    func requestConfirmation(email: String, code: String)
    {
        sLoading = true
        
        // beta
        if Env.beta_user_email == email
        {
            Task.main(delayInSeconds: 2) {
                if Env.beta_user_code == code {
                    self.view?.viewConfirmationSuccess()
                } else {
                    self.view?.viewCode(onError: "Código inválido!")
                }
                self.sLoading = false
            }
            
            return;
        }
        
        // non beta
        Http.shared.post(RModel.self, uri: "api/register/check", CheckParam(email: email, code: code))
            .then { rmodel in
                self.view?.viewConfirmationSuccess()
            }
            .catchIt { reason in
                self.view?.viewCode(onError: reason)
            }
            .done {
                self.sLoading = false
            }
    }
}


// MARK: view
protocol RegisterMainView {
    
    func viewCode(onLoading option: Bool)
    func viewCode(onError reason: String?)
    func viewCodeOnSuccess()
    
    func viewConfirmationSuccess()
    
}

// param and rmodel
fileprivate struct RModel: Decodable {
    var message: String
}
fileprivate struct RegisterParam: Encodable {
    let email: String
}
fileprivate struct CheckParam: Encodable {
    let email: String
    let code: String
}

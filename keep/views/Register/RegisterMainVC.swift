import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn


class RegisterMainVC: VC {

    @IBOutlet weak var edEmail: TextFieldEx!
    @IBOutlet weak var viewLoading: ViewRoundCorners!
    @IBOutlet weak var pbLoading: UIActivityIndicatorView!
    
    private let controller = RegisterMainController()

    // ui methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        controller.loading = false
        GoogleSignIn.i.initialize(caller: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        controller.attach(view: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        controller.detach()
    }
    
    @IBAction func onFb(_ sender: Any)
    {
        signIn(server: .fb)
    }
    
    @IBAction func onGmail(_ sender: Any) {
        signIn(server: .google)
    }
    
    @IBAction func submitEmail(_ sender: Any)
    {
        // PATCH ADON: remove email confirmation
        // controller.requestCode(email: Str.gtrim(edEmail.text))
        
        if !Str.isValidEmail(edEmail.text!) {
            Dialog.init(sender: self).msg("Email inválido").ok().present()
            return
        }
        
        onSuccess(email: Str.gtrim(edEmail.text), name: "Desconhecido", server: .own, avatar: nil)
    }
    
    private func signIn(server: UserRepo.OAuthServer)
    {
        var signable: SignInable!
        if server == .fb { signable = FbSignIn.i }
        else if server == .google { signable = GoogleSignIn.i }
        
        controller.loading = true
        signable.signIn(caller: self, onSuccess: { data in
            
            self.onSuccess(email: data.email, name: data.name, server: server, avatar: data.imgUrl)
            
        }, onError: { reason in
            self.errorIt(msg: reason)
        }) {
            self.controller.loading = false
        }
    }
    
    
    @IBAction func onBackRegisterMain(_ segue: UIStoryboardSegue)
    {
        if let source = segue.source as? RegisterCodeVC {
            if let code = source.outCode {
                controller.requestConfirmation(email: Str.gtrim(edEmail.text), code: Str.gtrim(code))
            }
        }
    }
    
    func onSuccess(email: String, name: String, server: UserRepo.OAuthServer, avatar: URL?)
    {
        Repo.user.save(email: email, name: name, server: server, avatar: avatar)
        
        // prepare to init
        Repo.seed();
        (UIApplication.shared.delegate as! AppDelegate).configInitialScreen()
    }
}

// MARK: view
extension RegisterMainVC: RegisterMainView {

    func viewCode(onLoading option: Bool) {
        viewLoading.isHidden = !option
    }
    
    func viewCode(onError reason: String?) {
        errorIt(msg: reason)
    }
    
    func viewCodeOnSuccess() {
        performSegue(withIdentifier: "sgCode", sender: nil)
    }
    
    func viewConfirmationSuccess() {
        onSuccess(email: Str.gtrim(edEmail.text), name: "Desconhecido", server: .own, avatar: nil)
    }
    
}


// MARK: google signIn
extension RegisterMainVC: GIDSignInDelegateAll {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        GoogleSignIn.i.delegate(signIn, didSignInFor: user, withError: error)
    }
    
}


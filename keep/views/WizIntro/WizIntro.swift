import UIKit

class WizIntro: UIPageViewController {
    
    private var mIndex = 0 {
        didSet {
            uiPc?.currentPage = mIndex
        }
    }
    private var mData = [
        "closet", "edit", "looks", "insp", "calendar", "luggage", "stats", "tuts", "thanku"
    ]
    
    private var uiPc: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViewControllers([newvc(mIndex)], direction: .forward, animated: false)
        delegate = self
        dataSource = self
        
        // page control
        uiPc = UIPageControl(frame:
            CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50)
        )
        uiPc.numberOfPages = mData.count
        uiPc.currentPage = 0
        uiPc.tintColor = UIColor.black
        uiPc.pageIndicatorTintColor = UIColor.white
        uiPc.currentPageIndicatorTintColor = UIColor.black
        view.addSubview(uiPc)
    }
    
    func newvc(_ index: Int) -> WizIntroBase {
        let vc = storyboard!.instantiateViewController(withIdentifier: mData[index]) as! WizIntroBase
        vc.wizIndex = index
        vc.delegate = self
        return vc
    }
    
    func curvc() -> WizIntroBase? {
        return viewControllers?.first as? WizIntroBase
    }
    
}

// MARK: delegate
extension WizIntro: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        if !completed { return }
        
        if let curIndex = curvc()?.wizIndex {
            mIndex = curIndex
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex-1
        if i < 0 || mData.count == 0 { return nil }
        
        return newvc(i)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex+1
        if i > mData.count-1 { return nil }
        
        return newvc(i)
    }
    
}

// MARK: base delegate
extension WizIntro: WizIntroBaseDelegate {
    
    func wizIntroBaseOnNextClicked()
    {
        let newIndex = mIndex+1
        if (newIndex < mData.count)
        {
            setViewControllers([newvc(newIndex)], direction: .forward, animated: true, completion: nil)
            mIndex = newIndex
        }
        else
        {
            dismiss(animated: true, completion: nil)
        }
    }
    
}

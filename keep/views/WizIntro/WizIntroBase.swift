import UIKit

class WizIntroBase: UIViewController {
    
    var wizIndex: Int!
    var delegate: WizIntroBaseDelegate?
    
    @IBAction func onNext(_ b: Any) {
        delegate?.wizIntroBaseOnNextClicked()
    }
    
    @IBAction func onDone(_ b: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

protocol WizIntroBaseDelegate {
    
    func wizIntroBaseOnNextClicked()
    
}

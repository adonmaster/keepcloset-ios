import UIKit

class ModalSearch: UIViewController {
    
    @IBOutlet weak var uiEdt: TextFieldEx!
    
    var delegate: ModalSearchDelegate?
    var outValue: String?
    var outPh: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        uiEdt.text = outValue
        uiEdt.placeholder = outPh
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        uiEdt.becomeFirstResponder()
    }
    
    private func submit() {
        let s = uiEdt.text?.trim() ?? ""
        delegate?.modalSearch(onQuery: s.count > 0 ? s : nil)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true) {
            //
        }
    }
    
    @IBAction func onOk(_ sender: Any) {
        dismiss(animated: true) { self.submit() }
    }
    
}

// MARK: edit delegate
extension ModalSearch: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismiss(animated: true) { self.submit() }
        return true
    }
    
}

// MARK: delegate
protocol ModalSearchDelegate {
    
    func modalSearch(onQuery text: String?)
    
}

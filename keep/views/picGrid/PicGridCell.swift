import UIKit

class PicGridCell: UICollectionViewCell {
    
    @IBOutlet weak var uiImg: UIImageView!
    @IBOutlet weak var uiCover: ViewRoundCorners!
    @IBOutlet weak var uiLbl: UILabel!
    
    
}

import UIKit

class PicGrid: UIViewController {
    
    @IBOutlet weak var uiCv: UICollectionView!
    @IBOutlet weak var uiEmpty: UIStackView!
    @IBOutlet weak var uiTitle: UILabel!
    
    var delegate: PicGridDelegate?
    var identifier: String?
    var outTitle: String = "Lista".l
    
    private var mCount: Int = 0 {
        didSet {
            uiCv?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Task.main {
            self.mCount = self.delegate?.picGridCountAsync(for: self.identifier) ?? 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        uiTitle.text = outTitle
    }
    
}

// MARK: collection view
extension PicGrid: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let s = (collectionView.frame.width - 4) / 3
        return CGSize(width: s, height: s)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        uiEmpty.isHidden = mCount > 0
        return mCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PicGridCell
        
        // on cell
        Task.main {
            self.delegate?.picGridAsync(for: self.identifier, on: cell, for: indexPath.row)
        }
        
        // desc
        if let desc = delegate?.picGridOnCellDesc?(for: identifier, index: indexPath.row) {
            cell.uiCover.isHidden = false
            cell.uiLbl.text = desc
        } else {
            cell.uiCover.isHidden = true
        }
        
        return cell
    }
    
}

// MARK: delegate
@objc protocol PicGridDelegate {
    
    func picGridCountAsync(for identifier: String?) -> Int
    func picGridAsync(for identifier: String?, on cell: PicGridCell, for index: Int)
    @objc optional func picGridOnCellDesc(for identifier: String?, index: Int) -> String?
    
}

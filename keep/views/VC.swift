import UIKit
import NotificationBanner

import FontAwesome


class VC: UIViewController {
    
    private var mAlertBanner: NotificationBanner!
    
    // static
    static func resolveTopVC(_ vc: UIViewController) -> UIViewController {
        if let nc = vc as? UINavigationController {
            return nc.topViewController!
        }
        return vc
    }
    
    
    // methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headIt(item: navigationItem)
    }
    
    func headIt(item: UINavigationItem?) {
        item?.titleView = FragHeader.f(owner: self)
    }
    
    private func alertIt(title: String, msg: String, style: BannerStyle, hangInSeconds: TimeInterval=5.0, onTap: (()->Void)?=nil)
    {
        let imger: (FontAwesome, CGFloat) -> UIImageView = { fa, border in
            return UIImageView(image:
                UIImage.fontAwesomeIcon(name: fa, style: .solid, textColor: Cl.white3, size: CGSize(width: 36, height: 36))
            )
        }
        
        var leftIcon: FontAwesome!
        switch style {
        case .danger: leftIcon = .exclamationTriangle
        case .success: leftIcon = .check
        case .warning: leftIcon = .exclamationTriangle
        default: leftIcon = .infoCircle
        }
        
        var rightView: UIImageView? = nil
        if onTap != nil {
            rightView = imger(.redo, 20)
        }
        
        let banner = NotificationBanner(title: title, subtitle: msg, leftView: imger(leftIcon, 20), rightView: rightView, style: style)
        banner.dismissOnSwipeUp = true
        banner.dismissOnTap = true
        banner.duration = hangInSeconds
        banner.onTap = {
            banner.dismiss()
            onTap?()
        }
        
        banner.show()
        mAlertBanner = banner
    }
    
    func successIt(msg: String, title: String="Maravilha!", onTap: (()->Void)?=nil) {
        alertIt(title: title, msg: msg, style: .success, onTap: onTap)
    }
    
    func errorIt(msg: String?, title: String="Erro!", onTap: (()->Void)?=nil) {
        if let err = msg {
            alertIt(title: title, msg: err, style: .danger, onTap: onTap)
        } else {
            mAlertBanner?.dismiss()
        }
    }
    
    func warnIt(msg: String, title: String="Atenção!", onTap: (()->Void)?=nil) {
        alertIt(title: title, msg: msg, style: .warning, onTap: onTap)
    }

    @IBAction func showMenuLeft() {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func closeMenuLeft() {
        slideMenuController()?.closeLeft()
    }
    
}


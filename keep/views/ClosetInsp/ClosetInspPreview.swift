import UIKit

class ClosetInspPreview: UIViewController {
    
    @IBOutlet weak var uiImg: UIImageView!
    @IBOutlet weak var uiCategory: UILabel!
    @IBOutlet weak var uiDesc: UILabel!
    
    private var mModel: Inspiration!
    
    func outSet(model: Inspiration) {
        self.mModel = model
    }
    
    func getModel() -> Inspiration {
        return self.mModel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mModel.p.img { self.uiImg.image = $0 }
        uiCategory.text = mModel.p.category
        uiDesc.text = mModel.p.desc
    }
    
}

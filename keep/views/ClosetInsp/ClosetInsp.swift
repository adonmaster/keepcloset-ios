import UIKit
import Photos

class ClosetInsp: UIViewController {
    
    // ui
    @IBOutlet weak var uiCv: UICollectionView!
    @IBOutlet weak var uiEmpty: UIView!
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    @IBOutlet weak var uiFilter: UIButton!
    
    @IBOutlet weak var uiSelectionContainerTop: NSLayoutConstraint!
    @IBOutlet weak var uiSelectionContainer: UIView!
    @IBOutlet weak var uiSelectionInfo: UILabel!
    
    // state
    private var mLoading = false {
        didSet {
            if mLoading {
                uiLoading?.startAnimating()
            } else {
                uiLoading?.stopAnimating()
            }
        }
    }
    private var mUser: User!
    private var mData: [(String, [Inspiration])] = []
    private var mFilter: String? {
        didSet {
            updateData()
            updateFilterDesktop()
        }
    }
    private var mIsModeSelection = false {
        didSet {
            if self.uiSelectionContainer==nil { return }
            
            self.uiSelectionContainerTop.constant = self.mIsModeSelection==true
                ? 0
                : -self.uiSelectionContainer.frame.height
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            
            self.mSelection = [:]
        }
    }
    
    private var mSelection = [Int: [Int]]() {
        didSet {
            var cont = 0
            mSelection.forEach {
                $0.value.forEach { _ in cont += 1 }
            }
            
            uiSelectionInfo?.text = Str.plural(
                cont, "Sem itens selecionados".l, "1 item selecionado".l, "$0 itens selecionados".l
            )
            uiSelectionInfo?.textColor = cont > 0 ? UIColor.systemBlue : UIColor.lightGray
            
            // build indexes to reload
            var indexesToReload = [IndexPath]()
            let mapper: (Dictionary<Int,[Int]>.Element)->() = { pack in
                indexesToReload.append(
                    contentsOf: pack.value.map { row in IndexPath(row: row, section: pack.key) }
                )
            }
            self.mSelection.forEach(mapper)
            oldValue.forEach(mapper)
            
            // reload
            uiCv?.withValidIndexes(indexesToReload, cb: { $0.reloadItems(at: $1) })
        }
    }

    private var mPendingUrls = [URL]()
    
    private let keyEmptyCategory = "* Sem categoria".l
    
    
    //
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        uiFilter.titleLabel?.textAlignment = .center
        
        updateFilterDesktop()
        
        mUser = Repo.user.active()!
        
        updateData()
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(notificationOnApplicationDidBecomeActive(_:)),
            name: .exApplicationDidBecomeActive, object: nil
        )
        
        mSelection = [:]
        uiSelectionContainerTop.constant = -self.uiSelectionContainer.frame.height
    }
    
    @objc func notificationOnApplicationDidBecomeActive(_ sender: Any) {
        processPending()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(onSearch(_:))),
            UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(onSelectModeToggle(_:)))
        ]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        processPending()
    }
    
    private func processPending() {
        // load pending urls
        let key = SharedPref.key_shared_pending_inpirations
        mPendingUrls = Arr(SharedPref.shared.get(key: key)).distinct { $0.absoluteString }
        
        // call for pending share import
        let pendingCount = mPendingUrls.count
        if pendingCount > 0 && !ShareImportVC.isShowing {
            performSegue(withIdentifier: "sgShareImport", sender: nil)
        }
    }
    
    private func updateData()
    {
        Task.main {
            let data = Repo.inspiration.query(for: self.mUser, q: self.mFilter)
            self.mData = Arr(data).categorize({ $0.category ?? "Sem categoria".l }, sorted: true)
            self.uiCv.reloadData()
        }
    }
    
    func updateFilterDesktop()
    {
        var msg = ""
        var suffix = ""
        if let s = mFilter {
            msg = "filtro para: \"".l+"\(s)\", "
            suffix = "\n(Aperte para remover filtro)".l
        }
        
        var mDataCount = 0
        mData.forEach {
            $0.1.forEach { _ in mDataCount += 1 }
        }
        
        msg += Str.plural(mDataCount, "sem resultados".l, "um resultado".l, { "\($0) "+"resultados".l })
        
        uiFilter?.setTitle(msg + suffix, for: .normal)
    }
    
    @IBAction func onAdd(_ sender: Any) {
        Dialog(sender: self)
            .stylesheet(popoverSourceView: sender as? UIView)
            .action(key: "Adicionar único".l, cb: { _ in
                Task.main {
                    self.performSegue(withIdentifier: "sgCreate", sender: nil)
                }
            })
            .action(key: "Adicionar múltiplo para categoria".l, cb: { _ in
                Task.main(delayInSeconds: 0.2, block: {
                    self.addMultiple()
                })
            })
            .cancel()
            .present()
    }
    
    @IBAction func onSelectionDelete(_ sender: Any)
    {
        if !mIsModeSelection || mSelection.count == 0 { return }
        
        Dialog(sender: self)
            .destructive("Apagar registros".l).cancel()
            .msg("Tem certeza que deseja apagar os registros selecionados?".l)
            .present(onDestructive: {
                
                self.uiCv.performBatchUpdates {
                    
                    for section in Array(self.mSelection.keys).sorted().reversed() {
                        let rows = Array(self.mSelection[section]!).sorted().reversed()
                        if self.mSelection[section]?.count == self.mData[section].1.count {
                            
                            // delete db in a bulk
                            self.mData[section].1.forEach(Repo.inspiration.remove)
                            
                            // delete locally && ui
                            self.mData.remove(at: section)
                            self.uiCv.deleteSections([section])
                            
                        } else {
                            for row in rows {
                                let m = self.mData[section].1[row]
                                
                                // remove from db
                                Repo.inspiration.remove(model: m)
                                
                                // delete local && ui
                                self.mData[section].1.remove(at: row)
                                self.uiCv.deleteItems(at: [IndexPath(row: row, section: section)])
                            }
                        }
                    }
                    
                } completion: { _ in
                    self.mSelection = [:]
                }
                
            }) // present
    }
    
    @IBAction func onSelectionCancel() {
        mIsModeSelection = false
    }
    
    
    private func addMultiple() {
        performSegue(withIdentifier: "sgPickCategory", sender: nil)
    }
    
    private func save(category: String?, img: UIImage, thumb: UIImage)
    {
        _ = Repo.inspiration.save(
            model: nil,
            user: mUser,
            category: category,
            color1: nil,
            color2: nil,
            img: img,
            thumb: thumb,
            desc: nil
        )
    }
    
    @objc func onSelectModeToggle(_ btn: Any) {
        mIsModeSelection = !mIsModeSelection
    }
    
    @objc func onSearch(_ btn: Any) {
        performSegue(withIdentifier: "sgModalSearch", sender: nil)
    }
    
    @IBAction func onFilterTap(_ sender: Any) {
        mFilter = nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // create
        if let vc = segue.destination as? ClosetInspEdit {
            if segue.identifier == "sgEdit", let i = uiCv.indexPathsForSelectedItems?.first {
                vc.outModel = mData[i.section].1[i.row]
            } else {
                vc.outModel = nil
            }
        }
        
        // slider
        else if let vc = segue.destination as? ClosetInspSlider, let i = uiCv.indexPathsForSelectedItems?.first {
            let models: [Inspiration] = Arr(mData).uncategorize()
            let index = models.firstIndex(of: mData[i.section].1[i.row])!
            vc.outSet(models: models, index: index)
        }
        
        // multiplo categoria picker
        else if let vc = segue.destination as? StrPicker, segue.identifier == "sgPickCategory" {
            vc.identifier = "category"
            vc.delegate = self
        }
    
        // search
        else if let dest = segue.destination as? ModalSearch {
            dest.outValue = mFilter
            dest.outPh = "Insira texto...".l
            dest.delegate = self
        }
        
        // share import
        else if let dest = segue.destination as? ShareImportVC {
            dest.delegate = self
        }
    }
    
    @IBAction func onBackToClosetInsp(_ segue: UIStoryboardSegue) {
        updateData()
    }
    
}

// MARK: collection view

extension ClosetInsp: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        updateFilterDesktop()
        
        let w = (collectionView.frame.width - 4) / 3
        return CGSize(width: w, height: w)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let n = mData.count
        uiEmpty?.isHidden = n > 0
        return n
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mData[section].1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ClosetInspCell
        let m = mData[indexPath.section].1[indexPath.row]
        
        cell.backgroundColor = Cl.mc.grey200Color()
        m.p.thumb { cell.uiImg.image = $0 }
        
        let isSelected = mSelection[indexPath.section]?.contains(indexPath.row) == true
        cell.uiSelected.isVisible = isSelected && mIsModeSelection
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if mIsModeSelection {
            
            // toggle selection
            let section = indexPath.section
            let row = indexPath.row
            if let index = mSelection[section]?.firstIndex(of: row) {
                mSelection[section]!.remove(at: index)
            } else {
                if mSelection[section] == nil { mSelection[section] = [] }
                mSelection[section]!.append(row)
            }
            
        } else {
            performSegue(withIdentifier: "sgSlider", sender: nil)
        }
    }
    
    @IBAction func collectionViewLongClick(_ g: UILongPressGestureRecognizer)
    {
        if mIsModeSelection { return }
        
        guard g.state == .began else { return }
        let point = g.location(in: uiCv)
        guard let i = uiCv.indexPathForItem(at: point) else { return }
        
        Dialog(sender: self)
            .destructive("Apagar registro".l).cancel()
            .title("Atenção".l).msg("Tem certeza que deseja apagar este registro?".l)
            .present(onOk: nil, onCancel: nil, onDestructive: {
                
                Task.main(delayInSeconds: 0.2) {
                    
                    // persist
                    Repo.inspiration.remove(model: self.mData[i.section].1[i.row])
                    
                    // local
                    self.mData[i.section].1.remove(at: i.row)
                    
                    // ui
                    let isSectionEmpty = self.mData[i.section].1.count == 0
                    if isSectionEmpty {
                        self.mData.remove(at: i.section)
                        self.uiCv.deleteSections(IndexSet(integer: i.section))
                    } else {
                        self.uiCv.deleteItems(at: [i])
                    }
                    
                }
                
            }, nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        let identifier = kind == UICollectionView.elementKindSectionFooter ? "footer" : "header"
        if let cell = collectionView
            .dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: identifier, for: indexPath)
            as? ClosetInspHeader
        {
            let m = mData[indexPath.section]
            
            cell.uiTitle.text = m.0.trim().uppercased()
            
            return cell;
        }

        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: identifier, for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
    {
        return CGSize(width: 0, height: section==mData.count-1 ? 60 : 0)
    }
    
    
}


// MARK: category picker
extension ClosetInsp: StrPickerDelegate {
    
    func strPickerOnRetrieveData(for identifier: String?) -> [String] {
        if identifier == "category" {
            return [keyEmptyCategory] + Repo.inspiration
                .listCategories(for: mUser)
                .map({ $0.uppercased() })
                .sorted()
        }
        return []
    }
    
    func strPicker(for identifier: String?, onSelect s: String) {
        if identifier == "category" {
            Media.presentPickerMultiple(identifier: s, caller: self, type: .image, singleSelect: false)
        }
    }
    
    func strPickerIsEditMode(for identifier: String?) -> Bool {
        return true
    }
}

// MARK: media picker multiple with category as identifier
extension ClosetInsp: MediaDelegate {
    
    func media(for identifier: String?, onSelect assets: [PHAsset]) {
        
        if assets.count == 0 { return }
        
        Task.main(delayInSeconds: 0.2) { [weak self] in
            
            self?.mLoading = true
            let semaph = Semaph { [weak self] in
                self?.mLoading = false
                self?.updateData()
            }
            
            for asset in assets {
                semaph.inc()
                
                Media.request(asset, size: .height(300)) { [weak self] img in
                    let thumb = Img(img).limit(.width(150), isOpaque: true, factorScale: 1.0).get()
                    self?.save(category: (identifier == self?.keyEmptyCategory ? nil : identifier), img: img, thumb: thumb)
                    semaph.dec()
                }
            }
        }
        
    }
    
}

// MARK: modal search
extension ClosetInsp: ModalSearchDelegate {
    
    func modalSearch(onQuery text: String?) {
        mFilter = text
    }
    
}

// MARK: share import delegate
extension ClosetInsp: ShareImportDelegate {

    func shareImportRetrieveUrls() -> [URL] {
        return mPendingUrls
    }
    
    func shareImportRetrieveCategories() -> [String] {
        return Repo.inspiration.listCategories(for: mUser)
            .map({ $0.uppercased() })
            .sorted()
    }
    
    func shareImportOnDone(urls: [URL], category: String?) {
        mLoading = true
        Task.main {
            urls.forEach { url in
                if !FileManager.default.fileExists(atPath: url.path) { return }
                
                // prepare images
                let img = UIImage(contentsOfFile: url.path)!
                let full = Img(img).limit(.height(1600), isOpaque: false, factorScale: 1).get()
                let thumb = Img(img).limit(.height(240), isOpaque: false, factorScale: 1).get()

                // save in db
                Repo.inspiration.save(
                    model: nil, user: self.mUser, category: category, color1: nil, color2: nil,
                    img: full, thumb: thumb, desc: nil
                )
            }
            
            self.shareImportCleanup(urls: urls)
            
            self.updateData()
            
            self.mLoading = false
        }
        
    }
    
    func shareImportCleanup(urls: [URL]) {
        // delete files
        urls.forEach { try? FileManager.default.removeItem(at: $0) }
        
        // remove urls
        SharedPref.shared.put(key: SharedPref.key_shared_pending_inpirations, list: [])
    }
    
}

import UIKit

class ClosetInspSlider: UIPageViewController {
    
    private var mData: [Inspiration] = []
    private var mIndex: Int = 0
    
    
    func outSet(models: [Inspiration], index: Int) {
        mData = models
        mIndex = index
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // closet pic nav item
        navigationItem.titleView = FragHeader.f(owner: self)
        
        delegate = self
        dataSource = self
        
        setViewControllers([newvc(mIndex)], direction: .forward, animated: false) { _ in
            // done
        }
    }
    
    func newvc(_ i: Int) -> ClosetInspPreview {
        let vc = storyboard!.instantiateViewController(withIdentifier: "preview") as! ClosetInspPreview
        vc.outSet(model: mData[i])
        return vc
    }
    
    func curvc() -> ClosetInspPreview? {
        return viewControllers?.first as? ClosetInspPreview
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // edit
        if let vc = segue.destination as? ClosetInspEdit {
            vc.outModel = mData[mIndex]
        }
    }
    
}

extension ClosetInspSlider: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        if !completed { return }
        
        if let cur = curvc(), let i = mData.firstIndex(of: cur.getModel()) {
            mIndex = i
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex-1
        if i < 0 || mData.count == 0 { return nil }
        
        return newvc(i)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex+1
        if i > mData.count-1 { return nil }
        
        return newvc(i)
    }

}

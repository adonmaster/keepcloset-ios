import UIKit

class ClosetInspEdit: UIViewController {
    
    // ui
    @IBOutlet weak var uiImg: UIImageView!
    @IBOutlet weak var uiEmpty: UIStackView!
    @IBOutlet weak var uiCategory: UIButton!
//    @IBOutlet weak var uiColors: UIButton!
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    @IBOutlet weak var uiDesc: UITextView!
    
    // public
    var outModel: Inspiration?
    
    // state
    private var mLoading = false {
        didSet {
            if mLoading {
                uiLoading?.startAnimating()
            } else {
                uiLoading?.stopAnimating()
            }
        }
    }
    
    private var mUser: User!
    
    private var mImg: UIImage? = nil {
        didSet {
            self.updateDesktopImg()
        }
    }
    
    private var mCategory: String? {
        didSet {
            self.updateDesktopCategory()
        }
    }
    
    private var mColors: [UIColor] = [] {
        didSet {
            self.updateDesktopColors()
        }
    }
    
    
    // methods
    
    override func viewDidLoad() {
        mUser = Repo.user.active()!
        
        if let m = outModel {
            mCategory = m.category
            mColors = m.p.colors
            m.p.img { img in self.mImg = img }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateDesktopImg()
        updateDesktopCategory()
        updateDesktopColors()
        uiDesc.text = outModel?.desc ?? "-"
    }
    
    private func updateDesktopImg() {
        uiEmpty?.isHidden = mImg != nil
        uiImg?.image = mImg
    }
    
    private func updateDesktopCategory() {
        uiCategory?.setTitle(mCategory ?? "Sem categoria".l, for: .normal)
    }
    
    private func updateDesktopColors()
    {
        let s = Span("")
        let square = "  "
        
        if let cl = mColors.first {
            _ = s.append(square).color(cl)
        }
        
        if let cl = Arr(mColors).second {
            _ = s.append(square).color(cl)
        }
        
        // uiColors.setAttributedTitle(s.get(), for: .normal)
    }
    
    @IBAction func onTapImage(_ gesture: UITapGestureRecognizer) {
        Dialog(sender: self)
            .stylesheet(popoverSourceView: gesture.view)
            .action(key: "Album", cb: { _ in
                Media.presentPicker(caller: self, .photoLibrary, delegate: self)
            })
            .action(key: "Camera", cb: { _ in
                Media.presentPicker(caller: self, .camera, delegate: self)
            })
            .cancel()
            .present()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? StrPicker {
            vc.delegate = self
        } else if let vc = segue.destination as? ColorPicker {
            vc.delegate = self
            vc.outLimitColors = 2
        }
    }
    
    private func checkErrors() -> String? {
        if mImg==nil {
            return "Escolha uma imagem antes de salvar!".l
        }
        return nil
    }
    
    @IBAction func onSave(_ any: Any)
    {
        if let err = checkErrors() {
            Dialog(sender: self)
                .title("Ops!").msg(err)
                .ok()
                .present()
            return
        }
        
        mLoading = true
        Task.main {
            let img = self.mImg!
            let thumb = Img(img).limit(.width(120), isOpaque: true, factorScale: 1).get()
            _ = Repo.inspiration.save(
                model: self.outModel, user: self.mUser, category: self.mCategory, color1: self.mColors.first,
                color2: Arr(self.mColors).second, img: img, thumb: thumb, desc: self.uiDesc.text
            )
            
            self.performSegue(withIdentifier: "backToClosetInsp", sender: nil)
            self.mLoading = false
        }
    }
    
}


// MARK: Image picker delegates
extension ClosetInspEdit: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let img = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)]
            as? UIImage
        {
            mImg = img
        }
        
        picker.dismiss(animated: true)
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }
}

// MARK: category picker
extension ClosetInspEdit: StrPickerDelegate {

    func strPickerOnRetrieveData(for indentifier: String?) -> [String] {
        return Repo.inspiration.listCategories(for: mUser)
            .map({ $0.uppercased() })
            .sorted()
    }
    
    func strPicker(for identifier: String?, onSelect s: String) {
        mCategory = s
    }
    
}

// MARK: color picker
extension ClosetInspEdit: ColorPickerDelegate {

    func colorPicker(didSelect selection: [UIColor], identifier: Int?) {
        var r: [UIColor] = []
        if let cl1 = selection.first {
             r.append(cl1)
        }
        if let cl2 = Arr(selection).second {
            r.append(cl2)
        }
        mColors = r
    }
    
}

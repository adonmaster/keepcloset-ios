import UIKit


class AvatarPickerVC: UIViewController {
    
    @IBOutlet weak var scrollViewImage: ImageScrollView!
    @IBOutlet weak var btnCamera: UIBarButtonItem!
    @IBOutlet weak var btnAlbum: UIBarButtonItem!
    @IBOutlet weak var viewTarget: UIView!
    @IBOutlet weak var progress_bar: UIActivityIndicatorView!
    
    var delegate: AvatarPickerDelegate?
    
    private var stLoading = false {
        didSet {
            if (stLoading) {
                progress_bar?.startAnimating()
            } else {
                progress_bar?.stopAnimating()
            }
        }
    }
    
    @IBAction func onCancelClicked(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func onCameraClicked(_ sender: Any) {
        Media.presentPicker(caller: self, .camera, delegate: self)
    }
    
    @IBAction func onAlbumClicked(_ sender: Any) {
        Media.presentPicker(caller: self, .photoLibrary, delegate: self)
    }
    
    func displayImage(_ img: UIImage) {
        stLoading = true
        
        DispatchQueue.main.async {
            // limit size
            let resizedImg = Img(img).limit(.width(800.0)).get();
            
            let bknd = Img(resizedImg)
                .blur(radius: 40)
                .scale(factor: 1.5)
                .get();
            
            let final = Img.merge(big: bknd, small: resizedImg)
            
            self.scrollViewImage.display(image: final)
            
            self.stLoading = false
        }
        
    }
    
    @IBAction func onSaveClicked(_ sender: Any) {
        guard scrollViewImage.imageDisplayed() != nil else { return }
        
        stLoading = true
        DispatchQueue.main.async {
            
            // hide a few things before screenshot
            let bkupAlpha = self.viewTarget.alpha;
            self.viewTarget.alpha = 0
            self.progress_bar.isHidden = true
            
            // full screenshot
            UIGraphicsBeginImageContext(self.view.frame.size)
            self.view.layer.render(in: UIGraphicsGetCurrentContext()!)
            let full = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            // show a few things after screenshot
            self.viewTarget.alpha = bkupAlpha
            self.progress_bar.isHidden = false
            
            // crop the damn thing
            UIGraphicsBeginImageContext(self.viewTarget.frame.size)
            let y: CGFloat = self.d((self.view.frame.size.height - self.viewTarget.frame.size.height), 2)
            let x: CGFloat = self.d((self.view.frame.size.width - self.viewTarget.frame.size.width), 2)
            full.draw(at: CGPoint(x: -x, y: -y))
            let img = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            self.delegate?.avatarPicked(didChooseImage: img)
            
            self.dismiss(animated: true)
        }
    }
    
    func d(_ divisor: CGFloat, _ quocient: CGFloat) -> CGFloat {
        guard divisor != 0 else { return 0 }
        return divisor/quocient
    }
}

extension AvatarPickerVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let img = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.displayImage(img)
        }
        picker.dismiss(animated: true)
    }
    
}

protocol AvatarPickerDelegate {
    
    func avatarPicked(didChooseImage img: UIImage)
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

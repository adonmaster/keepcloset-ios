import UIKit

class ClosetMontageSlider: UIPageViewController {
    
    private var mData: [Look] = []
    private var mIndex = 0
    
    func outLoad(models: [Look], index: Int) {
        mData = models
        mIndex = index
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // closet pic nav item
        navigationItem.titleView = FragHeader.f(owner: self)
        
        delegate = self
        dataSource = self
        
        setViewControllers([newvc(mIndex)], direction: .forward, animated: false) { _ in
            // done
        }
    }
    
    @IBAction func onShare(_ b: Any)
    {
        mData[mIndex].p.loadFull { img in
            
            // set up activity view controller
            let imageToShare = [ img ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ClosetMontageStudio {
            vc.outLoad(model: mData[mIndex])
        }
    }
    
}

// MARK: page
extension ClosetMontageSlider: UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    private func newvc(_ i: Int) -> ClosetMontagePreview {
        let vc = storyboard!.instantiateViewController(withIdentifier: "preview") as! ClosetMontagePreview
        vc.outSet(model: mData[i])
        return vc
    }
    
    private func curvc() -> ClosetMontagePreview? {
        return viewControllers?.first as? ClosetMontagePreview
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        if !completed { return }
        
        if let cur = curvc(), let i = mData.firstIndex(of: cur.outGetModel()) {
            mIndex = i
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex-1
        if i < 0 || mData.count == 0 { return nil }
        
        return newvc(i)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let i = mIndex+1
        if i > mData.count-1 { return nil }
        
        return newvc(i)
    }
    
}

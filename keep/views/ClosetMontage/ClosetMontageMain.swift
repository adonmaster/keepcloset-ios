import UIKit

class ClosetMontageMain: VC {
    
    @IBOutlet weak var uiCv: UICollectionView!
    @IBOutlet weak var uiFilter: UIButton!
    
    private var mUser: User!
    private var mData: [(String, [Look])] = []
    private var mFilter: String? {
        didSet {
            updateData()
            updateFilterDesktop()
        }
    }
    
    private var sCollectionViewItemSelected: IndexPath?
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        uiFilter.titleLabel?.textAlignment = .center
        
        mUser = Repo.user.active()!
        
        updateData()
        updateFilterDesktop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(onSearch(_:)))
        ]
    }
    
    private func updateData()
    {
        let data = Repo.look.query(for: mUser, q: mFilter)
        mData = Arr(data).categorize({ $0.category ?? "Sem categoria".l }, sorted: true)
        uiCv?.reloadData()
    }
    
    func updateFilterDesktop()
    {
        var msg = ""
        var suffix = ""
        if let s = mFilter {
            msg = "filtro para: \"".l+"\(s)\", "
            suffix = "\n(Aperte para remover filtro)".l
        }
        msg += Str.plural(mData.count, "sem resultados".l, "um resultado".l, { "\($0) "+"resultados".l })
        
        uiFilter?.setTitle(msg + suffix, for: .normal)
    }
    
    @objc func onSearch(_ btn: Any) {
        performSegue(withIdentifier: "sgModalSearch", sender: nil)
    }
    
    @IBAction func onFilterTap(_ sender: Any) {
        mFilter = nil
    }
    
    @IBAction func backToClosetMontageMain(_ segue: UIStoryboardSegue)
    {
        updateData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? ClosetMontageStudio {
            if segue.identifier == "sgNewLook" {
                vc.outLoad(model: nil)
            }
        }
        
        // slider
        else if let vc = segue.destination as? ClosetMontageSlider, let cell = sender as? UICollectionViewCell,
            let i = uiCv.indexPath(for: cell)
        {
            let models: [Look] = Arr(mData).uncategorize()
            let model = mData[i.section].1[i.row]
            let i = models.firstIndex(of: model)!
            
            vc.outLoad(models: models, index: i)
        }
        
        // search
        else if let dest = segue.destination as? ModalSearch {
            dest.outValue = mFilter
            dest.outPh = "Insira texto...".l
            dest.delegate = self
        }
    }
    
}

// MARK: collection
extension ClosetMontageMain: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        updateFilterDesktop()
        
        let w = (collectionView.frame.width - 4)/3
        return CGSize(width: w, height: w)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return mData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mData[section].1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ClosetMontageCell
        let model = mData[indexPath.section].1[indexPath.row]

        cell.uiImg.image = model.thumb == nil
            ? #imageLiteral(resourceName: "404")
            : UIImage(data: model.thumb!)
        cell.lblDesc.text = Carbon(model.updated_at).diffForHuman()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        // header
        if kind == UICollectionView.elementKindSectionHeader {
            let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! ClosetMontageCellHeader
            
            cell.lblDesc.text = mData[indexPath.section].0.uppercased()
            
            return cell
        }
        
        // footer
        else {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footer", for: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
    {
        let s = section >= mData.count-1 ? 60 : 0
        
        return CGSize(width: s, height: s)
    }
    

    @IBAction func collectionViewLongClick(gesture: UILongPressGestureRecognizer)
    {
        guard gesture.state == .began else { return }
        let point = gesture.location(in: uiCv)
        guard let i = uiCv.indexPathForItem(at: point) else { return }
        
        sCollectionViewItemSelected = i
        Dialog(sender: self)
            .title("Atenção".l)
            .msg("Escolha uma das opções:".l)
            .destructive("Apagar registro".l).cancel()
            .present(onOk: nil, onCancel: {
                self.sCollectionViewItemSelected = nil
            }, onDestructive: {
                self.collectionView(delete: i)
            })
    }
    
    func collectionView(delete i: IndexPath)
    {
        let model = mData[i.section].1[i.row]
        
        // db
        Repo.look.remove(model)
        
        // remove locally
        mData[i.section].1.remove(at: i.row)
        if mData[i.section].1.count == 0 {
            mData.remove(at: i.section)
            
            uiCv.reloadData()
        } else {
            uiCv.deleteItems(at: [i])
        }
        
        // selected
        self.sCollectionViewItemSelected = nil
    }
    
    
}

// MARK: modal search
extension ClosetMontageMain: ModalSearchDelegate {
    
    func modalSearch(onQuery text: String?) {
        mFilter = text
    }
    
}

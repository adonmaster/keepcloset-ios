import UIKit

class ClosetMontagePreview: UIViewController {
    
    // ui
    @IBOutlet weak var uiImg: UIImageView!
    
    // state
    private var mModel: Look!
    
    func outSet(model: Look) {
        mModel = model
    }
    
    func outGetModel() -> Look {
        return mModel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mModel.p.loadFull { self.uiImg.image = $0 }
    }
    
}

import UIKit

class ClosetMontageStudio: UIViewController {

    // ui
    @IBOutlet weak var ctCategoryPickerLeft: NSLayoutConstraint!
    @IBOutlet weak var uiSegment: UISegmentedControl!
    @IBOutlet weak var btnCategoryPickerToggle: UIButton!
    @IBOutlet weak var uiWrapper: UIView!
    @IBOutlet weak var uiImgBknd: UIImageView!
    @IBOutlet weak var uiBtnCategory: UIButton!
    @IBOutlet weak var uiLoading: UIActivityIndicatorView!
    
    private let imgBknds: [UIImage] = [
        UIImage(named: "bknd-mockup-white")!,
        UIImage(named: "bknd-mockup-black")!,
        UIImage(named: "bknd-mockup-empty-white")!,
        UIImage(named: "bknd-mockup-empty-black")!,
    ]

    private var mImgs: [ImageViewEx] = []
    private weak var mImgSelected: ImageViewEx?
    private var mLoading = false {
        didSet {
            if mLoading {
                uiLoading?.startAnimating()
                uiWrapper.alpha = 0
                uiBtnCategory.isHidden = true
            } else {
                uiLoading?.stopAnimating()
                UIView.animate(withDuration: 0.3) {
                    self.uiWrapper?.alpha = 1
                }
                uiBtnCategory.isHidden = false
            }
        }
    }

    // states
    
    private var mCategoryPickerToggle = false {
        didSet {
            self.updateItemPicker()
        }
    }
    
    // models
    private lazy var mUser: User = {
        return Repo.user.active()!
    }()
    
    private var mModel: Look?
    private var sCategory: String?
    private var sItems: [LookItem] = []
    private var sBknd: Int16 = 0

    
    // methods
    func outLoad(model: Look?)
    {
        mModel = model
        sCategory = model?.category
        sBknd = model?.bknd ?? 0
        
        if model != nil {
            self.sItems = Repo.look.getItemsfor(parent: model!)
        } else {
            self.sItems = []
        }
    }
    
    private func updateDesktopCategory() {
        uiBtnCategory.setTitle(sCategory ?? "Aperte para escolher categoria....".l, for: .normal)
    }
    
    private func updateDesktopItems()
    {
        mLoading = true
        Task.main {
            
            for item in self.sItems
            {
                let img = self.addImgToDesktop(UIImage(data: item.img!)!, item: item.statItem, rescale: false)
                
                img.center.x = CGFloat(item.center_x)
                img.center.y = CGFloat(item.center_y)
                
                img.transform.a = CGFloat(item.transform_a)
                img.transform.b = CGFloat(item.transform_b)
                img.transform.c = CGFloat(item.transform_c)
                img.transform.d = CGFloat(item.transform_d)
                img.transform.tx = CGFloat(item.transform_tx)
                img.transform.ty = CGFloat(item.transform_ty)
            }
            
            self.mLoading = false
        }
    }
    
    private func updateDesktopBackground() {
        uiSegment.selectedSegmentIndex = Int(sBknd)
        updateSegment()
    }
    
    
    func updateItemPicker()
    {
        let w: CGFloat = self.view?.frame.width ?? 340
        let buttonW = self.btnCategoryPickerToggle?.frame.width ?? 20
        let b = mCategoryPickerToggle
        
        btnCategoryPickerToggle.setTitle(
            String.fontAwesomeIcon(name: b ? .chevronRight : .chevronLeft), for: .normal
        )
        
        self.ctCategoryPickerLeft?.constant = b ? 0 : w - buttonW
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view?.layoutIfNeeded()
        }
    }
    
    // ui methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateSegment()
        mImgs = []
        
        doEditImage(v: nil, false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateItemPicker()
        
        updateDesktopCategory()
        updateDesktopBackground()
        updateDesktopItems()
        
        ImageViewEx.register(mImgs)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ImageViewEx.unregister(mImgs)
    }
    
    @IBAction func onCategoryPickerToggle(_ sender: Any) {
        mCategoryPickerToggle = !mCategoryPickerToggle
    }
    
    private func updateSegment() {
        uiImgBknd.image = imgBknds[uiSegment.selectedSegmentIndex]
    }
    
    @IBAction func onSegment(_ sender: Any) {
        sBknd = Int16(uiSegment.selectedSegmentIndex)
        updateSegment()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let destination = segue.destination as? FragItemPicker {
            destination.delegate = self
        } else if let destination = segue.destination as? StrPicker {
            destination.delegate = self
        }
    }
    
    @IBAction func onSave(_ sender: Any)
    {
        // halt if it is loading
        if mLoading {
            Dialog(sender: self)
                .ok()
                .title("Opa!".l)
                .msg("Aguarde processamento...".l)
                .present()
            return
        }
        
        // unselect
        ImageViewEx.unselectAll()
        
        // prepare to remove background & hide img
        let scaleFactor: CGFloat = 1.5
        let bkupColor = self.uiWrapper.backgroundColor
        self.uiWrapper.isOpaque = false
        self.uiWrapper.backgroundColor = UIColor.clear
        let imgBoundaries = Math.scale(rect: self.uiImgBknd.frame, factor: scaleFactor)
        self.uiImgBknd.isHidden = true
        
        // screenshot view, with uiimg boundaries
        let full = Img(v: self.uiWrapper, factorScale: scaleFactor)
            .subimage(imgBoundaries)
            .trim()
            .get()
        
        // restore bkup & show hidden img
        self.uiImgBknd.isHidden = false
        self.uiWrapper.backgroundColor = bkupColor
        
        // persist
        mLoading = true
        Task.main {
            
            // craete thumb
            let thumb = Img(full).limit(.width(240), isOpaque: false, factorScale: 1.0).get()
            self.uiImgBknd.image = thumb
            
            // persist
            _ = Repo.look.saveFrom(
                modelLook: self.mModel,
                user: self.mUser,
                category: self.sCategory,
                imgs: self.mImgs,
                bknd: Int16(self.uiSegment!.selectedSegmentIndex),
                full: full,
                thumb: thumb
            )
            
            // free memory
            self.mImgs = []
            
            self.performSegue(withIdentifier: "unBackToClosetMontageMain", sender: nil)
            
            // no need to show controllers
            // self.mLoading = false
        }
    }
    
    @IBAction func backToClosetMontageStudio(_ u: UIStoryboardSegue) {
        // do something?
    }
    
    @IBAction func onShareImage(_ sender: Any)
    {
        // image to share
        ImageViewEx.unselectAll()
        
        let rect = uiImgBknd.frame
        let scaleFactor = UIScreen.main.scale
        let scaledRect = Math.scale(rect: rect, factor: scaleFactor)
        
        // wrapper transparent bkup
        let bkndWrapperColor = uiWrapper.backgroundColor
        uiWrapper.backgroundColor = UIColor.clear
        
        // generate image
        let image = Img(v: uiWrapper, isOpaque: false, factorScale: scaleFactor)
            .subimage(scaledRect)
            .get()
        
        // wrapper restore
        uiWrapper.backgroundColor = bkndWrapperColor
        
        // set up activity view controller
        let imageToShare = [ image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        Dialog(sender: self)
            .title("Atenção".l)
            .msg("Aparentemente a memória deste aparelho está se esgotando, feche algumas janelas para melhor funcionamento.".l)
            .ok()
            .present()
    }
}

// MARK: frag item picker
extension ClosetMontageStudio: FragItemPickerDelegate, ImageViewExDelegate, UIGestureRecognizerDelegate {

    func addImgToDesktop(_ i: UIImage, item: Item?, rescale: Bool) -> ImageViewEx
    {
        let processedImage = rescale ? Img(i).limit(.width(800), isOpaque: false, factorScale: 1).get() : i
        let img = ImageViewEx(img: processedImage, item: item)
        
        img.selectGroup = 1
        img.delegate = self
        
        self.uiWrapper.addSubview(img)
        ImageViewEx.register(img)
        self.mImgs.append(img)
        
        if rescale {
            img.transform = img.transform.scaledBy(x: 0.4, y: 0.4)
            img.center.x = 100
            img.center.y = 100
        }
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onPan(gesture:)))
        img.addGestureRecognizer(panGesture)
        panGesture.delegate = self
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.onLongPress(gesture:)))
        img.addGestureRecognizer(longGesture)
        longGesture.delegate = self
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.onPinch(gesture:)))
        img.addGestureRecognizer(pinchGesture)
        pinchGesture.delegate = self

        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(self.onRotate(gesture:)))
        img.addGestureRecognizer(rotateGesture)
        rotateGesture.delegate = self
        
        return img
    }
    
    func fragItemPicker(onSelect item: FragItemPicker.SudoItem)
    {
        Task.main { [weak self] in
            if let data = item.full, let img = UIImage(data: data) {
                _ = self?.addImgToDesktop(img, item: item.item, rescale: true)
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func imageViewExOnSelect(_ v: ImageViewEx)
    {
        self.uiWrapper.bringSubviewToFront(v)
        
        // swap imgs positions
        if mImgs.count > 1, let index = mImgs.firstIndex(of: v) {
            mImgs.swapAt(mImgs.count-1, index)
        }
        
        doEditImage(v: v, true)
    }
    
    func imageViewExOnUnselect(_ v: ImageViewEx) {
        doEditImage(v: v, false)
    }
    
    func doEditImage(v: ImageViewEx?, _ option: Bool)
    {
        if option, let vv = v {
            mImgSelected = vv
        } else {
            mImgSelected = nil
        }
    }
    
}

// MARK: Gesture
extension ClosetMontageStudio {

    @objc func onPan(gesture: UIPanGestureRecognizer)
    {
        guard let v = gesture.view as? ImageViewEx, v.isSelected else { return }
        
        let translation = gesture.translation(in: v.superview)
        v.center = CGPoint(x: v.center.x + translation.x, y: v.center.y + translation.y)
        gesture.setTranslation(CGPoint.zero, in: v.superview)
    }
    
    @objc func onLongPress(gesture: UILongPressGestureRecognizer)
    {
        guard let v = gesture.view as? ImageViewEx, !v.isSelected else { return }
        
        if let index = mImgs.firstIndex(of: v) {
            
            self.mImgs.remove(at: index)
            ImageViewEx.unregister(v)
            
            UIView.animate(withDuration: 0.3, animations: {
                v.alpha = 0
                v.transform = v.transform.scaledBy(x: 0.01, y: 0.01)
            }) { _ in
                v.removeFromSuperview()
            }
            
            if mImgSelected == v {
                doEditImage(v: nil, false)
            }
        }
    }
    
    @objc func onPinch(gesture: UIPinchGestureRecognizer) {
        guard let vv = gesture.view as? ImageViewEx, vv.isSelected else { return }
        vv.transform = vv.transform.scaledBy(x: gesture.scale, y: gesture.scale)
        gesture.scale = 1
    }
    
    @objc func onRotate(gesture: UIRotationGestureRecognizer) {
        guard let vv = gesture.view as? ImageViewEx, vv.isSelected  else { return }
        vv.transform = vv.transform.rotated(by: gesture.rotation)
        gesture.rotation = 0
    }
    
}


// category picker
extension ClosetMontageStudio: StrPickerDelegate {
    
    func strPickerOnRetrieveData(for indentifier: String?) -> [String] {
        return Repo.look
            .listCategories(user: mUser)
            .map({ $0.uppercased() })
            .sorted()
    }

    func strPicker(for identifier: String?, onSelect s: String) {
        sCategory = s
        updateDesktopCategory()
    }
    
}

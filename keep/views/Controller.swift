class Controller<V> {
    
    var view: V? = nil;
    
    func attach(view: V) {
        self.view = view;
        
        afterAttached()
    }
    
    func detach() {
        beforeDetached();
        self.view = nil;
    }
    
    open func afterAttached() {
        // override this
    }
    
    open func beforeDetached() {
        // override this
    }
    
}

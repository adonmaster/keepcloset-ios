import UIKit
import CoreData
import FBSDKCoreKit
import GoogleSignIn


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "keep")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    struct Simb {
        var id: Int
        var text: String
    }
    
    func test()
    {
    }
   
    
    
    // MARK: init
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        test()
        
        configDb()
        configInitialScreen()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
    {
        FBSDKApplicationDelegate.sharedInstance()?.application(
            app, open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation] as? String
        )
        
        return GIDSignIn.sharedInstance()?.handle(
            url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation] as? String
        ) ?? true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        let n = Notification(name: .exApplicationDidBecomeActive)
        NotificationCenter.default.post(n)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
}

// MARK: initial screen
extension AppDelegate {
    
    func configInitialScreen()
    {
        let storyboardName = Repo.user.active() == nil
            ? "Register"
            : "Main"
        
        window?.rootViewController = UIStoryboard(name: storyboardName, bundle: nil)
            .instantiateInitialViewController()
    }
    
}

// MARK: core data

extension AppDelegate {
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}


// MARK: db
extension AppDelegate {
    
    func configDb() {
        Repo.seed()
    }
    
}

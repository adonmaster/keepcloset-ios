import CoreData

class LuggageItem: NSManagedObject {
    
    lazy var p = LuggageItemPresenter(self)
    
    func cloneFor(context: NSManagedObjectContext, parent: Luggage) -> LuggageItem
    {
        let new = LuggageItem(context: context)
        new.parent = parent
        new.title = self.title
        new.desc = self.desc
        new.thumb = self.thumb
        new.is_packed = false
        
        return new
    }
    
}

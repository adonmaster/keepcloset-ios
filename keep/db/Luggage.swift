import CoreData

class Luggage: NSManagedObject {
    
    lazy var p = LuggagePresenter(self)
    
    func clone(_ context: NSManagedObjectContext) -> Luggage
    {
        let new = Luggage(context: context)
        new.user = self.user
        new.dated_at = self.dated_at
        new.duration = self.duration
        new.location = self.location
        
        return new
    }
    
}

import CoreData
import UIKit

class User: NSManagedObject {
    
    lazy var p = Presenter(model: self)

    
    // presenter
    
    struct Presenter {
        
        var model: User
        
        func thumb(to imageView: UIImageView) {
            loadImg(model.avatar_thumb_url, imageView)
        }
        
        func avatar(to imageView: UIImageView) {
            loadImg(model.avatar_url, imageView)
        }
        
        private func loadImg(_ urlString: String?, _ imageView: UIImageView) {
            guard let u = urlString, let url = F.app.urlFrom(dynamicPath: u) else { return }
            imageView.kf.setImage(with: url, options: [.forceRefresh])
        }
        
    }
    
}

import CoreData
import UIKit

class LookItemRepo: BaseRepo<LookItem> {
    
    func sync(_ parent: Look, _ imgs: [ImageViewEx])
    {
        // delete remmaining
        for item in self.getFor(parent: parent) {
            self._delete(item)
        }
        
        // add
        for (index, v) in imgs.enumerated()
        {
            let model = LookItem(context: self.context)
            model.statItem = v.outItem
            model.parent = parent
            model.img = v.getUImage()?.pngData()
            
            model.center_x = Float(v.center.x)
            model.center_y = Float(v.center.y)

            model.position = Int16(index)

            model.transform_a = Float(v.transform.a)
            model.transform_b = Float(v.transform.b)
            model.transform_c = Float(v.transform.c)
            model.transform_d = Float(v.transform.d)
            
            model.transform_tx = Float(v.transform.tx)
            model.transform_ty = Float(v.transform.ty)
        }
        
        try? context.save()
    }
    
    func getFor(parent: Look) -> [LookItem]
    {
        let sort = [NSSortDescriptor(key: "position", ascending: true)]
        return self._get(condition: "parent = %@", args: [parent], sort: sort)
    }
    
}

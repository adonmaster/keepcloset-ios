struct SupportRModel: Decodable {
    
    var message: String
    var pack: [Item]
    
    struct Item: Decodable {
        var title: String
        var desc: String
        var url: String
    }
    
}

import UIKit
import Kingfisher


class ItemPresenter: BasePresenter<Item> {

    func thumbLoad(imageView: UIImageView?) {
        Task.main {
            if let data = self.model.thumb {
                imageView?.image = UIImage(data: data)
            } else {
                imageView?.image = #imageLiteral(resourceName: "404")
            }
        }
    }

    func fullLoad(cb: @escaping (UIImage)->Void) {
        Task.main {
            if let data = self.model.img, let img = UIImage(data: data) {
                cb(img)
            } else {
                cb(#imageLiteral(resourceName: "404"))
            }
        }
    }
    
    var category: String {
        return model.category?.uppercased() ?? "-Sem categoria-".l
    }
    
    var therm: String {
        return Res.i.therm(uid: model.therm_uid)
    }
    
    func therm(inverted: Bool=true) -> UIColor {
        return Res.i.therm(uid: model.therm_uid, inverted: inverted)
    }
    
    var brand: NSAttributedString {
        if let s = model.brand, s.count > 0 {
            return Span(s.uppercased()).color(Cl.mc.red700Color()).get()
        }
        return Span("Marca desconhecida".l.uppercased()).color(Cl.mc.grey500Color()).get()
    }
    
    var status: NSAttributedString {
        if let s = model.status, s.count > 0 {
            return Span(s.uppercased()).color(Cl.mc.red700Color()).get()
        }
        return Span("Status desconhecido".l.uppercased()).color(Cl.mc.grey500Color()).get()
    }
    
    var color1: UIColor? {
        if let cl = model.color1 {
            return Cl.from(hex: cl)
        }
        return nil
    }
    
    var color2: UIColor? {
        if let cl = model.color2 {
            return Cl.from(hex: cl)
        }
        return nil
    }
    
}

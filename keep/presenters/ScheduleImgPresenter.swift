import UIKit

class ScheduleImgPresenter: BasePresenter<ScheduleImg> {
    
    func loadThumb(_ cb: @escaping (UIImage)->()) {
        loadImage(model.thumb, cb)
    }
    
    func loadFull(_ cb: @escaping (UIImage)->()) {
        loadImage(model.img, cb)
    }
    
    var colors: NSAttributedString
    {
        let s = Span("")
        let fun: (String?)->() = { str in
            let square = ""
            if let cl = str, let hcl = Cl.from(hex: cl) {
                if s.count > 0 { _ = s.append(" ") }
                _ = s.append(square).color(hcl)
            }
        }
        
        fun(model.color1)
        fun(model.color2)
        
        return s.get()
    }
    
    var date: Date {
        var res: Date?
        if let s = model.date {
            res = Carbon.parseFromSqlDate(s)?.get()
        }
        return res ?? Carbon().get()
    }
    
}

import UIKit

class InspirationPresenter: BasePresenter<Inspiration> {
    
    private func loadImg(_ data: Data?, _ cb: @escaping (UIImage)->()) {
        Task.main {
            if let d = data, let img = UIImage(data: d) {
                cb(img)
            } else {
                cb(#imageLiteral(resourceName: "404"))
            }
        }
    }
    
    func img(_ cb: @escaping (UIImage)->()) {
        loadImg(model.img, cb)
    }
    
    func thumb(_ cb: @escaping (UIImage)->()) {
        loadImg(model.thumb, cb)
    }
    
    var category: String {
        if let s = model.category?.trim(), s.count > 0 {
            return Str.capitalizeFirst(s: s)
        }
        return "Sem categoria".l
    }
    
    var desc: String {
        if let s = model.desc?.trim(), s.count > 0 {
            return s
        }
        return "-"
    }
    
    var colors: [UIColor]
    {
        var r: [UIColor] = []
        
        if let cls = self.model.color1, let cl = Cl.from(hex: cls) {
            r.append(cl)
        }
        
        if let cls = self.model.color2, let cl = Cl.from(hex: cls) {
            r.append(cl)
        }
        
        return r
    }
    
}

import UIKit

class BasePresenter<M> {
    
    let model: M
    let cache = Cache()
    
    init(_ model: M) {
        self.model = model
    }
    
    func loadImage(_ d: Data?, _ cb: @escaping (UIImage)->()) {
        Task.main {
            if let data = d, let img = UIImage(data: data) {
                cb(img)
                return
            }
            cb(#imageLiteral(resourceName: "404"))
        }
    }
    
}

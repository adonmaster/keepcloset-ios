import UIKit


class LuggagePresenter: BasePresenter<Luggage> {
    
    var itemsCount: Int {
        let n = self.model.value(forKeyPath: "items.@count") as? Int
        return n ?? 0
    }
    
    var itemsPackedCount: Int {
        let uri = model.objectID.uriRepresentation().absoluteString
        return cache.retrieve(key: "LuggagePresenter.itemsPackedCount@\(uri)", {
            return Repo.luggage.countItemPacked(parent: self.model)
        })
    }
    
    var duration: String {
        if let dur = model.duration, !dur.contains("dia".l) {
            return "\(dur) "+"dia(s)".l
        }
        return model.duration ?? "desconhecido".l
    }
    
    var datedAt: String {
        let m = model
        return "\(Carbon(m.dated_at).format(pattern: "dd/MM")) (\(Carbon(m.dated_at).diffForHuman()))"
    }
    
    func clearCache() {
        cache.clear()
    }
    
}





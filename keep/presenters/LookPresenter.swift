import UIKit
import Kingfisher


class LookPresenter: BasePresenter<Look> {
    
    func loadFull(_ cb: @escaping (UIImage)->()) {
        Task.main {
            if let data = self.model.full, let img = UIImage(data: data) {
                cb(img)
            } else {
                cb(#imageLiteral(resourceName: "404"))
            }
        }
    }
    
    func loadThumb(_ cb: @escaping (UIImage)->()) {
        Task.main {
            if let data = self.model.thumb, let img = UIImage(data: data) {
                cb(img)
            } else {
                cb(#imageLiteral(resourceName: "404"))
            }
        }
    }
    
}

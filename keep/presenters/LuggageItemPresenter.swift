import UIKit

class LuggageItemPresenter: BasePresenter<LuggageItem> {
    
    func loadImage(cb: @escaping (UIImage)->()) {
        Task.main {
            if let data = self.model.thumb {
                if let uiimage = UIImage(data: data) {
                    cb(uiimage)
                    return
                }
            }
            cb(#imageLiteral(resourceName: "404"))
        }
    }
    
    var title: String {
        if let t = model.title {
            return t.prefix(1).uppercased() + t.dropFirst()
        }
        return "Desconhecido".l
    }
    
}

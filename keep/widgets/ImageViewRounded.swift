import UIKit;

class ImageViewRounded: UIImageView {
    
    @IBInspectable var borderSize: CGFloat = 0;
    @IBInspectable var borderColor: UIColor = UIColor.gray;
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        roundIt()
    }
    
    func roundIt()
    {
        layer.cornerRadius = frame.height/2;
        clipsToBounds = true;
        if (borderSize > 0) {
            layer.borderWidth = borderSize;
            layer.borderColor = borderColor.cgColor;
        }
    }
    
}

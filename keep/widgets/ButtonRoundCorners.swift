import UIKit


class ButtonRoundCorners: UIButton {
    
    @IBInspectable var borderRadius: CGFloat = 20
    @IBInspectable var borderSize: CGFloat = 0
    @IBInspectable var borderColor: UIColor = UIColor.gray
    
    override func awakeFromNib()
    {
        layer.masksToBounds = false;
        layer.cornerRadius = borderRadius == -1 ? frame.height/2 : borderRadius
        clipsToBounds = true
        if (borderSize > 0) {
            layer.borderWidth = borderSize
            layer.borderColor = borderColor.cgColor
        }
    }
    
}

import UIKit;

class TableViewCell: UITableViewCell {
    
    @IBInspectable var selectedColor: UIColor = UIColor.blue
    
    var mProtectedBackgroundViews: [UIView] = []
    
    override func awakeFromNib()
    {
        let v = UIView(frame: bounds)
        v.backgroundColor = selectedColor;
        selectedBackgroundView = v;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        let bkup = mProtectedBackgroundViews.map { ($0, $0.backgroundColor) }
        
        super.setSelected(selected, animated: animated)
        
        if selected { bkup.forEach { $0.0.backgroundColor = $0.1 } }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool)
    {
        let bkup = mProtectedBackgroundViews.map { ($0, $0.backgroundColor) }
        
        super.setHighlighted(highlighted, animated: animated)
        
        if highlighted { bkup.forEach { $0.0.backgroundColor = $0.1 } }
    }
    
}

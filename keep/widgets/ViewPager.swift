import UIKit


@IBDesignable class ViewPager: UIView {
    
    @IBOutlet var rootController: UIViewController!;
    
    @IBInspectable var controller_1: String = "";
    @IBInspectable var controller_2: String = "";
    @IBInspectable var controller_3: String = "";
    @IBInspectable var controller_4: String = "";    
    @IBInspectable var controller_5: String = "";
    
    var controllers: [String] = [];
    
    weak var currentController: UIViewController!;
    
    override func awakeFromNib()
    {
        controllers.removeAll();
        
        if (!controller_1.isEmpty) { controllers.append(controller_1) };
        if (!controller_2.isEmpty) { controllers.append(controller_2) };
        if (!controller_3.isEmpty) { controllers.append(controller_3) };
        if (!controller_4.isEmpty) { controllers.append(controller_4) };
        if (!controller_5.isEmpty) { controllers.append(controller_5) };
        
        if (controllers.count > 0)
        {
            setup();
        }
    }
    
    private func setup()
    {
        currentController = retrieveViewController(storyboardId: controllers[0]);
        currentController!.view.translatesAutoresizingMaskIntoConstraints = false;
        
        rootController.addChild(currentController);
        addSubview(subView: currentController.view, toView: self);
    }
    
    func swapToControllerStoryId(controllerStoryboardId: String, animator: SwapAnimator) -> UIViewController?
    {
        let newViewController = retrieveViewController(storyboardId: controllerStoryboardId);
        
        newViewController!.view.translatesAutoresizingMaskIntoConstraints = false;
        cycleFromViewController(oldViewController: currentController, toViewController: newViewController!, animator: animator)
        currentController = newViewController
        
        return newViewController;
    }
    
    func swapToController(pageId: Int, animator: SwapAnimator) -> UIViewController?
    {
        return swapToControllerStoryId(controllerStoryboardId: controllers[pageId], animator: animator);
    }
    
    // MARK - support
    
    private func retrieveViewController(storyboardId: String) -> UIViewController?
    {
        let vc = rootController.storyboard?.instantiateViewController(withIdentifier: storyboardId)
        return vc;
    }
    
    private func addSubview(subView:UIView, toView parentView:UIView)
    {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        
        parentView.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|", options: [],
                                           metrics: nil, views: viewBindingsDict)
        );
        
        parentView.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|", options: [],
                                           metrics: nil, views: viewBindingsDict)
        );
    }
    
    private func cycleFromViewController(oldViewController: UIViewController?, toViewController newViewController: UIViewController, animator: SwapAnimator)
    {
        oldViewController?.willMove(toParent: nil)
        rootController.addChild(newViewController)
        addSubview(subView: newViewController.view, toView:self)
        
        newViewController.view.layoutIfNeeded();
        oldViewController?.view.layoutIfNeeded();
        
        // there is an initial view controller
        if oldViewController != nil
        {
            animator.animate(root: rootController, old: oldViewController!, new: newViewController, completion: { finished in
                oldViewController!.view.removeFromSuperview()
                oldViewController!.removeFromParent()
                newViewController.didMove(toParent: self.rootController)
            });
        }
            
            // first vc in pager... no transition animation
        else
        {
            newViewController.didMove(toParent: self.rootController)
        }
    }
}


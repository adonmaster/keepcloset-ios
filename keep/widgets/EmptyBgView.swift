//
//  EmptyBGView.swift
//  keep
//
//  Created by Adonio Silva on 27/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit

class EmptyBgView: UIView {
    
    private struct RectEx {
        let x: Int
        let y: Int
        let w: Int
        let h: Int
        let color: UIColor
        func toRect() -> CGRect {
            return CGRect(origin: CGPoint(x: x, y: y), size: CGSize(width: w, height: h))
        }
    }
    
    override func draw(_ rect: CGRect) {
        let cellsize = 24
        var rects = [RectEx]()
        let ww = Int(ceil(Double(Int(frame.width)/cellsize)))
        for x in 0...ww {
            for y in 0...Int(ceil(Double(Int(frame.height)/cellsize))) {
                rects.append(
                    RectEx(
                        x: x * cellsize, y: y * cellsize, w: cellsize, h: cellsize,
                        color: (y * ww + x).isMultiple(of: 2)
                            ? UIColor.white
                            : UIColor(red: 0.93, green: 0.95, blue: 0.95, alpha: 1)
                    )
                )
            }
        }
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        rects.forEach {
            context.addRect($0.toRect())
            context.setFillColor($0.color.cgColor)
            context.fillPath()
        }
        
    }
}

import UIKit

class ViewCircleHollow: UIView {
    
    var mMaskLayer: CAShapeLayer!;
    var mSizeInCenter: CGFloat!
    
    override func awakeFromNib() {
        clipsToBounds = true;
        updateMask();
    }
    
    func updateMask()
    {
        let w = bounds.width;
        let h = bounds.height
        let radius : CGFloat = (w < h ? w : h) / 2
        mSizeInCenter = radius * 2
        let rect = UIBezierPath(rect: bounds)
        let center = CGPoint(x: bounds.width/2, y: bounds.height/2)
        
        rect.addArc(withCenter: center, radius: radius, startAngle: 0.0, endAngle: CGFloat(2 * Double.pi), clockwise: true);
        
        mMaskLayer = CAShapeLayer();
        mMaskLayer.path = rect.cgPath
        mMaskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        
        layer.mask = mMaskLayer
    }
    
    override func layoutSubviews() {
        updateMask();
    }
    
}

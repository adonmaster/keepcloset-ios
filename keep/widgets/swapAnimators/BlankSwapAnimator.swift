import UIKit;

class BlankSwapAnimator: SwapAnimator {
    
    func animate(root: UIViewController, old: UIViewController, new: UIViewController, completion: ((Bool) -> Void)?)
    {
        completion?(true);
    }
}
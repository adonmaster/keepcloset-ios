import UIKit;

class CrossDissolveSwapAnimator: SwapAnimator {
    
    func animate(root: UIViewController, old: UIViewController, new: UIViewController, completion: ((Bool) -> Void)?)
    {
        new.view.alpha = 0
        new.view.layoutIfNeeded()

        UIView.animate(withDuration: 0.5, animations: {
                new.view.alpha = 1
                old.view.alpha = 0
            },
            completion: completion);
    }
    
}

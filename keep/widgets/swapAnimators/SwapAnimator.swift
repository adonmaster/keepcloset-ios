import UIKit;

protocol SwapAnimator {
    
    func animate(root: UIViewController, old: UIViewController, new: UIViewController, completion: ((Bool) -> Void)?);
    
}
import UIKit;

class SlideVerticalSwapAnimator: SwapAnimator {
    
    let toBottom: Bool;
    
    init(toBottom: Bool = true)
    {
        self.toBottom = toBottom;
    }
    
    func animate(root: UIViewController, old: UIViewController, new: UIViewController, completion: ((Bool) -> Void)?)
    {
        let f = old.view.frame;
        
        let newStartY = toBottom
            ? -root.view.frame.size.height
            : root.view.frame.size.height;
        
        let oldFinishY = toBottom
            ? f.origin.y + f.size.height
            : f.origin.y - f.size.height
        
        new.view.frame.origin.y = newStartY;
        
        UIView.animate(withDuration: 5, delay: 0, options: [.curveEaseInOut], animations: {
            old.view.transform = CGAffineTransform(translationX: 0, y: oldFinishY);
            new.view.frame.origin.y = 0;
            }, completion: completion);
        
    }
}

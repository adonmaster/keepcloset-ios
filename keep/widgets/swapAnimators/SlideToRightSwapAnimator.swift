import UIKit;


class SlideToRightSwapAnimator: BaseSwapAnimator {
    
    override func animate(root: UIViewController, old: UIViewController, new: UIViewController, completion: ((Bool) -> Void)?)
    {
        let f = old.view.frame;
        new.view.frame.origin.x = root.view.frame.size.width;
        
        UIView.animate(withDuration: speed, delay: 0, options: [.curveEaseInOut], animations: {
            old.view.transform = CGAffineTransform(translationX: f.origin.x - f.size.width, y: 0);
            new.view.frame.origin.x = 0;
        }, completion: completion);
    }
    
}

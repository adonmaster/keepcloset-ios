import UIKit;


class BaseSwapAnimator: SwapAnimator {
    
    let speed: TimeInterval;
    
    init(speed: TimeInterval? = nil)
    {
        self.speed = speed ?? 0.5;
    }
    
    func animate(root: UIViewController, old: UIViewController, new: UIViewController, completion: ((Bool) -> Void)?) {
        // nothing
        
    }
}

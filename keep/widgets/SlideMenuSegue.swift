import UIKit;

class SlideMenuSegue: UIStoryboardSegue {
    
    override func perform()
    {
        if let sbName = destination.storyboard?.value(forKey: "name") as? String {
            Repo.pref.setLastStoryboard(sbName)
        }
        
        source.slideMenuController()?.changeMainViewController(destination, close: true);
    }
    
}

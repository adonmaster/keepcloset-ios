import UIKit


class ImageViewEx: UIView {
    
    
    // static
    private static var instances: [ImageViewEx] = []
    
    class func register(_ i: ImageViewEx) {
        if instances.firstIndex(of: i) == nil {
            instances.append(i)
        }
    }
    
    class func register(_ iss: [ImageViewEx]) {
        for i in iss {
            ImageViewEx.register(i)
        }
    }
    
    class func unregister(_ i: ImageViewEx) {
        if let index = instances.firstIndex(of: i) {
            instances.remove(at: index)
        }
    }
    
    class func unregister(_ iss: [ImageViewEx]) {
        for i in iss {
            ImageViewEx.unregister(i)
        }
    }
    
    class func unselectAll(groupSelection: Int?=nil) {
        for i in ImageViewEx.instances {
            if groupSelection == nil || i.selectGroup == groupSelection! {
                i.SELECTED = false
            }
        }
    }
    
    // instance
    
    private var BORDER_SIZE: CGFloat = 20.0
    private var SELECTED = false {
        didSet {
            updateSelected()
        }
    }
    
    private var IMAGEVIEW: UIImageView!
    var isSelected: Bool {
        get {
            return SELECTED
        }
    }
    
    private var UIIMAGE: UIImage?
    private var mItem: Item?
    var outItem: Item? {
        return mItem
    }
    
    // public
    var selectGroup: Int?
    var delegate: ImageViewExDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        mInit(nil)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        mInit(nil)
    }
    
    convenience init(img: UIImage, item: Item?) {
        self.init()
        self.mItem = item
        mInit(img)
    }
    
    private func mInit(_ img: UIImage?)
    {
        self.isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(gesture:))))
        translatesAutoresizingMaskIntoConstraints = false
        
        // imageview
        UIIMAGE = img
        IMAGEVIEW = UIImageView(image: img)
        let imageView = IMAGEVIEW!
        imageView.sizeToFit()
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        
        BORDER_SIZE = imageView.frame.width * 0.2
        
        initSize()
        updateSelected()
    }
    
    private func updateSelected()
    {
        backgroundColor = SELECTED ? UIColor(red: 0, green: 1, blue: 0, alpha: 0.2) : UIColor.clear
        
        if let sgroup = self.selectGroup, SELECTED {
            for i in ImageViewEx.instances {
                if i.selectGroup == sgroup && i !== self {
                    i.SELECTED = false
                }
            }
        }
        
        if SELECTED {
            delegate?.imageViewExOnSelect(self)
        } else {
            delegate?.imageViewExOnUnselect(self)
        }
    }
    
    private func initSize()
    {
        let w = IMAGEVIEW.frame.width + BORDER_SIZE * 2
        let h = IMAGEVIEW.frame.height + BORDER_SIZE * 2
        
        self.frame.size = CGSize(width: w, height: h)
        
        if let im = IMAGEVIEW {
            im.center.x = w / 2
            im.center.y = h / 2
        }
    }
    
    
    @objc func onTap(gesture: UITapGestureRecognizer) {
        SELECTED = !SELECTED
    }
    
    // getters
    
    func getUImage() -> UIImage? {
        return UIIMAGE
    }
    
}

protocol ImageViewExDelegate {
    
    func imageViewExOnSelect(_ v: ImageViewEx)
    func imageViewExOnUnselect(_ v: ImageViewEx)
    
}

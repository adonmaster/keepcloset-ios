import UIKit;

@IBDesignable class TextFieldEx: UITextField {
    
    @IBInspectable var awesomeCode: String = ""
    @IBInspectable var awesomeColor: UIColor? = nil
    
    @IBInspectable var insetTop: CGFloat = 10;
    @IBInspectable var insetRight: CGFloat = 10;
    @IBInspectable var insetBottom: CGFloat = 10;
    @IBInspectable var insetLeft: CGFloat = 10;
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func insetMaker(_ bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets.init(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight)
        )
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
    override func awakeFromNib() {
        // font awesome accessory button
        if (!awesomeCode.isEmpty)
        {
            Keyboards.setInputAccessoryButton(
                textField: self,
                withAwesome: awesomeCode,
                color: awesomeColor ?? self.tintColor,
                target: self,
                action: #selector(onAccessoryDoneClicked)
            );
        }
    }
    
    
    @objc func onAccessoryDoneClicked()
    {
        resignFirstResponder();
    }
}

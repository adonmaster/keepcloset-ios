
class Env {
    
    static let insta_id = "fc1eaf6642cc4f33bc7d0ab68a17c645"
    static let insta_secret = "4efc716a94bf4c75b22cbfe1d37df9e0"
    static let insta_auth_url = "https://api.instagram.com/oauth/authorize"

    static let google_id = "201823360614-n4lt3nf0k9hbv272s5ah7mlrmr75c4la.apps.googleusercontent.com"
    
    static let url = "http://keepcloset.com.br"
    static let url_api = url + "/api"
    
    static let beta_user_email = "beta11456@test.com"
    static let beta_user_code = "012345"
    
    static let group_container_id = "group.br.com.priscila.keep"
    
}

//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject

+(UIImage *)extractForegroundMask:(UIImage *)_img :(UIImage *)_mask;

@end

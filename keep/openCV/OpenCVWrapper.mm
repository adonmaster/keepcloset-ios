//
//  OpenCVWrapper.m
//  keep
//
//  Created by Adonio Silva on 17/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//
// Soh fiz uma tradução da contraparte em Android... lah estah o original!
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import <opencv2/imgproc.hpp>

#import "keep-Bridging-Header.h"


// utils
struct MaskCalculatedInfo {
    cv::Rect rect;
    cv::Mat markers;
};


// the damn thing
@implementation OpenCVWrapper: NSObject

    +(UIImage *)extractForegroundMask:(UIImage *)_img :(UIImage *)_mask
    {
        int width = _img.size.width;
        int height = _img.size.height;
        
        cv::Mat img;
        UIImageToMat(_img, img, true);
        
        cv::Mat img3c;
        cv::cvtColor(img, img3c, cv::COLOR_RGBA2RGB);

        cv::Mat fgdModel = cv::Mat();
        cv::Mat bgdModel = cv::Mat();
        
        MarkerInfo minfo = [self calculateMarkers:_mask];
        
        cv::Mat res = cv::Mat(height, width, CV_8UC3, cv::Scalar(0, 0, 0));
        
        // halt
        if (minfo.rect.width > 2 && minfo.rect.height > 2)
        {
            cv::grabCut(img3c, minfo.markers, minfo.rect, bgdModel, fgdModel, 8, cv::GC_INIT_WITH_MASK|cv::GC_INIT_WITH_RECT);
            
            //
            cv::Mat source = cv::Mat(1, 1, CV_8U, cv::Scalar(cv::GC_PR_FGD));
            cv::compare(minfo.markers, source, minfo.markers, CV_CMP_EQ);
            
            cv::Mat resFgnd = cv::Mat(height, width, CV_8UC3, cv::Scalar(255, 255, 255));
            resFgnd.copyTo(res, minfo.markers);
        }
        
        return MatToUIImage(res);
    }

    // *******

    struct MarkerInfo {
        cv::Mat markers;
        cv::Rect rect;
    };

    + (MarkerInfo)calculateMarkers: (UIImage *)_mask
    {
        cv::Mat mask;
        UIImageToMat(_mask, mask, false);
        
        cv::Mat markers = cv::Mat(_mask.size.height, _mask.size.width, CV_8U, cv::Scalar(cv::GC_BGD));
        cv::Rect rect = cvRect(0, 0, 0, 0);
        
        int minx = 99999999, miny = 999999999, maxx = 0, maxy = 0;
        bool hasPixel = false;
        
        for (int x = 0; x < mask.cols - 0; ++x) {
            for (int y = 0; y < mask.rows - 0; ++y) {
                uchar r = (*mask.ptr<cv::Vec3b>(y, x))[2];
                uchar g = (*mask.ptr<cv::Vec3b>(y, x))[1];
                uchar b = (*mask.ptr<cv::Vec3b>(y, x))[0];
                if (r > 2 || g > 2 || b > 2) {
                    markers.ptr(y, x)[0] = cv::GC_PR_FGD;
                    
                    hasPixel = true;
                    minx = MIN(minx, x);
                    miny = MIN(miny, y);
                    maxx = MAX(maxx, x);
                    maxy = MAX(maxy, y);
                }
            }
        }
        
        if (hasPixel) {
            rect = cv::Rect(minx, miny, maxx-minx, maxy-miny);
        }
        
        struct MarkerInfo res = MarkerInfo();
        res.markers = markers;
        res.rect = rect;
        
        return res;
    }


@end

//
//  ShareViewController.swift
//  ShareExt
//
//  Created by Adonio Silva on 30/05/21.
//  Copyright © 2021 Adonio Silva. All rights reserved.
//

import UIKit
import MobileCoreServices



class ShareViewController: UIViewController {

    //
    @IBOutlet var uiBtnDone: UIButton!
    @IBOutlet var uiLbl: UILabel!
    @IBOutlet var uiImg: UIImageView!
    @IBOutlet var uiSegment: UISegmentedControl!
    
    //
    private var mUrls = [URL]() {
        didSet {
            let has = mUrls.count > 0
            uiBtnDone?.isEnabled = has
            uiBtnDone?.setTitleColor(has ? UIColor.black : UIColor.lightGray, for: .normal)
            
            updateLblTxt()
        }
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mUrls = []
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadUrls()
    }
    
    @IBAction func onSegment(_ segment: UISegmentedControl) {
        switch segment.selectedSegmentIndex {
        case 0:
            updateLblTxt()
            uiImg.image = #imageLiteral(resourceName: "ic_hanger")
        case 1:
            updateLblTxt()
            uiImg.image = #imageLiteral(resourceName: "ic_hearts")
        default:
            break
        }
        updateLblTxt()
    }
    
    private func updateLblTxt() {
        var s = ""
        if mUrls.count == 0 {
            s = "Ainda processando as imagens para importação..."
        } else if uiSegment?.selectedSegmentIndex == 0 {
            s = "\(mUrls.count) "+"item(s) será(ão) adicionado(s) ao Closet automaticamente quando você abrir o KeepCloset".l
        } else if uiSegment?.selectedSegmentIndex == 1 {
            s = "\(mUrls.count) "+"item(s) será(ão) adicionado(s) ao Inspirações automaticamente quando você abrir o KeepCloset".l
        }
        uiLbl?.text = s
    }
    
    @IBAction func uiBtnCancel(_ sender: Any) {
        let err = NSError(domain: "", code: 0, userInfo: nil)
        extensionContext?.cancelRequest(withError: err)
    }

    @IBAction func uiBtnDone(_ sender: Any)
    {
        let key = uiSegment.selectedSegmentIndex == 0
            ? SharedPref.key_shared_pending_closet
            : SharedPref.key_shared_pending_inpirations
        
        var newList = SharedPref.shared.get(key: key) ?? []
        newList.append(contentsOf: mUrls)
        
        let res = saveImagesToGroupDomain(newList)
        SharedPref.shared.put(key: key, list: res)
        
        res.forEach {
            let ss = $0.path
            let ff = FileManager.default.fileExists(atPath: ss)
            print(ff)
        }
        
        extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
    }

    private func saveImagesToGroupDomain(_ _list: [URL]) -> [URL]
    {
        let list = Arr(_list).distinct { $0.absoluteString }
        
        var r = [URL]()
        
        for url in list
        {
            // aparently these urls are temporary, i'll copy em to group container's folder
            // make sure to clean that up later on
            var dest = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: Env.group_container_id)!
            dest.appendPathComponent(url.lastPathComponent)
            try! FileManager.default.copyItem(at: url, to: dest)
            
            r.append(dest)
        }
        
        return r
    }
    
}

// MARK: load urls
extension ShareViewController {
    private func loadUrls() {
        guard let items = extensionContext?.inputItems as? [NSExtensionItem] else { return }
        items.forEach { item in
            item.attachments?.forEach({ [weak self] provider in
                
                let type = kUTTypeImage as String
                if provider.hasItemConformingToTypeIdentifier(type) {
                    provider.loadItem(forTypeIdentifier: type) { item, _ in
                        if let url = item as? URL {
                            Task.main {
                                self?.mUrls.append(url)
                            }
                        }
                    }
                }
                
            })
        }
    }
}
